#pragma once

#cmakedefine i2MassChroQ_VERSION "@i2MassChroQ_VERSION@"


#cmakedefine SOFTWARE_NAME "@SOFTWARE_NAME@"

#cmakedefine BUILD_SYSTEM_NAME "@BUILD_SYSTEM_NAME@"

#include <QDebug>

#cmakedefine CMAKE_BUILD_TYPE "@CMAKE_BUILD_TYPE@"


#cmakedefine EXPERIMENTAL_FEATURES_ON @EXPERIMENTAL_FEATURES_ON@
