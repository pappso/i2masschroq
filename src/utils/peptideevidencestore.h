/**
 * \file utils/peptideevidencestore.h
 * \date 18/11/2017
 * \author Olivier Langella
 * \brief store unique instances of peptide evidences
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include <map>
#include <set>
#include "../core/automaticfilterparameters.h"

class MsRun;
class PeptideEvidence;
class PeptideXtp;

class PeptideEvidenceStore
{
  public:
  /** @brief directly register an instance of peptide evidence : no duplication
   * check
   */
  std::shared_ptr<PeptideEvidence> &
  recordInstance(const PeptideEvidence *p_peptide_evidence);
  /** @brief register an instance of peptide evidence : duplication check
   */
  std::shared_ptr<PeptideEvidence> &
  getInstance(const PeptideEvidence *p_peptide_evidence);

  void updateAutomaticFilters(
    const AutomaticFilterParameters &automatic_filter_parameters,
    std::map<const PeptideXtp *, std::set<const MsRun *>>
      &count_msrun_by_valid_peptide);

  void
  pepreproFilter(const AutomaticFilterParameters &automatic_filter_parameters,
                 std::map<const PeptideXtp *, std::set<const MsRun *>>
                   &count_msrun_by_valid_peptide);

  const std::vector<std::shared_ptr<PeptideEvidence>> &
  getPeptideEvidenceList() const;
  std::size_t size() const;
  void clearMap();


  /** @brief look for a peptide in the same XIC
   * @param peptide_evidence_list the peptide evidence list to build
   * @param p_msrun MSrun to look for
   * @param p_peptide peptide to look for
   * @param charge charge to look for
   */
  void getSameXicPeptideEvidenceList(
    std::vector<const PeptideEvidence *> &peptide_evidence_list,
    const MsRun *p_msrun,
    const PeptideXtp *p_peptide,
    unsigned int charge) const;

  /** @brief ensures that scan numbers are taken into account as spectrum index
   *
   * this is needed in the case of timsTOF data analysis for instance
   */
  void ensureSpectrumIndexRef();

  /** @brief counts unique identifier of spectrum used in valid PSM
   * intended to be used as counting purpose
   */
  std::size_t countUniqueValidSpectrumIndexList() const;


  /** @brief counts unique peptide sequenceLi found in this store
   */
  std::size_t countSequenceLi(ValidationState valid_status) const;

  private:
  std::vector<std::shared_ptr<PeptideEvidence>> _peptide_evidence_list;

  std::multimap<unsigned int, std::shared_ptr<PeptideEvidence>>
    _multimap_scan_evidence;
};
