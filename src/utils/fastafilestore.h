/**
 * \file utils/fastafiletore.h
 * \date 22/4/2017
 * \author Olivier Langella
 * \brief store unique version of fastafile
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef FASTAFILESTORE_H
#define FASTAFILESTORE_H

#include <vector>
#include "../files/fastafile.h"

class FastaFileStore
{
  public:
  FastaFileStore();
  ~FastaFileStore();

  FastaFileSp getInstance(const FastaFile &fastafile);

  const std::vector<FastaFileSp> &getFastaFileList() const;

  private:
  std::vector<FastaFileSp> _map_fastafile;
};


#endif // FASTAFILESTORE_H
