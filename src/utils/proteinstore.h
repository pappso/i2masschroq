/**
 * \file utils/proteinstore.h
 * \date 23/3/2017
 * \author Olivier Langella
 * \brief store unique version of proteins
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include "../core/proteinxtp.h"
#include "types.h"
#include <QString>
#include <QRegularExpression>
#include <map>
#include <vector>

class FastaFile;

class ProteinStore
{
  friend class AccessionContaminantReader;
  friend class AccessionDecoyReader;

  public:
  ProteinStore();
  ~ProteinStore();

  ProteinXtpSp &getInstance(ProteinXtpSp &protein_in);

  void setRegexpDecoyPattern(const QString &pattern);

  QRegularExpression getRegexpDecoy() const;

  void setRegexpContaminantPattern(const QString &pattern);

  QRegularExpression getRegexpContaminant() const;

  void addContaminantFastaFile(const FastaFile *p_fasta_file);
  void addDecoyFastaFile(const FastaFile *p_fasta_file);

  const std::vector<const FastaFile *> &getContaminantFastaFileList() const;
  const std::vector<const FastaFile *> &getDecoyFastaFileList() const;


  void clearContaminants();
  void clearDecoys();

  std::size_t size() const;

  const std::map<QString, ProteinXtpSp> &getProteinMap() const;


  /** @brief method used to select contaminant proteins
   */
  void setContaminantSelectionType(ContaminantSelectionType cont_select_type);
  ContaminantSelectionType getContaminantSelectionType() const;

  /** @brief method used to select decoy proteins
   */
  void setDecoySelectionType(DecoySelectionType decoy_select_type);
  DecoySelectionType getDecoySelectionType() const;

  protected:
  void setContaminantAccession(QString accession);
  void setDecoyAccession(QString accession);


  private:
  void setProteinInformations(ProteinXtpSp &protein_in);

  private:
  std::map<QString, ProteinXtpSp> _map_accession_protein_list;

  /** \brief decoy Fasta file liste */
  std::vector<const FastaFile *> _fasta_decoy_list;
  /** \brief contaminant Fasta file liste */
  std::vector<const FastaFile *> _fasta_contaminant_list;

  /** \brief recognize decoy accession */
  QRegularExpression _regexp_decoy;
  /** \brief recognize contaminant accession */
  QRegularExpression _regexp_contaminant;

  ContaminantSelectionType m_contaminantSelectionType =
    ContaminantSelectionType::regexp;

  DecoySelectionType m_decoySelectionType = DecoySelectionType::regexp;
};
