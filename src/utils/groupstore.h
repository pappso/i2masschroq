/**
 * \file utils/groupstore.h
 * \date 28/3/2017
 * \author Olivier Langella
 * \brief store unique version of groups
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "../grouping/groupinggroup.h"

#pragma once

#include <map>

class GroupStore
{
  public:
  GroupStore();
  ~GroupStore();
  GroupingGroupSp getInstance(unsigned int group_number);
  const std::map<unsigned int, GroupingGroupSp> &getGroupMap() const;

  void clear();
  std::size_t countGroup() const;
  std::size_t countSubGroup() const;

  private:
  std::map<unsigned int, GroupingGroupSp> _map_group;
};
