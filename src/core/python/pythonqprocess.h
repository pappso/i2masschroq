/**
 * \file core/python/pythonqprocess.h
 * \date 23/06/2023
 * \author Olivier Langella
 * \brief Overload the Qprocess for the python process
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include <QProcess>
#include <QTextStream>

// python3 -m venv ./compomic
//./compomic/bin/pip install ms2pip
//./compomic/bin/pip install deeplc


class PythonQProcess : public QProcess
{
  Q_OBJECT
  public:
  PythonQProcess(QObject *parent = nullptr);
  ~PythonQProcess();
  void startRprocess();
  void stopRprocess();

  qint64 executeOnTheRecord(const QString &mcqr_code);
  qint64 executeOffTheRecord(const QString &mcqr_otr);
  void executeMcqrStep(const QString &mcqr_step_name, const QString &mcqr_step);
  const QString &getRscriptCode() const;


  private:
  QString m_rscriptCode;
  QTextStream m_rscriptTextStream;
};
