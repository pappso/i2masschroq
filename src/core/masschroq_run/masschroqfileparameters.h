/**
 * \file src/core/masschroq_run/masschroqfileparameters.h
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief all needed information to write a MassChroqML file
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../utils/types.h"
#include <QString>
#include <pappsomspp/masschroq/alignmentmethod.h>
#include <pappsomspp/masschroq/quantificationmethod.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include "../../core/msrun.h"

struct MasschroqFileParameters;
typedef std::shared_ptr<MasschroqFileParameters> MasschroqFileParametersSp;


struct MasschroqFileParameters
{
  void save() const;
  void load(ProposedMassSpectrometerParameterFamily family);

  /** @brief quantification result output file name*/
  // QString result_file_name;

  /** @brief quantification result output file type*/
  TableFileFormat result_file_format = TableFileFormat::tsv;

  bool export_compar_file = false;
  // QString compar_file_name;
  // TableFileFormat compar_file_format = TableFileFormat::ods;

  bool write_alignment_times;
  QString alignment_times_directory;

  pappso::masschroq::AlignmentMethod m_alignmentMethod =
    pappso::masschroq::AlignmentMethod("my_ms2");

  pappso::masschroq::QuantificationMethod m_quantificationMethod =
    pappso::masschroq::QuantificationMethod("quant1");

  std::vector<MsRunAlignmentGroupSp> alignment_groups;


  ProposedMassSpectrometerParameterFamily m_family =
    ProposedMassSpectrometerParameterFamily::undefined;
};
