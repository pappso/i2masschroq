/**
 * \file core/masschroq_run/masschorqslurmprocess.h
 * \date 21/06/2024
 * \author Olivier Langella
 * \brief handles execution of a bunch of MassChroQ process through slurm
 * workload manager
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "masschroqbatchprocess.h"
#include <QTemporaryDir>
#include "../../utils/workmonitor.h"
#include "../slurm_process/slurmprocess.h"

/**
 * @todo write docs
 */
class MassChroQSlurmProcess : public MassChroQBatchProcess
{
  public:
  MassChroQSlurmProcess(MainWindow *p_main_window,
                        WorkMonitorInterface *p_monitor,
                        const MassChroQRunBatch masschroq_batch_param);
  virtual ~MassChroQSlurmProcess();

  virtual void run() override;

  private:
  void runExport();

  private:
  WorkMonitorInterface *mp_workMonitor = nullptr;
  std::size_t m_slurmRequestMemory;
};
