/**
 * \file src/core/masschroq_run/masschroqfileparameters.cpp
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief all needed information to write a MassChroqML file
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "masschroqfileparameters.h"
#include <QSettings>

void
MasschroqFileParameters::save() const
{
  qDebug();
  QSettings settings;
  settings.setValue("export_masschroqml/result_file_format",
                    (std::uint8_t)result_file_format);


  settings.setValue("export_masschroqml/export_compar_file",
                    QString("%1").arg(export_compar_file));

  settings.setValue("export_masschroqml/write_alignment_times",
                    write_alignment_times);
}
void
MasschroqFileParameters::load(ProposedMassSpectrometerParameterFamily family)
{
  qDebug() << (uint8_t)m_family << " vs " << (uint8_t)family;

  m_quantificationMethod.setIsotopeMinimumRatio(0.8);
  std::shared_ptr<pappso::TraceDetectionZivy> sp_detection_zivy =
    std::make_shared<pappso::TraceDetectionZivy>(1, 3, 2, 5000, 3000);
  m_quantificationMethod.setTraceDetectionInterfaceCstSPtr(sp_detection_zivy);

  if(family == ProposedMassSpectrometerParameterFamily::BrukerTimsTOF)
    {
      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>(
          "passQuantileBasedRemoveY|0.6"));
    }
  else if(family == ProposedMassSpectrometerParameterFamily::ThermoQexLumos)
    {
      //<anti_spike half="5"/>
      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>("antiSpike|5"));
    }
  else if(family == ProposedMassSpectrometerParameterFamily::undefined)
    {
      //<anti_spike half="5"/>
      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>("antiSpike|5"));
    }

  if(m_quantificationMethod.getXicExtractionLowerPrecisionPtr() == nullptr)
    {
      // new settings
      qDebug() << "new settings";
    }
  else
    {
      if(m_family == family)
        {
          // do not reset settings
          qDebug() << "no reset";
          return;
        }
      else
        {
          // replace settings with different spectrometer family
          qDebug() << "reset";
        }
    }
  m_family = family;

  QSettings settings;
  /*
  result_file_format =
    (TableFileFormat)settings
      .value("export_masschroqml/result_file_format",
             QString("%1").arg((std::uint8_t)TableFileFormat::tsv))
      .toUInt();
*/
  result_file_format = TableFileFormat::tsv;

  export_compar_file =
    settings
      .value("export_masschroqml/compar_file_name", QString("%1").arg(true))
      .toBool();

  write_alignment_times = settings
                            .value("export_masschroqml/write_alignment_times",
                                   QString("%1").arg(false))
                            .toBool();
  m_alignmentMethod.setMs2TendencyWindow(10);
  m_alignmentMethod.setMs2SmoothingWindow(15);
  m_alignmentMethod.setMs1SmoothingWindow(0);

  m_quantificationMethod.setXicExtractMethod(pappso::XicExtractMethod::max);


  m_quantificationMethod.setXicFilter(
    std::make_shared<pappso::FilterSuiteString>(
      "passQuantileBasedRemoveY|0.6"));
  if(family == ProposedMassSpectrometerParameterFamily::BrukerTimsTOF)
    {

      sp_detection_zivy =
        std::make_shared<pappso::TraceDetectionZivy>(1, 1, 1, 50, 30);
      m_quantificationMethod.setTraceDetectionInterfaceCstSPtr(
        sp_detection_zivy);


      m_quantificationMethod.setXicExtractionLowerPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(20));
      m_quantificationMethod.setXicExtractionUpperPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(20));

      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>(
          "passQuantileBasedRemoveY|0.6"));
    }
  else if(family == ProposedMassSpectrometerParameterFamily::ThermoQexLumos)
    {

      sp_detection_zivy =
        std::make_shared<pappso::TraceDetectionZivy>(1, 3, 2, 5000, 3000);
      m_quantificationMethod.setTraceDetectionInterfaceCstSPtr(
        sp_detection_zivy);


      m_quantificationMethod.setXicExtractionUpperPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(10));
      m_quantificationMethod.setXicExtractionLowerPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(10));
      //<anti_spike half="5"/>
      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>("antiSpike|5"));
    }
  else if(family == ProposedMassSpectrometerParameterFamily::undefined)
    {

      sp_detection_zivy =
        std::make_shared<pappso::TraceDetectionZivy>(1, 3, 2, 5000, 3000);
      m_quantificationMethod.setTraceDetectionInterfaceCstSPtr(
        sp_detection_zivy);


      m_quantificationMethod.setXicExtractionUpperPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(10));
      m_quantificationMethod.setXicExtractionLowerPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(10));
      //<anti_spike half="5"/>
      m_quantificationMethod.setXicFilter(
        std::make_shared<pappso::FilterSuiteString>("antiSpike|5"));
    }
}
