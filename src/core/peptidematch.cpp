
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidematch.h"
#include <pappsomspp/pappsoexception.h>


PeptideMatch::PeptideMatch()
{
}

PeptideMatch::PeptideMatch(const PeptideMatch &other)
{
  this->operator=(other);
}
bool
PeptideMatch::operator==(const PeptideMatch &other) const
{
  if((_p_peptide_evidence == other._p_peptide_evidence) &&
     (_start == other._start))
    {
      return true;
    }
  return false;
}

PeptideMatch &
PeptideMatch::operator=(const PeptideMatch &other)
{

  _start              = other._start;
  _p_peptide_evidence = other._p_peptide_evidence;
  return *this;
}


void
PeptideMatch::setPeptideEvidenceSp(PeptideEvidenceSp sp_peptide_evidence)
{
  _p_peptide_evidence = sp_peptide_evidence.get();
}
const PeptideEvidence *
PeptideMatch::getPeptideEvidence() const
{
  return _p_peptide_evidence;
}
PeptideEvidence *
PeptideMatch::getPeptideEvidence()
{
  return _p_peptide_evidence;
}
void
PeptideMatch::setStart(unsigned int start)
{
  _start = start;
}
unsigned int
PeptideMatch::getStart() const
{
  return _start;
}
unsigned int
PeptideMatch::getStop() const
{
  return _start + _p_peptide_evidence->getPeptideXtpSp().get()->size();
}

bool
PeptideMatch::operator<(const PeptideMatch &r) const
{
  return std::tie(_start, _p_peptide_evidence) <
         std::tie(r._start, r._p_peptide_evidence); // keep the same order
}

bool
PeptideMatch::containsPosition(unsigned int position) const
{
  if(position < _start)
    {
      return false;
    }
  if(position < getStop())
    {
      return true;
    }
  return false;
}
