/**
 * \file /core/mcqr/mcqrmetadataset.cpp
 * \date 30/05/2023
 * \author Olivier Langella
 * \brief collection of McqrMetadata
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadataset.h"
#include <pappsomspp/exception/exceptionnotfound.h>

McqrMetadataSet::McqrMetadataSet()
{
}

McqrMetadataSet::McqrMetadataSet(const McqrMetadataSet &other)
{
  m_mcqrMetadataSet = other.m_mcqrMetadataSet;
}

McqrMetadataSet::~McqrMetadataSet()
{
}

void
McqrMetadataSet::push_back(const McqrMetadata &metadata)
{
  m_mcqrMetadataSet.push_back(metadata);
}

const McqrMetadata &
McqrMetadataSet::getMcqrMetadaByDataId(const QString &data_id) const
{
  auto it = std::find_if(
    m_mcqrMetadataSet.begin(),
    m_mcqrMetadataSet.end(),
    [data_id](const McqrMetadata &a) { return (a.m_msrunId == data_id); });

  if(it != m_mcqrMetadataSet.end())
    {
      return *it;
    }

  throw pappso::ExceptionNotFound(
    QObject::tr("data id %1 not found").arg(data_id));
}

void
McqrMetadataSet::addColumn(const QString &column_name)
{
  qDebug() << column_name;
  for(McqrMetadata &metadata : m_mcqrMetadataSet)
    {
      metadata.m_otherData.insert({column_name, QVariant()});

      qDebug() << metadata.m_otherData.at(column_name).isNull();
      qDebug() << metadata.m_otherData.at(column_name).toString();
    }
}

const std::vector<McqrMetadata> &
McqrMetadataSet::getMcqrMetadataList() const
{
  return m_mcqrMetadataSet;
}

QStringList
McqrMetadataSet::getOtherDataColumns() const
{
  QStringList column_list;
  for(const McqrMetadata &metadata : m_mcqrMetadataSet)
    {
      for(auto it_other_data : metadata.m_otherData)
        {
          qDebug() << it_other_data.first;
          if(!column_list.contains(it_other_data.first))
            {
              column_list << it_other_data.first;
            }
        }
    }
  qDebug() << column_list.join(" ");
  return column_list;
}

std::size_t
McqrMetadataSet::size() const
{
  return m_mcqrMetadataSet.size();
}

McqrMetadataSet &
McqrMetadataSet::operator=(const McqrMetadataSet &other)
{
  m_mcqrMetadataSet = other.m_mcqrMetadataSet;
  return (*this);
}

QVector<QVariant>
McqrMetadataSet::getValuesFromFactor(const QString &factor) const
{
  QVector<QVariant> values;

  for(const McqrMetadata &metadata : m_mcqrMetadataSet)
    {
      QVariant value = metadata.getQVariantByColumnName(factor);
      if(!value.isNull())
        values.push_back(value);
    }

  return values;
}

QStringList
McqrMetadataSet::getLevelsFromFactorList(const QStringList &factor_list) const
{
  QStringList level_list;

  for(const McqrMetadata &metadata : m_mcqrMetadataSet)
    {
      QStringList one_level;
      for(auto factor : factor_list)
        {
          QVariant value = metadata.getQVariantByColumnName(factor);
          one_level << value.toString();
        }

      level_list << one_level.join("-");
    }
  return level_list;
}
