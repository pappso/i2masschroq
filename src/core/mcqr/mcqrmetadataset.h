/**
 * \file /core/mcqr/mcqrmetadataset.h
 * \date 30/05/2023
 * \author Olivier Langella
 * \brief collection of McqrMetadata
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "mcqrmetadata.h"

/**
 * @todo write docs
 */
class McqrMetadataSet
{
  public:
  /**
   * Default constructor
   */
  McqrMetadataSet();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  McqrMetadataSet(const McqrMetadataSet &other);

  McqrMetadataSet &operator=(const McqrMetadataSet &);
  /**
   * Destructor
   */
  ~McqrMetadataSet();

  void push_back(const McqrMetadata &metadata);

  const McqrMetadata &getMcqrMetadaByDataId(const QString &data_id) const;

  void addColumn(const QString &column_name);

  const std::vector<McqrMetadata> &getMcqrMetadataList() const;

  QStringList getOtherDataColumns() const;

  QVector<QVariant> getValuesFromFactor(const QString &factor) const;

  QStringList getLevelsFromFactorList(const QStringList &factor_list) const;

  std::size_t size() const;

  private:
  std::vector<McqrMetadata> m_mcqrMetadataSet;
};
