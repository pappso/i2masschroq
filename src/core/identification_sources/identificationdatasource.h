
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once


#include <pappsomspp/massspectrum/massspectrum.h>
#include <memory>
#include <QVariant>
#include "../msrun.h"
#include <pappsomspp/processing/project/projectparameters.h>
#include "../../utils/fastafilestore.h"
#include "../../utils/peptideevidencestore.h"

class Project;

class IdentificationDataSource;
typedef std::shared_ptr<IdentificationDataSource> IdentificationDataSourceSp;


class IdentificationDataSource
{
  public:
  IdentificationDataSource(const QString resource_name);
  IdentificationDataSource(const IdentificationDataSource &other);
  virtual ~IdentificationDataSource();

  PeptideEvidenceStore &getPeptideEvidenceStore();
  const PeptideEvidenceStore &getPeptideEvidenceStore() const;

  void setXmlId(const QString xmlid);
  const QString &getXmlId() const;

  /** @brief URL or filename containing identification data
   * */
  const QString &getResourceName() const;

  /** @brief get biological sample name
   * */
  const QString getSampleName() const;
  void setMsRunSp(MsRunSp ms_run_sp);
  virtual MsRunSp getMsRunSp() const;


  /** \brief read source content to store it in project
   * @param p_monitor monitor parsing
   * @param p_project the project to add PSMs
   */
  virtual void parseTo(pappso::UiMonitorInterface *p_monitor,
                       Project *p_project) = 0;

  /** \brief set identification engine
   */
  virtual void setIdentificationEngine(IdentificationEngine engine);

  /** \brief identification engine
   */
  virtual IdentificationEngine getIdentificationEngine() const;

  /** \brief identification engine name
   */
  const QString getIdentificationEngineName() const;

  /** \brief identification engine version
   */
  virtual const QString &getIdentificationEngineVersion() const;
  /** \brief set identification engine version
   */
  virtual void setIdentificationEngineVersion(const QString &version);

  /** \brief set identification engine parameter value
   */
  virtual void setIdentificationEngineParam(IdentificationEngineParam param,
                                            const QVariant &value);


  /** \brief get specific identification engine parameter value
   */
  virtual const QVariant
  getIdentificationEngineParam(IdentificationEngineParam param) const;


  /** \brief set identification engine statistics
   * any statistics on this identification run that can be told by the
   * identification engine
   */
  virtual void
  setIdentificationEngineStatistics(IdentificationEngineStatistics param,
                                    const QVariant &value);


  /** \brief get specific identification engine statistics value
   */
  virtual const QVariant
  getIdentificationEngineStatistics(IdentificationEngineStatistics param) const;

  /** \brief add Fastafile used by the identification engine
   */
  void addFastaFile(FastaFileSp file);

  const std::vector<FastaFileSp> &getFastaFileList() const;


  /** \brief get identification engine statistics map
   */
  virtual const std::map<IdentificationEngineStatistics, QVariant> &
  getIdentificationEngineStatisticsMap() const;

  /** \brief get identification engine param map
   */
  virtual const std::map<IdentificationEngineParam, QVariant> &
  getIdentificationEngineParamMap() const;

  virtual bool
  isValid(const PeptideEvidence *p_peptide_evidence,
          const AutomaticFilterParameters &automatic_filter_parameters) const;


  virtual void
  fillProjectParameters(pappso::ProjectParameters &parameters) const;


  protected:
  QString _resource_name;
  IdentificationEngine m_identificationEngine = IdentificationEngine::unknown;
  MsRunSp msp_msRun                           = nullptr;

  private:
  // static std::map<QString, pappso::MsRunIdSp> _map_msrunidsp;
  QString _xml_id;
  QString _version;
  std::map<IdentificationEngineParam, QVariant> _params;
  std::map<IdentificationEngineStatistics, QVariant> _param_stats;
  std::vector<FastaFileSp> _fastafile_list;

  PeptideEvidenceStore _peptide_evidence_store;
};
