/**
 * \file src/core/identification_sources/identificationmzidentmlfile.cpp
 * \date 20/1/2021
 * \author Olivier Langella
 * \brief parse mzIdentML file
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "identificationmzidentmlfile.h"
#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/mzidentml/msrunlocationreader.h"
#include "../../input/mzidentml/mzidentmlreader.h"

IdentificationMzIdentMlFile::IdentificationMzIdentMlFile(
  const QFileInfo &mzidentmlFile, const MsRunSp &msrun)
  : IdentificationDataSource(mzidentmlFile.absoluteFilePath()),
    m_mzidentmlFile(mzidentmlFile)
{
  m_identificationEngine = IdentificationEngine::unknown;
  setMsRunSp(msrun);
}

IdentificationMzIdentMlFile::IdentificationMzIdentMlFile(
  const IdentificationMzIdentMlFile &other)
  : IdentificationDataSource(other), m_mzidentmlFile(other.m_mzidentmlFile)
{
  qDebug();
  m_identificationEngine = other.m_identificationEngine;
}

IdentificationMzIdentMlFile::~IdentificationMzIdentMlFile()
{
}

void
IdentificationMzIdentMlFile::parseTo(pappso::UiMonitorInterface *p_monitor,
                                     Project *p_project)
{
  qDebug();


  qDebug();
}

void
IdentificationMzIdentMlFile::fillProjectParameters(
  pappso::ProjectParameters &parameters) const
{

  MsRunLocationReader msrun_location_reader;

  if(msrun_location_reader.readFile(m_mzidentmlFile.absoluteFilePath()))
    {
      parameters.merge(msrun_location_reader.getProjectParameters());
    }
  else
    {
      qDebug() << msrun_location_reader.errorString();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 mzIdentML file :\n %2")
          .arg(m_mzidentmlFile.absoluteFilePath())
          .arg(msrun_location_reader.errorString()));
    }
}

void
IdentificationMzIdentMlFile::getMzIdentMlFileMsRunSp(Project *p_project)
{
  MsRunLocationReader msrun_location_reader;

  if(msrun_location_reader.readFile(m_mzidentmlFile.absoluteFilePath()))
    {
      if(msrun_location_reader.getSpectraDataList().size() != 1)
        {
          throw pappso::PappsoException(QObject::tr(
            "Unable to read mzIdentML files containing multiple samples"));
        }
      MsRunSp msrun_sp = p_project->getMsRunStore().getInstance(
        msrun_location_reader.getSpectraDataList().front().location);
      setMsRunSp(msrun_sp);
    }
  else
    {
      qDebug() << msrun_location_reader.errorString();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 mzIdentML file :\n %2")
          .arg(m_mzidentmlFile.absoluteFilePath())
          .arg(msrun_location_reader.errorString()));
    }
}
