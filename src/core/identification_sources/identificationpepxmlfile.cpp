/**
 * \file /core/identification_sources/identificationpepxmlfile.cpp
 * \date 16/6/2018
 * \author Olivier Langella
 * \brief pep xml identification file handler
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "identificationpepxmlfile.h"

#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/pepxml/pepxmlreader.h"

IdentificationPepXmlFile::IdentificationPepXmlFile(
  const QFileInfo &mascot_dat_file)
  : IdentificationDataSource(mascot_dat_file.absoluteFilePath()),
    _pep_xml_file(mascot_dat_file)
{
  m_identificationEngine = IdentificationEngine::unknown;
}

IdentificationPepXmlFile::IdentificationPepXmlFile(
  const IdentificationPepXmlFile &other)
  : IdentificationDataSource(other), _pep_xml_file(other._pep_xml_file)
{
  m_identificationEngine = other.m_identificationEngine;
}

IdentificationPepXmlFile::~IdentificationPepXmlFile()
{
}


void
IdentificationPepXmlFile::parseTo(pappso::UiMonitorInterface *p_monitor,
                                  Project *p_project)
{
  qDebug();


  MsRunSp msrun_sp =
    p_project->getMsRunStore().getInstance(QFileInfo(_pep_xml_file).baseName());
  setMsRunSp(msrun_sp);
  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msrun_sp.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }

  identification_group_p->addIdentificationDataSourceP(this);


  PepXmlReader pepxml_reader(
    p_monitor, p_project, identification_group_p, this);

  if(!pepxml_reader.readFile(_pep_xml_file.absoluteFilePath()))
    {

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!pep xml file :\n %2")
          .arg(_pep_xml_file.absoluteFilePath())
          .arg(pepxml_reader.errorString()));
    }


  qDebug();
}
