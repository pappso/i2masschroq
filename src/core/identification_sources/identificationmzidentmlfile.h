/**
 * \file src/core/identification_sources/identificationmzidentmlfile.h
 * \date 20/1/2021
 * \author Olivier Langella
 * \brief parse mzIdentML file
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once
#include "identificationdatasource.h"
#include <QFileInfo>


class IdentificationMzIdentMlFile;
typedef std::shared_ptr<IdentificationMzIdentMlFile>
  IdentificationMzIdentMlFileSp;

class IdentificationMzIdentMlFile : public IdentificationDataSource
{
  public:
  IdentificationMzIdentMlFile(const QFileInfo &mzidentmlFile,
                              const MsRunSp &msrun);
  IdentificationMzIdentMlFile(const IdentificationMzIdentMlFile &other);
  virtual ~IdentificationMzIdentMlFile();

  virtual void parseTo(pappso::UiMonitorInterface *p_monitor,
                       Project *p_project) override;

  virtual void
  fillProjectParameters(pappso::ProjectParameters &parameters) const override;

  private:
  /** @brief before parsing the file, we need to know on which mz raw data file
   * the identification was made
   */
  void getMzIdentMlFileMsRunSp(Project *p_project);

  private:
  const QFileInfo m_mzidentmlFile;
};
