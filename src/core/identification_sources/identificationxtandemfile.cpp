
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "identificationxtandemfile.h"
#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/tandem/tandeminfoparser.h"
#include "../../input/tandem/tandemreader.h"

IdentificationXtandemFile::IdentificationXtandemFile(
  const QFileInfo &xtandem_file)
  : IdentificationDataSource(xtandem_file.absoluteFilePath()),
    _xtandem_file(xtandem_file)
{
  m_identificationEngine = IdentificationEngine::XTandem;
}

IdentificationXtandemFile::IdentificationXtandemFile(
  const IdentificationXtandemFile &other)
  : IdentificationDataSource(other), _xtandem_file(other._xtandem_file)
{
  m_identificationEngine = IdentificationEngine::XTandem;
}

IdentificationXtandemFile::~IdentificationXtandemFile()
{
}

void
IdentificationXtandemFile::parseTo(pappso::UiMonitorInterface *p_monitor,
                                   Project *p_project)
{
  qDebug() << "begin";

  qDebug() << "Read X!Tandem XML result file '"
           << _xtandem_file.absoluteFilePath() << "'";
  TandemInfoParser tandem_info_parser;

  if(!tandem_info_parser.readFile(_xtandem_file.absoluteFilePath()))
    {

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem file :\n %2")
          .arg(_xtandem_file.absoluteFilePath())
          .arg(tandem_info_parser.errorString()));
    }

  MsRunSp msrun_sp = p_project->getMsRunStore().getInstance(
    tandem_info_parser.getSpectraDataLocation());
  msrun_sp.get()->setMsDataFormat(tandem_info_parser.getMsDataFormat());
  setMsRunSp(msrun_sp);

  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msrun_sp.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }

  identification_group_p->addIdentificationDataSourceP(this);

  p_monitor->setTotalSteps(tandem_info_parser.getModelCount());

  TandemReader tandem_reader(p_monitor,
                             p_project,
                             identification_group_p,
                             this,
                             tandem_info_parser.getResidueMassModifications());

  if(!tandem_reader.readFile(_xtandem_file.absoluteFilePath()))
    {

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem file :\n %2")
          .arg(_xtandem_file.absoluteFilePath())
          .arg(tandem_reader.errorString()));
    }

  qDebug() << "end";
}

void
IdentificationXtandemFile::setTimstofMs2CentroidParameters(
  const QString &centroid_parameters)
{
  qDebug() << centroid_parameters;
  m_centroidOptions = centroid_parameters;
  /*
    double resolution,              //!< instrument resolution
    double smoothwidth        = 2., //!< smoothwidth
    double integrationWidth   = 4,  //! integration width
    double intensityThreshold = 10.,
    */

  if(msp_msRun.get() != nullptr)
    {
      //_ms_run_sp.get()->setTimstofMs2CentroidParameters(m_centroidOptions);
    }
}

void
IdentificationXtandemFile::fillProjectParameters(
  pappso::ProjectParameters &parameters) const
{

  TandemInfoParser msrun_location_reader;

  if(msrun_location_reader.readFile(_xtandem_file.absoluteFilePath()))
    {
      parameters.merge(msrun_location_reader.getProjectParameters());
    }
  else
    {
      qDebug() << msrun_location_reader.errorString();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem file :\n %2")
          .arg(_xtandem_file.absoluteFilePath())
          .arg(msrun_location_reader.errorString()));
    }
}
