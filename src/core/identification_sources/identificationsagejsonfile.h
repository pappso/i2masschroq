/**
 * \file core/identification_sources/identificationsagejsonfile.h
 * \date 21/08/2024
 * \author Olivier Langella
 * \brief read data from Sage json output file
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "identificationdatasource.h"
#include <QJsonDocument>


class IdentificationSageJsonFile;
typedef std::shared_ptr<IdentificationSageJsonFile>
  IdentificationSageJsonFileSp;
/**
 * @todo write docs
 */
class IdentificationSageJsonFile : public IdentificationDataSource
{
  public:
  IdentificationSageJsonFile(const QFileInfo &sage_json_file,
                             const QJsonDocument &jsondoc,
                             const MsRunSp &msrun);
  IdentificationSageJsonFile(const QFileInfo &sage_json_file,
                             const MsRunSp &msrun);
  IdentificationSageJsonFile(const IdentificationSageJsonFile &other);

  virtual void parseTo(pappso::UiMonitorInterface *p_monitor,
                       Project *p_project) override;

  virtual void
  fillProjectParameters(pappso::ProjectParameters &parameters) const override;
  /**
   * Destructor
   */
  virtual ~IdentificationSageJsonFile();

  const QJsonDocument &getJsonDocument() const;

  private:
  void readJson(Project *p_project);

  void recursiveFillProjectParameters(pappso::ProjectParameters &parameters,
                                      const QString &prefix,
                                      const QJsonObject &node) const;
  QString getSageJsonPrecision(const QJsonObject &node) const;
  QString getSageJsonJoinArray(const QJsonArray &array) const;
  QString getSageJsonStaticMods(const QJsonObject &node) const;
  QString getSageJsonVariableMods(const QJsonObject &node) const;

  private:
  const QFileInfo m_sageJsonFile;
  QJsonDocument m_jsonData;
};
