/**
 * \filed core/identification_sources/identificationpwizfile.h
 * \date 17/6/2017
 * \author Olivier Langella
 * \brief read identification files with proteowizard library
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "identificationdatasource.h"
#include <QFileInfo>

class IdentificationPwizFile : public IdentificationDataSource
{
  public:
  IdentificationPwizFile(const QFileInfo &ident_file);
  IdentificationPwizFile(const IdentificationPwizFile &other);
  ~IdentificationPwizFile();

  virtual void parseTo(pappso::UiMonitorInterface *p_monitor,
                       Project *p_project) override;

  private:
  const QFileInfo _ident_file;
};
