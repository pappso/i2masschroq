/**
 * \file core/labelingmethod.h
 * \date 20/5/2017
 * \author Olivier Langella
 * \brief labeling method
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QString>
#include <QDomNode>
#include <vector>
#include <memory>
#include <QXmlStreamWriter>
#include "label.h"

class LabelingMethod;
typedef std::shared_ptr<LabelingMethod> LabelingMethodSp;

class LabelingMethod
{
  public:
  LabelingMethod(const QString &method_id);
  LabelingMethod(const LabelingMethod &other);
  virtual ~LabelingMethod();
  LabelingMethodSp makeLabelingMethodSp() const;
  const Label *
  getLabel(const std::list<pappso::AaModificationP> &modification_set) const;
  const Label *getLabel(const QString &xml_id) const;
  const QString &getXmlId() const;
  void writeMassChroqMl(QXmlStreamWriter *output_stream) const;
  const std::vector<Label *> &getLabelList() const;

  const Label *getEmptyLabel() const;

  private:
  void parseMethod(QDomNode &method_node);
  void parseLabel(QDomNode &method_node);

  private:
  const QString _xml_id;
  std::vector<Label *> _label_list;
};
