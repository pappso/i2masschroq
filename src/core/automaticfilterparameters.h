
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <pappsomspp/types.h>
#include "../utils/types.h"
#include <pappsomspp/processing/project/projectparameters.h>


class AutomaticFilterParameters
{
  public:
  AutomaticFilterParameters();
  AutomaticFilterParameters(const AutomaticFilterParameters &other);
  ~AutomaticFilterParameters();

  AutomaticFilterParameters &operator=(const AutomaticFilterParameters &);


  void setFilterPeptideEvalue(pappso::pappso_double evalue);
  void setFilterPeptideFDR(double fdr);
  void setFilterProteinEvalue(pappso::pappso_double evalue);
  void setFilterProteinQvalue(double qvalue);
  void setFilterMinimumPeptidePerMatch(unsigned int number);

  /** @brief filter on peptide reproducibility accross MS runs
   * if a peptide is observed in less MS runs than this value, the peptide is
   * not valid
   */
  void setFilterPeptideObservedInLessSamplesThan(unsigned int number);
  void setFilterCrossSamplePeptideNumber(bool cross);

  pappso::pappso_double getFilterPeptideEvalue() const;
  double getFilterPeptideFDR() const;
  pappso::pappso_double getFilterProteinEvalue() const;
  double getFilterProteinQvalue() const;
  unsigned int getFilterMinimumPeptidePerMatch() const;
  bool getFilterCrossSamplePeptideNumber() const;
  unsigned int getFilterPeptideObservedInLessSamplesThan() const;

  AutomaticFilterType getAutomaticFilterType() const;
  void setAutomaticFilterType(AutomaticFilterType);

  double getSvmprobThreshold() const;
  void setSvmprobThreshold(double svmprob);

  double getProteinSvmprobThreshold() const;
  void setProteinSvmprobThreshold(double svmprob);


  void fillProjectParameters(pappso::ProjectParameters &parameters) const;

  private:
  double m_filterFDR                                   = -1;
  pappso::pappso_double _filter_minimum_peptide_evalue = 1;
  pappso::pappso_double _filter_minimum_protein_evalue = 1;
  double m_filterProteinQvalue                         = 0.01;
  unsigned int _filter_minimum_peptide_per_match       = 1;
  /** @brief peprepro filter
   * consider a peptide only when it is observed in less than n samples
   */
  unsigned int m_filter_peptide_observed_in_less_samples_than = 1;
  bool _filter_is_cross_sample_peptide_number                 = false;
  double m_svmprobThreshold                                   = 0;
  double m_proteinProbThreshold                               = 0;

  AutomaticFilterType m_filterType = AutomaticFilterType::evalue;
};
