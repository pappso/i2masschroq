
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "automaticfilterparameters.h"

AutomaticFilterParameters::AutomaticFilterParameters()
{
}

AutomaticFilterParameters::AutomaticFilterParameters(
  const AutomaticFilterParameters &other)
{
  this->operator=(other);
}

AutomaticFilterParameters::~AutomaticFilterParameters()
{
}

AutomaticFilterParameters &
AutomaticFilterParameters::operator=(const AutomaticFilterParameters &other)
{
  _filter_minimum_peptide_evalue    = other._filter_minimum_peptide_evalue;
  _filter_minimum_protein_evalue    = other._filter_minimum_protein_evalue;
  _filter_minimum_peptide_per_match = other._filter_minimum_peptide_per_match;
  _filter_is_cross_sample_peptide_number =
    other._filter_is_cross_sample_peptide_number;
  m_filter_peptide_observed_in_less_samples_than =
    other.m_filter_peptide_observed_in_less_samples_than;
  m_filterFDR            = other.m_filterFDR;
  m_svmprobThreshold     = other.m_svmprobThreshold;
  m_filterType           = other.m_filterType;
  m_proteinProbThreshold = other.m_proteinProbThreshold;
  m_filterProteinQvalue  = other.m_filterProteinQvalue;

  return *this;
}

void
AutomaticFilterParameters::setFilterProteinQvalue(double qvalue)
{
  if(qvalue > 0)
    {
      m_filterProteinQvalue = qvalue;
    }
}

double
AutomaticFilterParameters::getFilterProteinQvalue() const
{
  return m_filterProteinQvalue;
}

void
AutomaticFilterParameters::setFilterPeptideFDR(double fdr)
{
  m_filterFDR = fdr;
}
void
AutomaticFilterParameters::setFilterPeptideEvalue(pappso::pappso_double evalue)
{
  _filter_minimum_peptide_evalue = evalue;
}

void
AutomaticFilterParameters::setFilterProteinEvalue(pappso::pappso_double evalue)
{
  _filter_minimum_protein_evalue = evalue;
}
void
AutomaticFilterParameters::setFilterMinimumPeptidePerMatch(unsigned int number)
{
  _filter_minimum_peptide_per_match = number;
}
void
AutomaticFilterParameters::setFilterCrossSamplePeptideNumber(bool cross)
{
  _filter_is_cross_sample_peptide_number = cross;
}
double
AutomaticFilterParameters::getFilterPeptideFDR() const
{
  return (m_filterFDR);
}
pappso::pappso_double
AutomaticFilterParameters::getFilterPeptideEvalue() const
{
  return (_filter_minimum_peptide_evalue);
}
pappso::pappso_double
AutomaticFilterParameters::getFilterProteinEvalue() const
{
  return (_filter_minimum_protein_evalue);
}
unsigned int
AutomaticFilterParameters::getFilterMinimumPeptidePerMatch() const
{
  return (_filter_minimum_peptide_per_match);
}
bool
AutomaticFilterParameters::getFilterCrossSamplePeptideNumber() const
{
  return _filter_is_cross_sample_peptide_number;
}


void
AutomaticFilterParameters::setFilterPeptideObservedInLessSamplesThan(
  unsigned int number)
{
  m_filter_peptide_observed_in_less_samples_than = number;
}
unsigned int
AutomaticFilterParameters::getFilterPeptideObservedInLessSamplesThan() const
{
  return (m_filter_peptide_observed_in_less_samples_than);
}

double
AutomaticFilterParameters::getSvmprobThreshold() const
{
  return m_svmprobThreshold;
}

void
AutomaticFilterParameters::setSvmprobThreshold(double svmprob)
{

  m_svmprobThreshold = svmprob;
}

AutomaticFilterType
AutomaticFilterParameters::getAutomaticFilterType() const
{
  return m_filterType;
}

void
AutomaticFilterParameters::setAutomaticFilterType(AutomaticFilterType type)
{
  m_filterType = type;
}

double
AutomaticFilterParameters::getProteinSvmprobThreshold() const
{
  return m_proteinProbThreshold;
}

void
AutomaticFilterParameters::setProteinSvmprobThreshold(double svmprob)
{
  m_proteinProbThreshold = svmprob;
}

void
AutomaticFilterParameters::fillProjectParameters(
  pappso::ProjectParameters &parameters) const
{
  pappso::ProjectParam param(
    {pappso::ProjectParamCategory::filter, "", QVariant()});
  if(m_filterType == AutomaticFilterType::evalue)
    {
      param.name = "i2m_minimum_peptide_evalue";
      param.value.setValue(_filter_minimum_peptide_evalue);
      parameters.setProjectParam(param);

      param.name = "i2m_minimum_peptides_per_protein";
      param.value.setValue(_filter_minimum_peptide_per_match);
      parameters.setProjectParam(param);

      param.name = "i2m_count_peptides_per_protein_accross_msruns";
      param.value.setValue(_filter_is_cross_sample_peptide_number);
      parameters.setProjectParam(param);

      param.name = "i2m_minimum_protein_log10_evalue";
      param.value.setValue(std::log10(_filter_minimum_protein_evalue));
      parameters.setProjectParam(param);

      param.name = "i2m_pep_repro";
      param.value.setValue(m_filter_peptide_observed_in_less_samples_than);
      parameters.setProjectParam(param);
    }
  else if(m_filterType == AutomaticFilterType::fdr)
    {
      param.name = "i2m_psm_fdr_threshold_target";
      param.value.setValue(m_filterFDR);
      parameters.setProjectParam(param);

      param.name = "i2m_minimum_peptides_per_protein";
      param.value.setValue(_filter_minimum_peptide_per_match);
      parameters.setProjectParam(param);

      param.name = "i2m_count_peptides_per_protein_accross_msruns";
      param.value.setValue(_filter_is_cross_sample_peptide_number);
      parameters.setProjectParam(param);

      param.name = "i2m_protein_qvalue_threshold";
      param.value.setValue(m_filterProteinQvalue);
      parameters.setProjectParam(param);

      param.name = "i2m_minimum_protein_log10_evalue";
      param.value.setValue(std::log10(_filter_minimum_protein_evalue));
      parameters.setProjectParam(param);

      param.name = "i2m_pep_repro";
      param.value.setValue(m_filter_peptide_observed_in_less_samples_than);
      parameters.setProjectParam(param);
    }
  else if(m_filterType == AutomaticFilterType::svmprob)
    {
    }
}
