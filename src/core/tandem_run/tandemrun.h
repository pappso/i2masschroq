/**
 * \file /core/tandem_run/tandemrun.h
 * \date 25/01/2020
 * \author Olivier Langella
 * \brief actually does really run tandem directly on Bruker's data
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QObject>
#include <QProcess>
#include <QTemporaryDir>
#include <QMutex>

#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>

class TandemRun : public QObject
{
  Q_OBJECT
  public:
  /** @brief prepare a tandem run
   * @param tandem_binary file path to tandem.exe if not set, a default value is
   * given in QSettings
   */
  TandemRun(const QString &tandem_binary);

  /** @brief run a tandem job
   *
   * The tandem input file *should* contain an additional input parameter called
   * "spectrum, timstof MS2 filters". The value of this parameters *must*
   * contain a string describing the FilterSuiteString to apply on TimsTOF MS2.
   * A default value of "chargeDeconvolution|0.02dalton" is recommended for this
   * additional tandem input parameter
   *
   * @param monitor user interface monitor
   * @param tandem_input_file tandem xml input file
   */
  void run(pappso::UiMonitorInterface &monitor, const QString &tandem_input_file);

  /**
   * Destructor
   */
  ~TandemRun();

  signals:
  void tandemProgressMessage(QString message);


  private:
  void setTandemBinaryPath(const QString &tandem_binary_path);

  /** @brief run a tandem job
   * @param tandem_input_file tandem xml input file
   */
  void runTandem(const QString &tandem_input_file);



  private slots:
  void readyReadStandardOutput();
  void readyReadStandardError();

  private:
  pappso::UiMonitorInterface *mp_monitor;
  QString m_tandemBinary;
  QString m_tandemVersion;
  QString m_tmpDir;
  int m_maxTandemRunTimeMs =
    1000; // If msecs is -1, this function will not time out.
  QProcess *m_xtProcess = nullptr;
};
