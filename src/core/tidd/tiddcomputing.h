/**
 * \file /output/tidd/tidddataoutput.h
 * \date 30/9/2022
 * \author Olivier Langella
 * \brief compute TIDD PSM probabilities
 * (https://pubmed.ncbi.nlm.nih.gov/35354356/)
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include "../mcqr/mcqrqprocess.h"
#include "tiddparameters.h"
#include <memory>
#include <QWidget>
#include <QTemporaryDir>
#include <cutelee/engine.h>
#include "../peptideevidence.h"


class TiddComputing;
typedef std::shared_ptr<TiddComputing> TiddComputingSp;

class Project;

/**
 * @todo write docs
 */

class TiddComputing : public QWidget
{
  Q_OBJECT

  public:
  /**
   * Default constructor
   */
  TiddComputing();

  void run(pappso::UiMonitorInterface &monitor, Project *project);

  /**
   * Destructor
   */
  virtual ~TiddComputing();


  void setTiddParameters(const TiddParameters &parameters);


  const TiddResults &getTiddResults() const;

  protected slots:
  void updateRscriptTiddOutput();
  void handleRscriptTiddMessages();

  private:
  void rProcess(const QString &feature_file, const QString &result_file);
  void rInstallTiddDependencies();
  void computeFeatures(Project *project);

  private:
  McqrQProcess *mp_rProcess              = nullptr;
  pappso::UiMonitorInterface *mp_monitor = nullptr;
  bool m_tiddFinished                    = false;

  TiddParameters m_parameters;

  TiddResults m_results;

  QTemporaryDir *mpa_temporaryTiddDir = nullptr;

  QFileInfo m_tiddFeatureTsvFile;

  Cutelee::Engine *mpa_gtEngine;

  std::vector<PeptideEvidence *> m_peptideEvidenceList;
};
