/**
 * \file /output/tidd/tiddparameters.h
 * \date 30/9/2022
 * \author Olivier Langella
 * \brief compute TIDD PSM probabilities
 * (https://pubmed.ncbi.nlm.nih.gov/35354356/)
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include <pappsomspp/precision.h>

struct TiddParameters
{
  pappso::PrecisionPtr m_ms2precision;
  double m_minimumMz      = 150;
  std::size_t m_trainSize = 1000;
  std::size_t m_iteration = 1;
  std::size_t m_kCross    = 3;
};


struct TiddResults
{
  double svmProbThreshold = 0;
  double fdr              = 0;
};
