/**
 * \file src/gui/xic_view/xicwindow.h
 * \date 11/1/2018
 * \author Olivier Langella
 * \brief XIC window
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAbstractButton>
#include <pappsomspp/xic/xic.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>

#include "../../core/peptideevidence.h"
#include "xic_widgets/zivydialog.h"
#include <qcustomplot.h>

class ProjectWindow;


namespace Ui
{
class XicWindow;
}

class XicBox;

class XicWindow : public QMainWindow
{
  Q_OBJECT
  friend XicBox;

  public:
  explicit XicWindow(ProjectWindow *parent = 0);
  ~XicWindow();

  void addXic(const PeptideEvidence *p_peptide_evidence);
  void addXicInMsRun(const PeptideEvidence *p_peptide_evidence,
                     MsRunSp msrun_sp);

  pappso::PrecisionPtr getXicExtractPrecision() const;
  pappso::XicExtractMethod getXicExtractionMethod() const;

  bool isRetentionTimeSeconds() const;

  void clear();
  public slots:
  void xicPrecisionChanged(pappso::PrecisionPtr precision);
  void rtUnitChanged(QAbstractButton *button);

  protected slots:
  void doEditZivyParams();
  void doAcceptedZivyDialog();
  void doXicExtractionMethodChanged(pappso::XicExtractMethod xic_method);
  void doExportXicToCsv();
  void doChangeLockXaxisRule();
  void doFitAllAxis(QString axis_name, QCPRange new_range);

  signals:
  void reExtractXicNeeded();
  void rtUnitChangeNeeded();
  void operateXicAreaToCsv(QString filename, std::vector<XicBox *> xic_boxs);


  protected:
  ProjectWindow *getProjectWindow();
  void removeXicBox(XicBox *xic_box);
  void xicDetect(const pappso::Xic &xic,
                 pappso::TraceDetectionSinkInterface *sink) const;

  private:
  ProjectWindow *_project_window;
  Ui::XicWindow *ui;
  bool m_lockXaxis = false;
  pappso::TraceDetectionInterfaceCstSPtr msp_detect_zivy;


  ZivyDialog *_p_zivy_dialog;

  bool m_removePeakBase = false;
};
