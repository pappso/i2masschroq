/**
 * \file src/gui/xic_view/xic_box/xicbox.h
 * \date 12/1/2018
 * \author Olivier Langella
 * \brief XIC box widget
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QWidget>
#include <QThread>
#include <pappsomspp/widget/xicwidget/xicwidget.h>
#include "../../../utils/types.h"
#include "../../../core/peptideevidence.h"
#include "../xicwindow.h"
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <qcustomplot.h>

namespace Ui
{
class XicBox;
}


struct XicBoxNaturalIsotope
{

  void computeMeanIntensityIn(const std::vector<std::size_t> &rt_list);

  bool contains(const pappso::TracePeakCstSPtr &peak) const;

  pappso::XicCstSPtr xic_sp;
  pappso::PeptideNaturalIsotopeAverageSp peptide_natural_isotope_sp;
  pappso::TracePeakCstSPtr matched_peak_sp;
  pappso::TracePeakCstSPtr one_peak_sp;
  std::vector<pappso::TracePeakCstSPtr> detected_peak_list;
  double meanIntensityInCurrentPeak = 0;
};

class XicBox : public QWidget
{
  Q_OBJECT
  public:
  explicit XicBox(XicWindow *parent = 0);
  virtual ~XicBox();
  void setPeptideEvidence(const PeptideEvidence *p_peptide_evidence);
  void setPeptideEvidenceInMsRun(const PeptideEvidence *p_peptide_evidence,
                                 MsRunSp msrun_sp);
  const PeptideEvidence *getPeptideEvidenceP() const;

  std::vector<XicBoxNaturalIsotope> getNaturalIsotopList() const;
  MsRunSp getMsRunSp();
  double getIsotopicFractionDistribution() const;

  void rescaleXicWidget(QString axis_name, QCPRange intensity_range);
  QCPRange getRtRange();
  void showLoadingIcon(bool loading_status);

  signals:
  void loadXic(MsRunSp p_msrun,
               std::vector<pappso::XicCoordSPtr> mz_list,
               MasschroqFileParametersSp masschroq_params_sp);
  void computeIsotopeMassList(pappso::PeptideSp peptide_sp,
                              unsigned int charge,
                              pappso::PrecisionPtr precision,
                              double minimum_isotope_pattern_ratio);
  void fitAxisRangeInXics(QString axis_name, QCPRange new_range);

  private slots:
  void remove();
  void reExtractXic();
  void setXic(std::vector<pappso::XicCoordSPtr> xic_sp_list);
  void setIsotopeMassList(
    std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotope_mass_list);
  void error(QString error_message);
  void extractXicInOtherMsRun();
  void setRetentionTime(double rt);
  void setXicPeakList(pappso::XicXicPeakPairList xic_peak_list);
  void onRtUnitChanged();
  void onXicWidgetClick(double rt, double intensity);
  void onIsotopicDistributionClick();
  void doGetNewRtRange(QCPRange new_range);
  void doGetNewIntensityRange(QCPRange new_range);

  private:
  void drawObservedAreaBars(
    const std::vector<pappso::TracePeakCstSPtr> &observed_peak_to_draw);

  /** @brief select common retention times between peaks
   * @param common_peak_rt_measure_list vector to fill
   * @param peak peak to consider
   * @param xic to consider
   */
  void addCommonRt(std::vector<std::size_t> &common_peak_rt_measure_list,
                   const pappso::TracePeak *peak,
                   const pappso::Xic *xic) const;

  private:
  Ui::XicBox *ui;
  QThread _xic_thread;
  QMovie *mp_loadingMovie;
  XicWindow *_p_xic_window;
  const PeptideEvidence *_p_peptide_evidence;
  MsRunSp _msrun_sp;
  QCPBars *m_theoreticalRatioBars = nullptr;
  // QCPBars *m_observedRatioBars    = nullptr;
  std::vector<const PeptideEvidence *> _peptide_evidence_list;
  std::vector<XicBoxNaturalIsotope> _natural_isotope_list;
  std::vector<pappso::XicWidget *> _xic_widget_list;

  QCPBars *m_observedAreaBars = nullptr;
  bool _scaled                = false;

  // QFrame * _popup_peak_info = nullptr;
};
