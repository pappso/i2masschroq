/**
 * \file src/gui/xic_view/xic_widgets/zivywidget.h
 * \date 29/5/2018
 * \author Olivier Langella
 * \brief Widget to edit Zivy quantification method
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QWidget>
#include "../../../core/masschroq_run/masschroqfileparameters.h"

namespace Ui
{
class ZivyWidget;
}


class ZivyWidget : public QWidget
{
  Q_OBJECT
  public:
  explicit ZivyWidget(QWidget *parent = 0);
  virtual ~ZivyWidget();

  void setZivyParams(const pappso::TraceDetectionZivy &detection_zivy);

  const pappso::TraceDetectionZivy getZivyParams() const;

  signals:
  void zivyChanged(pappso::TraceDetectionZivy zivy_params);

  protected slots:
  void doSpinboxChanged(int value);

  private:
  Ui::ZivyWidget *ui;
};
