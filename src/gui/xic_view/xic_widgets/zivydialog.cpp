/**
 * \file src/gui/xic_view/xic_widgets/zivydialog.h
 * \date 30/5/2018
 * \author Olivier Langella
 * \brief dialog window to edit Zivy quantification method
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "zivydialog.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QGroupBox>
#include <pappsomspp/pappsoexception.h>

ZivyDialog::ZivyDialog(QWidget *parent) : QDialog(parent)
{
  _p_zivy_widget     = new ZivyWidget(this);
  mp_xicExportWidget = new XicExportWidget(this);
  _p_button_box =
    new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

  connect(_p_button_box, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(_p_button_box, &QDialogButtonBox::rejected, this, &QDialog::reject);

  QVBoxLayout *mainLayout = new QVBoxLayout;

  // Zivy parameters GroupBox
  QVBoxLayout *zivy_Layout = new QVBoxLayout;
  zivy_Layout->addWidget(_p_zivy_widget);
  zivy_Layout->setContentsMargins(3, 3, 3, 3);
  QGroupBox *zivy_grp_box = new QGroupBox("XIC Plot Parameters");
  zivy_grp_box->setLayout(zivy_Layout);
  mainLayout->addWidget(zivy_grp_box);

  _p_display_filters = new QLabel(QString("XIC filters : %1").arg(""));
  mainLayout->addWidget(_p_display_filters);

  // XIC export parameters GroupBox
  QVBoxLayout *xic_Layout = new QVBoxLayout;
  xic_Layout->addWidget(mp_xicExportWidget);
  xic_Layout->setContentsMargins(3, 3, 3, 3);
  QGroupBox *xic_grp_box = new QGroupBox("XIC Export Parameters");

  xic_grp_box->setLayout(xic_Layout);
  mainLayout->addWidget(xic_grp_box);

  mainLayout->addWidget(_p_button_box);
  setLayout(mainLayout);

  setWindowTitle(tr("Edit parameters"));
}


ZivyDialog::~ZivyDialog()
{
  qDebug() << "ZivyDialog::~ZivyDialog";
}

void
ZivyDialog::setMasschroqFileParametersSp(
  const MasschroqFileParametersSp &params)
{
  msp_MasschroqFileParametersSp = params;
  _p_display_filters->setText(QString("XIC filters : %1").arg(""));
  if(params.get()->m_quantificationMethod.getFilterSuiteStringSPtr().get() !=
     nullptr)
    {
      _p_display_filters->setText(
        QString("XIC filters : %1")
          .arg(params.get()
                 ->m_quantificationMethod.getXicFilter()
                 .get()
                 ->toString()));
    }


  const pappso::TraceDetectionZivy *detection_zivy =
    dynamic_cast<const pappso::TraceDetectionZivy *>(
      params.get()
        ->m_quantificationMethod.getTraceDetectionInterfaceCstSPtr()
        .get());

  if(detection_zivy == nullptr)
    {
      throw pappso::PappsoException(
        "m_quantificationMethod.getTraceDetectionInterfaceCstSPtr().get() "
        "== "
        "nullptr");
    }
  _p_zivy_widget->setZivyParams(*detection_zivy);
}

void
ZivyDialog::updateProjectMasschroqFileParameters() const
{

  std::shared_ptr<pappso::TraceDetectionZivy> sp_detection_zivy =
    std::make_shared<pappso::TraceDetectionZivy>(
      _p_zivy_widget->getZivyParams());
  msp_MasschroqFileParametersSp.get()
    ->m_quantificationMethod.setTraceDetectionInterfaceCstSPtr(
      sp_detection_zivy);
}

void
ZivyDialog::setXicExportParams()
{
  mp_xicExportWidget->setXicExportParams();
}

void
ZivyDialog::saveXicExportSettings()
{
  mp_xicExportWidget->saveXicExportSettings();
}
