/**
 * \file src/gui/xic_view/xic_widgets/xicexportparam.h
 * \date 08/12/2020
 * \author Thomas Renne
 * \brief XIC export parameter widget
 */
/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QWidget>
#include <qcustomplot.h>

namespace Ui
{
class XicExportWidget;
}

class XicExportWidget : public QWidget
{
  public:
  XicExportWidget(QWidget *parent);
  ~XicExportWidget();

  void setXicExportParams();
  void saveXicExportSettings();

  private:
  Ui::XicExportWidget *ui;
};
