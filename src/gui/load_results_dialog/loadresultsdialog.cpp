
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "loadresultsdialog.h"

#include "ui_load_results_dialog.h"
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <pappsomspp/pappsoexception.h>


LoadResultsDialog::LoadResultsDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::LoadResultsDialog)
{
  qDebug() << "LoadResultsDialog::LoadResultsDialog begin";
  ui->setupUi(this);
  this->setModal(true);
  _p_file_list = new QStringListModel();

  QSettings settings;
  AutomaticFilterParameters param =
    ui->automatic_filter_widget->getAutomaticFilterParameters();
  param.setFilterCrossSamplePeptideNumber(
    settings.value("automatic_filter/cross_sample", "true").toBool());
  param.setFilterMinimumPeptidePerMatch(
    (unsigned int)settings.value("automatic_filter/peptide_number", "2")
      .toInt());
  param.setFilterPeptideEvalue(
    settings.value("automatic_filter/peptide_evalue", "0.05").toDouble());
  param.setFilterProteinEvalue(
    settings.value("automatic_filter/protein_evalue", "0.01").toDouble());
  ui->automatic_filter_widget->setAutomaticFilterParameters(param);

  setMinimumWidth(ui->automatic_filter_widget->getAutomaticFilterWindowWidth() +
                  200);

#if QT_VERSION >= 0x050000
  // Qt5 code
#else
  // Qt4 code

#endif

  qDebug() << "LoadResultsDialog::LoadResultsDialog end";
}

LoadResultsDialog::~LoadResultsDialog()
{
  delete _p_file_list;
  delete ui;
}

bool
LoadResultsDialog::isIndividual() const
{
  if(ui->individual_radio->isChecked())
    {
      return true;
    }
  return false;
}
QStringList
LoadResultsDialog::getFileList() const
{
  return _p_file_list->stringList();
}

AutomaticFilterParameters
LoadResultsDialog::getAutomaticFilterParameters() const
{
  return ui->automatic_filter_widget->getAutomaticFilterParameters();
}
void
LoadResultsDialog::clearFileList()
{
  _p_file_list->removeRows(0, _p_file_list->rowCount());
  ui->nb_files_label->setText("");
}
void
LoadResultsDialog::chooseFiles()
{
  try
    {
      QSettings settings;
      QString default_location =
        settings.value("path/identificationfiles", "").toString();

      QStringList filenames = QFileDialog::getOpenFileNames(
        this,
        tr("identification files"),
        default_location,
        tr("mzIdentML files (*.mzid);;SAGE, X!Tandem, Mascot DAT or pepXML "
           "files "
           "(*.json *.xml *.dat *.pepxml *.pepXML);;all files (*)"));

      if(filenames.size() > 0)
        {
          settings.setValue("path/identificationfiles",
                            QFileInfo(filenames[0]).absolutePath());
        }
      QStringList file_list = _p_file_list->stringList();
      file_list.append(filenames);
      _p_file_list->setStringList(file_list);
      ui->file_list_view->setModel(_p_file_list);
      ui->nb_files_label->setText("Number of files: " +
                                  QString::number(filenames.size()));
    }
  catch(pappso::PappsoException &error)
    {
      // QMessageBox::warning(this,
      //                  tr("Error choosing identification result files :
      //                  %1").arg(error.qwhat()), error);
    }
}


void
LoadResultsDialog::setProjectContaminants(Project *p_project) const
{
  ui->contaminant_widget->setProjectContaminants(p_project);
}
