/**
 * \file gui/export/export_spreadsheet_dialog/exportspreadsheetdialog.h
 * \date 11/4/2017
 * \author Olivier Langella
 * \brief choose ODS export options
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialog>
#include <QStringListModel>
#include "../../widgets/automatic_filter_widget/automaticfilterwidget.h"
#include "../../../core/project.h"


namespace Ui
{
class ExportSpreadsheetDialog;
}

class ExportSpreadsheetDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit ExportSpreadsheetDialog(QWidget *parent);
  ~ExportSpreadsheetDialog();

  QString getExportFormat() const;

  void setProject(const Project *p_project);

  bool isProjectParameters() const;
  bool isGroupingSheet() const;
  bool isSimpleSheet() const;
  bool isSampleSheet() const;
  bool isProteinSheet() const;
  bool isPtmIslandSheet() const;
  bool isPtmSpectraSheet() const;
  bool isQvaluesSheet() const;
  bool isMassDeltaSheet() const;
  bool isPeptidePosSheet() const;
  bool isValidPsmSheet() const;
  bool isSpectraSheetAll() const;
  bool isSpectraSheet() const;
  bool isPeptideSheet() const;


  public slots:
  void doSimpleProteinPeptideList(bool simple);
  void doProteinList(bool simple);
  void doGroups(bool simple);
  void doPeptidePos(bool simple);
  void doComparEmpai(bool simple);
  void doComparNsaf(bool simple);
  void doComparPai(bool simple);
  void doComparSpecificUniqueSequence(bool simple);
  void doComparUniqueSequence(bool simple);
  void doComparSpecificSpectra(bool simple);
  void doComparSpectra(bool simple);
  void doSamples(bool simple);
  void doSpectra(bool simple);
  void doSpectraIonMobilityInformations(bool simple);
  void doPsms(bool simple);
  void doValidPsms(bool simple);
  void doPeptides(bool simple);
  void doProteins(bool simple);
  void doPtmIslands(bool simple);
  void doPtmSpectra(bool simple);
  void doPeptidomicComparSpectra(bool simple);

  signals:

  private:
  Ui::ExportSpreadsheetDialog *ui;
};
