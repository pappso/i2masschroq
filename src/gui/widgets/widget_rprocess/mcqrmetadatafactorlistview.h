/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatafactorlistview.h
 * \date 14/09/2023
 * \author Olivier Langella
 * \brief metadata factor list view
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QListView>
#include <QStandardItemModel>
#include "../../../core/mcqr/mcqrmetadataset.h"

class McqrMetadataFactorListView : public QListView
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  McqrMetadataFactorListView(QWidget *parent = nullptr);

  /**
   * Destructor
   */
  virtual ~McqrMetadataFactorListView();

  void setMcqrMetadataSet(const McqrMetadataSet &);

  QStringList getQStringList() const;

  QStringList getLevels() const;

  private:
  QStandardItemModel *mp_listModel;
  McqrMetadataSet m_mcqrMetadataSet;
};
