/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatafactorcombobox.cpp
 * \date 15/09/2023
 * \author Olivier Langella
 * \brief metadata factor combo box
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadatafactorcombobox.h"

McqrMetadataFactorComboBox::McqrMetadataFactorComboBox(QWidget *parent)
  : QComboBox(parent)
{
}

McqrMetadataFactorComboBox::~McqrMetadataFactorComboBox()
{
}

void
McqrMetadataFactorComboBox::setMcqrMetadataSet(
  const McqrMetadataSet &metadata_set)
{
  this->clear();
  qDebug();
  for(QString factor : metadata_set.getOtherDataColumns())
    {
      qDebug() << factor;
      this->addItem(factor);
    }
}


void
McqrMetadataFactorComboBox::setMcqrMetadataSetFactorValues(
  const McqrMetadataSet &mcqr_metadataset, const QString &factor)
{
  qDebug() << factor;
  this->clear();
  if(!factor.isEmpty())
    {
      QStringList value_list_str;
      for(auto one_value : mcqr_metadataset.getValuesFromFactor(factor))
        {
          value_list_str << one_value.toString();
        }

      value_list_str.removeDuplicates();
      value_list_str.sort();

      for(QString one_value : value_list_str)
        {
          qDebug() << one_value;
          this->addItem(one_value);
        }
    }
}
