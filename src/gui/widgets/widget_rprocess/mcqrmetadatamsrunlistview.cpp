/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatamsrunlistview.cpp
 * \date 28/10/2023
 * \author Olivier Langella
 * \brief metadata MS run list view
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadatamsrunlistview.h"


McqrMetadataMsrunListView::McqrMetadataMsrunListView(QWidget *parent)
  : QListView(parent)
{
  mp_listModel = new QStandardItemModel(this);
}

McqrMetadataMsrunListView::~McqrMetadataMsrunListView()
{
}

void
McqrMetadataMsrunListView::setMcqrMetadataSet(
  const McqrMetadataSet &metadata_set)
{
  m_mcqrMetadataSet = metadata_set;
  mp_listModel->clear();

  int i = 0;
  for(McqrMetadata metadata : metadata_set.getMcqrMetadataList())
    {
      qDebug() << metadata.m_msrunFile;
      QStandardItem *new_factor_color = new QStandardItem(
        QString("%1 - %2").arg(metadata.m_msrunId).arg(metadata.m_msrunFile));
      new_factor_color->setCheckable(true);
      new_factor_color->setData(Qt::Unchecked, Qt::CheckStateRole);
      new_factor_color->setData(QVariant::fromValue(metadata));
      mp_listModel->setItem(i, new_factor_color);

      i++;
    }
  setModel(mp_listModel);
}

QStringList
McqrMetadataMsrunListView::getQStringList() const
{
  QStringList factors_list;
  for(int i = 0; i < mp_listModel->rowCount(); i++)
    {
      QStandardItem *color_item = mp_listModel->item(i, 0);
      if(color_item->checkState() == Qt::CheckState(Qt::Checked))
        {
          factors_list << color_item->data().value<McqrMetadata>().m_msrunId;
        }
    }
  return factors_list;
}


QString
McqrMetadataMsrunListView::getMsrunListString() const
{
  QString color_list;
  if(getQStringList().size() > 0)
    {
      color_list = QString("\"%1\"").arg(
        getQStringList().join("\",\""));
    }
  return color_list;
}
