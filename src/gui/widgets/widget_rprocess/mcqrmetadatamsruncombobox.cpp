/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatamsruncombobox.cpp
 * \date 30/10/2023
 * \author Olivier Langella
 * \brief msrun combo box
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadatamsruncombobox.h"
#include <QStandardItemModel>

McqrMetadataMsrunComboBox::McqrMetadataMsrunComboBox(QWidget *parent)
  : QComboBox(parent)
{
}

McqrMetadataMsrunComboBox::~McqrMetadataMsrunComboBox()
{
}

void
McqrMetadataMsrunComboBox::setMcqrMetadataSet(
  const McqrMetadataSet &metadata_set)
{
  this->clear();
  qDebug();


  for(McqrMetadata metadata : metadata_set.getMcqrMetadataList())
    {
      this->addItem(
        QString("%1 - %2").arg(metadata.m_msrunId).arg(metadata.m_msrunFile),
        QVariant::fromValue(metadata));
    }
}

QString
McqrMetadataMsrunComboBox::getMsrunId() const
{
  return this->currentData().value<McqrMetadata>().m_msrunId;
}
