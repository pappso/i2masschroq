/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatafactorcombobox.h
 * \date 15/09/2023
 * \author Olivier Langella
 * \brief metadata factor combo box
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QComboBox>
#include "../../../core/mcqr/mcqrmetadataset.h"

/**
 * @todo write docs
 */
class McqrMetadataFactorComboBox : public QComboBox
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  McqrMetadataFactorComboBox(QWidget *parent = nullptr);

  /**
   * Destructor
   */
  virtual ~McqrMetadataFactorComboBox();


  void setMcqrMetadataSet(const McqrMetadataSet &);
  void setMcqrMetadataSetFactorValues(const McqrMetadataSet &, const QString &);
};
