/**
 * \file /gui/widgets/widget_rprocess/mcqrmetadatafactorlistview.cpp
 * \date 14/09/2023
 * \author Olivier Langella
 * \brief metadata factor list view
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadatafactorlistview.h"

McqrMetadataFactorListView::McqrMetadataFactorListView(QWidget *parent)
  : QListView(parent)
{
  mp_listModel = new QStandardItemModel(this);
}

McqrMetadataFactorListView::~McqrMetadataFactorListView()
{
}

void
McqrMetadataFactorListView::setMcqrMetadataSet(
  const McqrMetadataSet &metadata_set)
{
  m_mcqrMetadataSet = metadata_set;
  mp_listModel->clear();

  int i = 0;
  for(QString factor : metadata_set.getOtherDataColumns())
    {
      QStandardItem *new_factor_color = new QStandardItem(factor);
      new_factor_color->setCheckable(true);
      new_factor_color->setData(Qt::Unchecked, Qt::CheckStateRole);
      mp_listModel->setItem(i, new_factor_color);

      i++;
    }
  setModel(mp_listModel);
}

QStringList
McqrMetadataFactorListView::getQStringList() const
{
  QStringList factors_list;
  for(int i = 0; i < mp_listModel->rowCount(); i++)
    {
      QStandardItem *color_item = mp_listModel->item(i, 0);
      if(color_item->checkState() == Qt::CheckState(Qt::Checked))
        {
          factors_list << color_item->text();
        }
    }
  return factors_list;
}

QStringList
McqrMetadataFactorListView::getLevels() const
{
  return m_mcqrMetadataSet.getLevelsFromFactorList(getQStringList());
}
