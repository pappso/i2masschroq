/**
 * \file /gui/widgets/widget_rprocess/widgetrprocess.h
 * \date 25/05/2023
 * \author Olivier Langella
 * \brief dialog window to drive R process
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QWidget>
#include <QTextDocument>
#include "../../../core/mcqr/mcqrqprocess.h"

namespace Ui
{
class WidgetRprocess;
}

class WidgetRprocess : public QWidget
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  explicit WidgetRprocess(QWidget *parent);

  /**
   * Destructor
   */
  virtual ~WidgetRprocess();
  // McqrQProcess *getRProcessRunning();


  void executeStepOnTheRecord(const QString &step, const QString &mcqr_code);
  void executeOnTheRecord(const QString &mcqr_code);
  const QString &getRscriptCode() const;
  const QString getRmdScriptCode(const QString &title) const;


  const QString &getMcqrVersion() const;

  bool isWorking() const;
  bool isFinished() const;

  void waitForStep(const QString &step_name);

  signals:
  void stepFinished(QString message);
  void stepFailed(QString message);


  protected:
  void closeEvent(QCloseEvent *event) override;


  void handleRerrors(QString error_message);

  void setWorking(bool is_working);
  void checkIsRunning();


  protected slots:
  void updateRresultsBrowser();
  void handleRmessages();
  void handleFinished(int exitCode, QProcess::ExitStatus exitStatus);
  // void saveRscriptAndRdata();

  private:
  private:
  Ui::WidgetRprocess *ui;
  McqrQProcess *mp_rProcess      = nullptr;
  QMovie *mp_loadingIcon         = nullptr;
  QTextDocument *mp_documentText = nullptr;
  QString m_documentString;
  bool m_isWorking         = false;
  bool m_processIsFinished = false;
  QString m_currentStep;
  QString m_mcqrVersion;
};
