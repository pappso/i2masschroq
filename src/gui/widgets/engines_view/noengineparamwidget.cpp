
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "noengineparamwidget.h"

#include "../../project_view/projectwindow.h"
#include "ui_no_engine_view_widget.h"
#include <QDebug>

NoEngineParamWidget::NoEngineParamWidget(QWidget *parent, QString message)
  : QWidget(parent), ui(new Ui::NoEngineParamWidget)
{
  qDebug() << "begin";
  ui->setupUi(this);

  ui->label->setText(message);
  qDebug() << "end";
}

NoEngineParamWidget::~NoEngineParamWidget()
{
  qDebug() << "NoEngineParamWidget::~NoEngineParamWidget";
  delete ui;
  qDebug() << "end";
}
