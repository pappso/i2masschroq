
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QWidget>
#include <core/tandem_run/tandemparameters.h>
#include <pappsomspp/processing/project/projectparameters.h>


namespace Ui
{
class XtandemParamWidget;
}

class XtandemParamWidget : public QWidget
{
  Q_OBJECT

  public:
  explicit XtandemParamWidget(QWidget *parent);
  ~XtandemParamWidget();

  void
  setProjectParameters(const pappso::ProjectParameters &project_parameters);

  public slots:
  void doHelp();

  private:
  void
  setSpectrumParameters(const pappso::ProjectParameters &project_parameters);
  void
  setProteinParameters(const pappso::ProjectParameters &project_parameters);
  void
  setResidueParameters(const pappso::ProjectParameters &project_parameters);
  void
  setScoringParameters(const pappso::ProjectParameters &project_parameters);
  void setRefineParameters(const pappso::ProjectParameters &project_parameters);
  void setOutputParameters(const pappso::ProjectParameters &project_parameters);

  Ui::XtandemParamWidget *ui;
};
