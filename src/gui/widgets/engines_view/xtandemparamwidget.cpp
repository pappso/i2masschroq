
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xtandemparamwidget.h"

#include "../../project_view/projectwindow.h"
#include "ui_xtandem_view_widget.h"
#include <QDebug>
#include <files/tandemparametersfile.h>
#include <pappsomspp/exception/exceptionnotfound.h>


XtandemParamWidget::XtandemParamWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::XtandemParamWidget)
{
  qDebug() << "begin";
  ui->setupUi(this);

  qDebug() << "end";
}

XtandemParamWidget::~XtandemParamWidget()
{
  qDebug() << "XtandemParamWidget::~XtandemParamWidget";
  delete ui;
  qDebug() << "end";
}


void
XtandemParamWidget::setProjectParameters(
  const pappso::ProjectParameters &project_parameters)
{
  setSpectrumParameters(project_parameters);
  setProteinParameters(project_parameters);
  setResidueParameters(project_parameters);
  setScoringParameters(project_parameters);
  setRefineParameters(project_parameters);
  setOutputParameters(project_parameters);
}


void
XtandemParamWidget::doHelp()
{
  QObject *senderObj    = sender();
  QString senderObjName = senderObj->objectName();
  qDebug() << "XtandemParamWidget::doHelp begin " << senderObjName;
  QFile html_doc;
  if(senderObjName == "spmmeu_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/spmmeu.html");
    }
  if(senderObjName == "smpc_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/smpc.html");
    }
  if(senderObjName == "spmmem_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmem.html");
    }
  if(senderObjName == "spmmep_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmep.html");
    }
  if(senderObjName == "spmmie_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmie.html");
    }
  if(senderObjName == "sfmt_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmt.html");
    }
  if(senderObjName == "sfmmeu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmmeu.html");
    }
  if(senderObjName == "sfmme_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmme.html");
    }
  if(senderObjName == "sunlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sunlw.html");
    }
  if(senderObjName == "snlm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlm.html");
    }
  if(senderObjName == "snlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlw.html");
    }
  if(senderObjName == "sdr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sdr.html");
    }
  if(senderObjName == "stp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/stp.html");
    }
  if(senderObjName == "smp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smp.html");
    }
  if(senderObjName == "smfmz_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smfmz.html");
    }
  if(senderObjName == "smpmh_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smpmh.html");
    }
  if(senderObjName == "spsbs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spsbs.html");
    }
  if(senderObjName == "suca_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/suca.html");
    }
  if(senderObjName == "st_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/st.html");
    }
  if(senderObjName == "pcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcs.html");
    }
  if(senderObjName == "pcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcsemi.html");
    }

  if(senderObjName == "pcctmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcctmc.html");
    }
  if(senderObjName == "pcntmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcntmc.html");
    }
  if(senderObjName == "pctrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pctrmm.html");
    }
  if(senderObjName == "pntrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pntrmm.html");
    }
  if(senderObjName == "pqa_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqa.html");
    }
  if(senderObjName == "pqp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqp.html");
    }
  if(senderObjName == "pstpb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pstpb.html");
    }
  if(senderObjName == "pmrmf_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pmrmf.html");
    }
  if((senderObjName == "rmm_push_button") ||
     (senderObjName == "rmm1_push_button") ||
     (senderObjName == "rmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rmm.html");
    }
  if(senderObjName == "rpmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmm.html");
    }
  if(senderObjName == "rpmmotif_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmmotif.html");
    }
  if(senderObjName == "smic_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smic.html");
    }
  if(senderObjName == "smmcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smmcs.html");
    }
  if(senderObjName == "scp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/scp.html");
    }
  if(senderObjName == "sir_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sir.html");
    }
  if(senderObjName == "syi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/syi.html");
    }
  if(senderObjName == "sbi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sbi.html");
    }
  if(senderObjName == "sci_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sci.html");
    }
  if(senderObjName == "szi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/szi.html");
    }
  if(senderObjName == "sai_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sai.html");
    }
  if(senderObjName == "sxi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sxi.html");
    }
  if(senderObjName == "refine_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refine.html");
    }
  if(senderObjName == "rmvev_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rmvev.html");
    }
  if(senderObjName == "refpntm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpntm.html");
    }
  if(senderObjName == "refpctm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpctm.html");
    }
  if((senderObjName == "refmm_push_button") ||
     (senderObjName == "refmm1_push_button") ||
     (senderObjName == "refmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refmm.html");
    }
  if((senderObjName == "refpmm_push_button") ||
     (senderObjName == "refpmm1_push_button") ||
     (senderObjName == "refpmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmm.html");
    }
  if((senderObjName == "refpmmotif_push_button") ||
     (senderObjName == "refpmmotif1_push_button") ||
     (senderObjName == "refpmmotif2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmmotif.html");
    }
  if(senderObjName == "rupmffr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rupmffr.html");
    }
  if(senderObjName == "rcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rcsemi.html");
    }
  if(senderObjName == "ruc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ruc.html");
    }
  if(senderObjName == "rss_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rss.html");
    }
  if(senderObjName == "rpm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpm.html");
    }
  if((senderObjName == "omvev_push_button") ||
     (senderObjName == "omvpev_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/omvev.html");
    }
  if(senderObjName == "oresu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oresu.html");
    }
  if(senderObjName == "osrb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/osrb.html");
    }
  if(senderObjName == "oprot_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oprot.html");
    }
  if(senderObjName == "oseq_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oseq.html");
    }
  if(senderObjName == "oosc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oosc.html");
    }
  if(senderObjName == "ospec_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ospec.html");
    }
  if(senderObjName == "opara_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/opara.html");
    }
  if(senderObjName == "ohist_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohist.html");
    }
  if(senderObjName == "ohcw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohcw.html");
    }
  if(senderObjName == "oph_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oph.html");
    }
  if(senderObjName == "oxp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oxp.html");
    }
  if(senderObjName == "ttfparam_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ttfparam.html");
    }
  if(html_doc.open(QFile::ReadOnly | QFile::Text))
    {
      QTextStream in(&html_doc);
      ui->textEdit->setHtml(in.readAll());
      qDebug() << "EditTandemPresetDialog::doHelp doc " << in.readAll();
    }
  else
    {
      qDebug() << "EditTandemPresetDialog::doHelp doc not found";
    }
  qDebug() << "EditTandemPresetDialog::doHelp end " << senderObjName;
}

void
XtandemParamWidget::setSpectrumParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {
      ui->parenttIonMHMassToleranceWindowUnitLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, parent monoisotopic mass error units")
          .toString());
      ui->parentIonMHMassToleranceUpperWindowLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, parent monoisotopic mass error plus")
          .toString());

      ui->parentIonMHMassToleranceLowerWindowLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, parent monoisotopic mass error minus")
          .toString());

      ui->anticipateCarbonIsotopeParentIonAssignmentsErrorsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, parent monoisotopic mass isotope error")
          .toString());


      ui->setTheMaximumPrecursorChargeToBeScoredLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, maximum parent charge")
          .toString());


      ui->useChemicalAverageOrMonoisotopicMassForFragmentIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, fragment mass type")
          .toString());


      ui->unitsForFragmentIonMassToleranceMonoisotopicLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, fragment monoisotopic mass error units")
          .toString());
      ui->fragmentIonMassToleranceMonoisotopicLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, fragment monoisotopic mass error")
          .toString());


      ui->controlTheUseOfTheNeutralLossWindowLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, use neutral loss window")
          .toString());

      ui->neutralLossMassLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, neutral loss mass")
          .toString());

      ui->neutralLossWindowDaltonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, neutral loss window")
          .toString());


      ui->setTheDynamicRangeToScoreSpectraLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, dynamic range")
          .toString());


      ui->maximumNumberOfPeaksToUseInASpectrumLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, total peaks")
          .toString());


      ui->minimumNumberOfPeaksForASpectrumToBeConsideredLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, minimum peaks")
          .toString());

      ui->minimumMZFragmentToBeConsideredLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, minimum fragment mz")
          .toString());

      ui->minimumParentMHToBeConsideredLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, minimum parent m+h")
          .toString());

      ui->alterHowProteinSequencesAreRetrievedFromFASTAFilesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, sequence batch size")
          .toString());

      ui->useContrastAngleLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, use contrast angle")
          .toString());

      ui->setNumberOfThreadsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, threads")
          .toString());

      ui->ttfparam_edit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "spectrum, timstof MS2 filters")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}

void
XtandemParamWidget::setProteinParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {

      ui->defineProteinCleavageSiteLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, cleavage site")
          .toString());

      ui->useSemiEnzymaticCleavageRulesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, cleavage semi")
          .toString());

      ui->moietyAddedToPeptideCTerminusByCleavageLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, cleavage C-terminal mass change")
          .toString());

      ui->moietyAddedToPeptideNTerminusByCleavageLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, cleavage N-terminal mass change")
          .toString());

      ui->moietyAddedToProteinCTerminusLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, C-terminal residue modification mass")
          .toString());


      ui->moietyAddedToProteinNTerminusLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, N-terminal residue modification mass")
          .toString());


      ui->proteinNTerminusCommonModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, quick acetyl")
          .toString());


      ui->proteinNTerminusCyclisationLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, quick pyrolidone")
          .toString());


      ui->interpretationOfPeptidePhosphorylationModelLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, stP bias")
          .toString());


      ui->modifyTheResidueMassesOfOneAllAminoAcidsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "protein, modified residue mass file")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}

void
XtandemParamWidget::setResidueParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {

      ui->fixedModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "residue, modification mass")
          .toString());
      ui->fixedModifications1LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "residue, modification mass 1")
          .toString());

      ui->fixedModifications2LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "residue, modification mass 2")
          .toString());
      ui->potentialModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "residue, potential modification mass")
          .toString());
      ui->potentialModificationsMotifLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "residue, potential modification motif")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}

void
XtandemParamWidget::setScoringParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {
      ui->minimumNumberOfIonsToScoreAPeptideLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, minimum ion count")
          .toString());
      ui->maximumMissedCleavageSitesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, maximum missed cleavage sites")
          .toString());
      ui->compensateForVerySmallSequenceListFileLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, cyclic permutation")
          .toString());
      ui->automaticallyPerformReverseDatabaseSearchLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, include reverse")
          .toString());
      ui->useYIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, y ions")
          .toString());
      ui->useBIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, b ions")
          .toString());


      ui->useCIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, c ions")
          .toString());


      ui->useZIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, z ions")
          .toString());

      ui->useAIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, a ions")
          .toString());

      ui->useXIonsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "scoring, x ions")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}


void
XtandemParamWidget::setRefineParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {
      ui->useOfTheRefinementModuleLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification, "refine")
          .toString());

      ui->refine_maximum_missed_cleavage_lineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, maximum missed cleavage sites")
          .toString());

      ui->maximumEValueToAcceptPeptideLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, maximum valid expectation value")
          .toString());

      ui->peptideNTerPotentialModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential N-terminus modifications")
          .toString());

      ui->peptideCTerPotentialModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential C-terminus modifications")
          .toString());
      ui->alterFixedModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, modification mass")
          .toString());


      ui->alterFixedModifications1LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, modification mass 1")
          .toString());

      ui->alterFixedModifications2LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, modification mass 2")
          .toString());

      ui->alterPotentialModificationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification mass")
          .toString());

      ui->alterPotentialModifications1LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification mass 1")
          .toString());

      ui->alterPotentialModifications2LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification mass 2")
          .toString());

      ui->potentialModificationsMotifLineEdit_2->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification motif")
          .toString());

      ui->potentialModificationsMotif1LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification motif 1")
          .toString());

      ui->potentialModificationsMotif2LineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, potential modification motif 2")
          .toString());

      ui->usePotentialModificationsInRefinementModuleLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, use potential modifications for full refinement")
          .toString());

      ui->useSemiEnzymaticCleavageRulesLineEdit_2->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, cleavage semi")
          .toString());

      ui->cleavageAtEachResiduesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, unanticipated cleavage")
          .toString());

      ui->spectrumIntensityModelSynthesisLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, spectrum synthesis")
          .toString());

      ui->pointMutationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "refine, point mutations")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}


void
XtandemParamWidget::setOutputParameters(
  const pappso::ProjectParameters &project_parameters)
{
  try
    {
      ui->highestEValueToRecordPeptidesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, maximum valid expectation value")
          .toString());


      ui->highestEValueToRecordProteinsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, maximum valid protein expectation value")
          .toString());

      ui->resultsTypeLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, results")
          .toString());

      ui->sortResultsByLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, sort results by")
          .toString());

      ui->writeProteinsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, proteins")
          .toString());

      ui->writeProteinSequencesLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, sequences")
          .toString());

      ui->writeOnlyFirstProteinSequenceLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, one sequence copy")
          .toString());

      ui->writeSpectrumInformationsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, spectra")
          .toString());

      ui->writeParametersLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, parameters")
          .toString());

      ui->writeHistogramsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, histograms")
          .toString());

      ui->histogramColumnWidthLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, histogram column width")
          .toString());

      ui->hashFilenameWithDateAndTimeStampLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, path hashing")
          .toString());

      ui->xSLTStyleSheetPathToViewXMLResultsLineEdit->setText(
        project_parameters
          .getValue(pappso::ProjectParamCategory::identification,
                    "output, xsl path")
          .toString());
    }
  catch(const pappso::ExceptionNotFound &error)
    {
      qDebug() << error.qwhat();
    }
}
