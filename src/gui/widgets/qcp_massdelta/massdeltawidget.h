/**
 * \file src/gui/widgets/qcp_massdelta/massdeltawidget.h
 * \date 19/03/2021
 * \author Olivier Langella
 * \brief custom widget to display peptide evidence mass delta graph
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include <QWidget>
#include "../../../core/peptideevidence.h"
#include <qcustomplot.h>

namespace Ui
{
class QcpMassDelta;
}

class QCPMassDelta;

class MassDeltaWidget : public QWidget
{
  Q_OBJECT

  friend QCPMassDelta;

  public:
  explicit MassDeltaWidget(QWidget *parent);
  ~MassDeltaWidget();


  void addPeptideEvidenceList(
    const Project *p_project,
    const std::vector<std::shared_ptr<PeptideEvidence>> &peptide_evidence_list);

  void clearPeptideEvidenceList();

  signals:
  void
  peptideEvidenceChanged(std::shared_ptr<PeptideEvidence> peptide_evidence_sp);
  void selectedPeptideEvidenceChanged(
    std::shared_ptr<PeptideEvidence> peptide_evidence_sp);

  protected:
  void mousePressEvent(QMouseEvent *event) override;

  /** @brief sets the current peptide evidence from the QCP mass deltat widget
   */
  void setCurrentPeptideEvidenceSp(
    std::shared_ptr<PeptideEvidence> &currentPeptideEvidence);

  /** @brief sets the selected peptide evidence from the QCP mass deltat widget
   */
  void setSelectedPeptideEvidenceSp(
    std::shared_ptr<PeptideEvidence> &currentPeptideEvidence);

  private:
  Ui::QcpMassDelta *ui;
};
