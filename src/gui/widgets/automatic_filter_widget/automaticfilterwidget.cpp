
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "automaticfilterwidget.h"

#include "../../project_view/projectwindow.h"
#include "ui_automatic_filter_widget.h"
#include <QDebug>
#include <cmath>


AutomaticFilterWidget::AutomaticFilterWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::AutomaticFilterWidget)
{
  qDebug() << "begin";
  ui->setupUi(this);


  AutomaticFilterParameters params;
  params.setAutomaticFilterType(AutomaticFilterType::evalue);
  params.setFilterPeptideEvalue(0.05);

  this->setAutomaticFilterParameters(params);
  qDebug() << "end";
}

AutomaticFilterWidget::~AutomaticFilterWidget()
{
  qDebug();
  delete ui;
  qDebug() << "end";
}


AutomaticFilterParameters
AutomaticFilterWidget::getAutomaticFilterParameters() const
{
  AutomaticFilterParameters params;

  if(ui->fdr_radio_button->isChecked())
    {
      params.setAutomaticFilterType(AutomaticFilterType::fdr);
    }
  else if(ui->evalue_radio_button->isChecked())
    {
      params.setAutomaticFilterType(AutomaticFilterType::evalue);
    }
  else
    {
      params.setAutomaticFilterType(AutomaticFilterType::svmprob);
    }
  params.setFilterCrossSamplePeptideNumber(
    ui->cross_sample_checkbox->isChecked());
  params.setFilterMinimumPeptidePerMatch(ui->peptide_number_spinbox->value());
  params.setFilterPeptideEvalue(ui->peptide_evalue_spinbox->value());
  params.setFilterPeptideFDR(getFilterPeptideFDR());
  params.setFilterPeptideObservedInLessSamplesThan(
    ui->peprepro_spinbox->value());
  params.setFilterProteinQvalue(ui->protein_qvalue_spinbox->value());
  params.setFilterProteinEvalue(ui->protein_evalue_spinbox->value());
  params.setSvmprobThreshold(ui->svm_prob_spinbox->value());
  params.setProteinSvmprobThreshold(ui->protein_prob_spinbox->value());

  return params;
}

void
AutomaticFilterWidget::setAutomaticFilterParameters(
  const AutomaticFilterParameters &params)
{
  qDebug() << "begin ";
  _emit_changed = false;
  ui->evalue_radio_button->setChecked(true);
  if(params.getAutomaticFilterType() == AutomaticFilterType::fdr)
    {
      ui->fdr_radio_button->setChecked(true);
    }
  if(params.getAutomaticFilterType() == AutomaticFilterType::svmprob)
    {
      ui->svm_prob_button->setChecked(true);
    }
  doSetParameters();
  setFilterPeptideFDR(params.getFilterPeptideFDR());
  ui->peptide_evalue_spinbox->setValue(params.getFilterPeptideEvalue());
  ui->protein_evalue_spinbox->setValue(params.getFilterProteinEvalue());
  ui->protein_qvalue_spinbox->setValue(params.getFilterProteinQvalue());
  ui->protein_evalue_log_spinbox->setValue(
    std::log10(params.getFilterProteinEvalue()));
  ui->protein_prob_spinbox->setValue(params.getProteinSvmprobThreshold());

  ui->peptide_number_spinbox->setValue(
    params.getFilterMinimumPeptidePerMatch());
  ui->svm_prob_spinbox->setValue(params.getSvmprobThreshold());
  // ui->protein_evalue_log_spinbox->setValue(std::log(_parameters.getFilterProteinEvalue()));
  ui->cross_sample_checkbox->setCheckState(Qt::Unchecked);
  if(params.getFilterCrossSamplePeptideNumber())
    ui->cross_sample_checkbox->setCheckState(Qt::Checked);
  _emit_changed = true;
  qDebug() << "end ";
}

void
AutomaticFilterWidget::doCrossSample(bool is_cross_sample [[maybe_unused]])
{

  if(_emit_changed)
    emit automaticFilterParametersChanged();
}
void
AutomaticFilterWidget::doPeptideEvalue(double evalue [[maybe_unused]])
{

  if(_emit_changed)
    emit automaticFilterParametersChanged();
}

void
AutomaticFilterWidget::doSvmProbChanged(double evalue [[maybe_unused]])
{

  if(_emit_changed)
    emit automaticFilterParametersChanged();
}
void
AutomaticFilterWidget::doProteinEvalue(double evalue [[maybe_unused]])
{
  if(_signal)
    {
      _signal = false;
      ui->protein_evalue_log_spinbox->setValue(std::log10(evalue));

      if(_emit_changed)
        emit automaticFilterParametersChanged();
    }
  else
    {
      _signal = true;
    }
}
void
AutomaticFilterWidget::doProteinLogEvalue(double evalue)
{
  if(_signal)
    {
      _signal = false;
      ui->protein_evalue_spinbox->setValue(std::pow(10, evalue));

      if(_emit_changed)
        emit automaticFilterParametersChanged();
    }
  else
    {
      _signal = true;
    }
}
void
AutomaticFilterWidget::doPeptideNumber(int number [[maybe_unused]])
{
  if(_emit_changed)
    emit automaticFilterParametersChanged();
}

void
AutomaticFilterWidget::doPepreproChanged(int number [[maybe_unused]])
{
  if(_emit_changed)
    emit automaticFilterParametersChanged();
}

void
AutomaticFilterWidget::doValueChanged(double value [[maybe_unused]])
{
  doSetParameters();
}
void
AutomaticFilterWidget::doSetParameters()
{
  if(ui->fdr_radio_button->isChecked())
    {
      // FDR
      ui->peptide_evalue_spinbox->setVisible(false);
      ui->peptide_evalue_label->setVisible(false);
      ui->svm_prob_spinbox->setVisible(false);
      ui->svm_prob_label->setVisible(false);
      ui->peptide_fdr_spinbox->setVisible(true);
      ui->peptide_fdr_label->setVisible(true);
      ui->protein_evalue_log_spinbox->setVisible(true);
      ui->protein_evalue_log_label->setVisible(true);
      ui->protein_qvalue_spinbox->setVisible(true);
      ui->protein_qvalue_label->setVisible(true);
      ui->protein_evalue_spinbox->setVisible(true);
      ui->protein_evalue_label->setVisible(true);
      ui->protein_evalue_log_spinbox->setVisible(true);
      ui->protein_evalue_log_label->setVisible(true);
      ui->protein_prob_spinbox->setVisible(false);
      ui->protein_prob_label->setVisible(false);
    }
  else if(ui->svm_prob_button->isChecked())
    {
      // SVM
      ui->peptide_evalue_spinbox->setVisible(false);
      ui->peptide_evalue_label->setVisible(false);
      ui->svm_prob_spinbox->setVisible(true);
      ui->svm_prob_label->setVisible(true);
      ui->peptide_fdr_spinbox->setVisible(false);
      ui->peptide_fdr_label->setVisible(false);
      ui->protein_qvalue_spinbox->setVisible(false);
      ui->protein_qvalue_label->setVisible(false);
      ui->protein_evalue_log_spinbox->setVisible(false);
      ui->protein_evalue_log_label->setVisible(false);
      ui->protein_evalue_spinbox->setVisible(false);
      ui->protein_evalue_label->setVisible(false);
      ui->protein_prob_spinbox->setVisible(true);
      ui->protein_prob_label->setVisible(true);
      // ui->protein_prob_spinbox->findChild<QLineEdit *>()->setReadOnly(false);
    }
  else
    {
      // Evalue
      ui->peptide_evalue_spinbox->setVisible(true);
      ui->peptide_evalue_label->setVisible(true);
      ui->svm_prob_spinbox->setVisible(false);
      ui->svm_prob_label->setVisible(false);
      ui->peptide_fdr_spinbox->setVisible(false);
      ui->peptide_fdr_label->setVisible(false);
      ui->protein_qvalue_spinbox->setVisible(false);
      ui->protein_qvalue_label->setVisible(false);
      ui->protein_evalue_log_spinbox->setVisible(true);
      ui->protein_evalue_log_label->setVisible(true);
      ui->protein_evalue_spinbox->setVisible(true);
      ui->protein_evalue_label->setVisible(true);
      ui->protein_prob_spinbox->setVisible(false);
      ui->protein_prob_label->setVisible(false);
    }
  if(_emit_changed)
    emit automaticFilterParametersChanged();
}


int
AutomaticFilterWidget::getAutomaticFilterWindowWidth() const
{
  return ui->gridLayout_2->minimumSize().width();
}

double
AutomaticFilterWidget::getFilterPeptideFDR() const
{
  return (ui->peptide_fdr_spinbox->value() / 100);
}

void
AutomaticFilterWidget::setFilterPeptideFDR(double fdr)
{
  ui->peptide_fdr_spinbox->setValue(fdr * 100);
}
