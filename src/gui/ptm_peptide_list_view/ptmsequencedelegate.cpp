/**
 * \file gui/ptm_peptide_list_window/ptmsequencedelegate.cpp
 * \date 28/7/2017
 * \author Olivier Langella
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptmsequencedelegate.h"
#include "../../grouping/ptm/ptmsamplescan.h"
#include "../ptm_island_list_view/ptmislandlistwindow.h"
#include <QTextDocument>
#include <QPainter>

PtmSequenceDelegate::PtmSequenceDelegate(
  PtmIslandListWindow *p_ptm_island_list_window, QWidget *parent)
  : QStyledItemDelegate(parent)
{
  _p_ptm_island_list_window = p_ptm_island_list_window;
}

void
PtmSequenceDelegate::paint(QPainter *painter,
                           const QStyleOptionViewItem &option,
                           const QModelIndex &index) const
{
  if(index.data().canConvert<PtmSampleScan *>())
    {
      PtmSampleScan *p_ptm_sample_scan = index.data().value<PtmSampleScan *>();

      // if (option.state & QStyle::State_Selected)
      //    painter->fillRect(option.rect, option.palette.highlight());
      QTextDocument document;
      document.setDocumentMargin(2);
      document.setHtml(p_ptm_sample_scan->getHtmlSequence(
        _p_ptm_island_list_window->getIdentificationGroup()
          ->getPtmGroupingExperiment()));
      painter->translate(option.rect.topLeft());
      document.drawContents(painter);
      painter->translate(-option.rect.topLeft());

      // edit_seq.render(painter);
      // edit..paint(painter, option.rect, option.palette,
      //                 StarRating::ReadOnly);
    }
  else
    {
      QStyledItemDelegate::paint(painter, option, index);
    }
}
