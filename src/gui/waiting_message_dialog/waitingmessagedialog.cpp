
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "ui_waiting_message_dialog.h"
#include "waitingmessagedialog.h"
#include <QDebug>
#include <QLabel>
#include <QMutexLocker>
#include <QThread>

WaitingMessageDialog::WaitingMessageDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::WaitingMessageDialog)
{
  qDebug() << "begin";
  ui->setupUi(this);
  this->setModal(true);
  this->setWindowFlags(Qt::Window | Qt::WindowTitleHint |
                       Qt::CustomizeWindowHint);
  // this->setWindowFlags(Qt::FramelessWindowHint);
  // this->setWindowFlags(Qt::WindowTitleHint);

  _p_movie =
    new QMovie(":/i2masschroq_icon/resources/i2MassChroQ_animated_gif.gif");
  if(!_p_movie->isValid())
    {
      qDebug() << " animation not found";
    }

  // ui->subtask_progress_bar = new QProgressBar(this);
  ui->movie_label->setMovie(_p_movie);
  ui->progress_bar->setVisible(false);
  ui->subtask_bar->setVisible(false);

  ui->progress_bar->setMaximum(100);
  ui->progress_bar->setMinimum(0);
  ui->subtask_bar->setMaximum(100);
  ui->subtask_bar->setMinimum(0);
  ui->console_plaintextedit->setHidden(true);
  ui->stop_push_button->setHidden(true);
  ui->stop_push_button->setChecked(false);
  // movie->start();
  qDebug() << QThread::currentThreadId();
  qDebug() << " end";
}

WaitingMessageDialog::~WaitingMessageDialog()
{
  qDebug();
  delete ui;
  qDebug();
}

bool
WaitingMessageDialog::stopWorkerThread()
{
  QMutexLocker mutex_locker(&m_mutex);
  qDebug();
  if(ui->stop_push_button->isHidden())
    {
      qDebug();
      // DO NOT TOUCH directly to the UI in an other thread :not safe
      // ui->stop_push_button->setChecked(false);
      // ui->stop_push_button->setHidden(false);
    }
  else
    {
      qDebug();
      if(ui->stop_push_button->isChecked())
        {
          qDebug();
          return true;
        }
    }
  qDebug();
  return false;
}

void
WaitingMessageDialog::message(const QString &message)
{
  // QMutexLocker mutex_locker(&m_mutex);
  // ui->progress_bar->setVisible(false);
  ui->message_label->setText(message);
}

void
WaitingMessageDialog::message(const QString &message, int progress_value)
{
  // QMutexLocker mutex_locker(&m_mutex);
  qDebug() << progress_value;
  ui->progress_bar->setVisible(true);
  ui->progress_bar->setValue(progress_value);
  ui->message_label->setText(message);
}

void
WaitingMessageDialog::appendText(const char *text)
{
  // QMutexLocker mutex_locker(&m_mutex);
  ui->console_plaintextedit->setHidden(false);
  // ui->console_plaintextedit->appendText(text);

  ui->console_plaintextedit->moveCursor(QTextCursor::End);
  ui->console_plaintextedit->insertPlainText(text);
}

void
WaitingMessageDialog::appendText(const QString &message)
{

  // QMutexLocker mutex_locker(&m_mutex);
  ui->console_plaintextedit->setHidden(false);
  // ui->console_plaintextedit->appendText(text);

  ui->console_plaintextedit->moveCursor(QTextCursor::End);
  ui->console_plaintextedit->insertPlainText(message);
}


void
WaitingMessageDialog::setText(const QString text)
{
  // QMutexLocker mutex_locker(&m_mutex);
  ui->console_plaintextedit->setHidden(false);
  // ui->console_plaintextedit->appendText(text);
  ui->console_plaintextedit->setPlainText(text);
}

void
WaitingMessageDialog::hideEvent(QHideEvent *event)
{
  // QMutexLocker mutex_locker(&m_mutex);
  QWidget::hideEvent(event);
  qDebug();
  _p_movie->stop();
  ui->console_plaintextedit->setHidden(true);
  ui->subtask_bar->setHidden(true);
  ui->progress_bar->setHidden(true);
  qDebug();
}
void
WaitingMessageDialog::showEvent(QShowEvent *event)
{
  qDebug();
  // QMutexLocker mutex_locker(&m_mutex);
  qDebug();
  QWidget::showEvent(event);
  qDebug();
  _p_movie->start();
  qDebug();
  ui->stop_push_button->setChecked(false);
  qDebug();
  ui->stop_push_button->setHidden(true);
  qDebug();
  ui->console_plaintextedit->clear();
  ui->subtask_bar->setHidden(true);
  ui->progress_bar->setHidden(true);
  qDebug();
}


void
WaitingMessageDialog::showStopButton()
{
  ui->stop_push_button->setChecked(false);
  ui->stop_push_button->setHidden(false);
}

void
WaitingMessageDialog::percent(int progress_value)
{

  qDebug() << QThread::currentThreadId();
  // QMutexLocker mutex_locker(&m_mutex);
  qDebug() << progress_value;
  ui->progress_bar->setVisible(true);
  ui->progress_bar->setValue(progress_value);
  // ui->subtask_progress_label->setText(QString("%1 %").arg(progress_value));
}

void
WaitingMessageDialog::subTaskPercent(int progress_value)
{

  // QMutexLocker mutex_locker(&m_mutex);
  qDebug() << QThread::currentThreadId();
  ui->subtask_bar->setVisible(true);
  ui->subtask_bar->setValue(progress_value);
}

void
WaitingMessageDialog::closeEvent(QCloseEvent *event)
{
  event->ignore();
}
