/**
 * \file /gui/run/msstats/msstatsrunview.h
 * \date 25/05/2023
 * \author Olivier Langella
 * \brief dialog window to run MSstats process
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QWidget>
#include "../rworkflowrunviewbase.h"
#include "../../../input/masschroq/masschroqcborinfoparser.h"

namespace Ui
{
class MsStatsRunView;
}

class MainWindow;
class MsStatsRunView : public RworkflowRunViewBase
{
  Q_OBJECT

  public:
  explicit MsStatsRunView(MainWindow *parent);
  virtual ~MsStatsRunView();

  protected:
  void checkInstall() override;

  void executeStep(const QString &step) override;
  virtual WidgetRprocess *getWidgetRprocess() const override;
  virtual void setWorkingDirectoryLabel() override;


  protected slots:
  //void doSelectMasschroqMlFile();
  void doSelectMasschroqCborFile();
  void doGetMetadataTemplateFile();
  void doUploadMetadataFile();
  void doMsstatsDataProcess();
  void doMsstatsSampleQuantification();
  void doMsstatsGroupQuantification();
  void doGetOdsSampleQuantification();
  void doGetOdsGroupQuantification();
  void doGetMsstatsInputTable();
  void doMcqrDubiousDataFilter();


  signals:


  private:
  // McqrMetadataSet readMetadata() const;

  private:
  Ui::MsStatsRunView *ui;

  MassChroqCborInfoParser m_mcqCborReader;

  McqrMetadataSet m_metadataSet;
};
