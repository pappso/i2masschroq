/**
 * \file /gui/run/mcqr_spectral_count/mcqrscrunview.h
 * \date 21/12/2023
 * \author Olivier Langella
 * \brief dialog window to run MCQR spectral count process
 */

/*********************MsStatsRunView**********************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <QWidget>
#include "../rworkflowrunviewbase.h"
#include "../../../core/project.h"


namespace Ui
{
class McqrScRunView;
}

class MainWindow;
class McqrScRunView : public RworkflowRunViewBase
{
  Q_OBJECT

  public:
  explicit McqrScRunView(MainWindow *parent);

  /**
   * Destructor
   */
  virtual ~McqrScRunView();

  protected:
  void checkInstall() override;

  void executeStep(const QString &step) override;
  virtual WidgetRprocess *getWidgetRprocess() const override;
  virtual void setWorkingDirectoryLabel() override;


  protected slots:
  void doGetMetadataTemplateFile();
  void doGetSpectrumCountTable();
  void doUploadMetadataFile();
  void doA2A2McqrPca();
  void doA3A1McqrRemoveMsrun();
  void doA4McqrProteinFilter();
  void doA5McqrHeatmap();
  void doA6McqrCluster();
  void doA7McqrAnova();
  void doA8McqrPlotProteinAbundance();
  void doA9McqrProteinAbundanceFoldChange();
  void doFactorProteinFoldSelected(QString);


  signals:

  private:
  QString getColorListString() const;
  QString getFactorListString() const;
  QString getLabelListString() const;
  void updateMetadataFactorListView();

  private:
  Ui::McqrScRunView *ui;

  McqrMetadataSet m_metadataSet;

  ProjectSp msp_project;
};
