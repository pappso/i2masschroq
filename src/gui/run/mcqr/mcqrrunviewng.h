/**
 * \file /gui/run/mcqr/mcqrrunviewng.h
 * \date 14/06/2023
 * \author Olivier Langella
 * \brief dialog window to run MCQR process
 */

/*********************MsStatsRunView**********************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QWidget>
#include "../rworkflowrunviewbase.h"
#include "../../../input/masschroq/masschroqmlinfoparser.h"
#include "../../../input/masschroq/masschroqcborinfoparser.h"
#include <pappsomspp/processing/project/projectparameters.h>

namespace Ui
{
class McqrRunViewNg;
}

class MainWindow;
class McqrRunViewNg : public RworkflowRunViewBase
{
  Q_OBJECT

  public:
  explicit McqrRunViewNg(MainWindow *parent);
  virtual ~McqrRunViewNg();

  protected:
  void checkInstall() override;

  void executeStep(const QString &step) override;
  virtual WidgetRprocess *getWidgetRprocess() const override;
  virtual void setWorkingDirectoryLabel() override;

  protected slots:
  //void doSelectMasschroqMlFile();
  void doSelectMasschroqCborFile();
  void doGetMetadataTemplateFile();
  void doUploadMetadataFile();
  void doA2McqrPca();
  void doA3A1McqrRemoveMsrun();
  void doA3A2McqrDubiousDataFilter();
  void doA4A1McqrNormalization();
  void doA4A1proteobench();
  void doA4A2McqrNormRemoveMsrun();
  void doA5A1FilterSharedPeptides();
  void doA5A2FilterUnreproduciblePeptides();
  void doA6A1filterUncorrelatedPeptides();
  void doA6A2filterProteinWithFewPeptides();
  void doA7imputation();
  void doA8proteinHeatmap();
  void doA9proteinClustering();
  void doB10proteinAnova();
  void doB11plotProteinAbundance();
  void doB12proteinAbundanceFoldChange();
  void doFactorProteinFoldSelected(QString);
  void doNormalisationMethodChanged(QString);

  void doStepFinished(QString);


  signals:

  private:
  QString getColorListString() const;
  QString getFactorListString() const;
  QString getLabelListString() const;
  void updateMetadataFactorListView();
  void writeProteobenchOds(const QString &tsv_file,
                           const QString &file_description);

  private:
  Ui::McqrRunViewNg *ui;


  //MasschroqMlInfoParser m_mcqlReader;
  MassChroqCborInfoParser m_mcqCborReader;
  McqrMetadataSet m_metadataSet;

  pappso::ProjectParameters m_projectParameters;
};
