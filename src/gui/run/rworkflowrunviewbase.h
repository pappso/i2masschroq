/**
 * \file /gui/run/rworkflowrunviewbase.h
 * \date 3/11/2023
 * \author Olivier Langella
 * \brief base window for R run views
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QWidget>
#include "../../input/masschroq/masschroqmlinfoparser.h"
#include "../../core/mcqr/mcqrqprocess.h"
#include "../widgets/widget_rprocess/widgetrprocess.h"


class MainWindow;
class RworkflowRunViewBase : public QWidget
{
  Q_OBJECT

  public:
  explicit RworkflowRunViewBase(MainWindow *parent);
  virtual ~RworkflowRunViewBase();
  McqrQProcess *getRProcessRunning();


  virtual void checkInstall();

  virtual void warning(const QString &message) final;

  virtual void executeStep(const QString &step) = 0;

  virtual bool fileExists(const QString &filename) const final;
  virtual QString filePath(const QString &filename) const final;
  virtual bool removeFile(const QString &filename) const final;


  protected slots:
  virtual void doSelectWorkingDirectory() final;
  virtual void doGetRscript() final;
  virtual void doGetRmdScript() final;

  protected:
  McqrMetadataSet readMetadata() const;
  virtual void closeEvent(QCloseEvent *event) override;
  virtual WidgetRprocess *getWidgetRprocess() const = 0;
  virtual void setWorkingDirectoryLabel()           = 0;
  void launchDesktopSoftwareOnFile(const QString &filePath) const;
  virtual QString toRboolean(bool is_ok) const final;

  protected:
  MainWindow *mp_main = nullptr;
  QString m_workingDirectory;
  QString m_rviewName;
};
