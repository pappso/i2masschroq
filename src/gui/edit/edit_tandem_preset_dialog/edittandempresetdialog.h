/**
 * \file gui/edit_tandem_preset_dialog/edittandempresetdialog.h
 * \date 30/9/2017
 * \author Olivier Langella
 * \brief edit tandem preset dialog
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../../files/tandemparametersfile.h"
#include <pappsomspp/processing/project/projectparameters.h>
#include <QDialog>

namespace Ui
{
class EditTandemPresetView;
}

class EditTandemPresetDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit EditTandemPresetDialog(QWidget *parent);
  ~EditTandemPresetDialog();

  void setTandemParametersFile(const TandemParametersFile &tandem_preset_file);
  void newTandemParameters(const TandemParameters &tandem_params);

  void setProjectParameters(const pappso::ProjectParameters &project_parameters);

  const TandemParametersFile &getTandemParametersFile() const;

  protected:
  void done(int r) override;

  public slots:
  void doHelp();
  void doCopy();
  void doEdit(QString value);
  void doResultType();
  void doRefine();
  void doLoad();
  void doSelectDir();

  private:
  void doSave();
  void fillPresetComboBox();
  void fillPresetDirectoryLabel();
  void populate();
  void readUi();

  private:
  Ui::EditTandemPresetView *ui;
  TandemParametersFile *_p_tandem_preset_file = nullptr;
  TandemParameters _tandem_params;
  QString _preset_directory;
};
