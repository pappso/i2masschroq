
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#include "msidentificationtablemodel.h"

#include <QDebug>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include "msidentificationlistwindow.h"
#include "../../../utils/identificationdatasourcestore.h"


std::vector<msIdentificationListColumn>
  MsIdentificationTableModel::m_columnOrder = {
    msIdentificationListColumn::align_groups,
    msIdentificationListColumn::run_id,
    msIdentificationListColumn::id_file,
    msIdentificationListColumn::ms_instrument_name,
    msIdentificationListColumn::sample_name,
    msIdentificationListColumn::mzML,
    msIdentificationListColumn::engine_name,
    msIdentificationListColumn::fasta_file,
    msIdentificationListColumn::sequence,
    msIdentificationListColumn::nbr_spectrum,
    msIdentificationListColumn::total_spectra_assigned,
    msIdentificationListColumn::percent_assign,
    msIdentificationListColumn::msrun_total_spectra_assigned,
    msIdentificationListColumn::msrun_percent_assign,
    msIdentificationListColumn::id_msrun_id,
    msIdentificationListColumn::nb_ms_1,
    msIdentificationListColumn::nb_ms_2,
    msIdentificationListColumn::nb_ms_3,
    msIdentificationListColumn::nb_tic_1,
    msIdentificationListColumn::nb_tic_2,
    msIdentificationListColumn::nb_tic_3};

MsIdentificationTableModel::MsIdentificationTableModel(
  MsIdentificationListWindow *ms_id_list_window, Project *project)
  : QAbstractTableModel(ms_id_list_window)
{
  m_ms_id_list_window = ms_id_list_window;
  mp_project          = project;
  msp_workerStatus    = std::make_shared<MsIdListWorkerStatus>();

  mp_movie = new QMovie(":icons/resources/icons/icon_wait.gif");
  mp_movie->start();
  mp_movie->setScaledSize(QSize(20, 20));

  connect(mp_movie,
          &QMovie::frameChanged,
          this,
          &MsIdentificationTableModel::onMsIdentificationDataChanged);
  connect(this,
          &MsIdentificationTableModel::alignmentGroupChanged,
          this,
          &MsIdentificationTableModel::onMsIdentificationDataChanged);
}

MsIdentificationTableModel::~MsIdentificationTableModel()
{
  delete mp_movie;
}

void
MsIdentificationTableModel::setIdentificationDataSourceSpList(
  std::vector<IdentificationDataSourceSp> &identificationDataSourceSpList)
{
  qDebug();
  qDebug() << " identificationDataSourceSpList.size()="
           << identificationDataSourceSpList.size();

  beginResetModel();
  m_identificationDataSourceSpList = identificationDataSourceSpList;
  endResetModel();

  m_ms_id_list_window->resizeColumnsToContents();
}

std::vector<IdentificationDataSourceSp>
MsIdentificationTableModel::getIdentificationDataSourceSpList() const
{
  return m_identificationDataSourceSpList;
}

int
MsIdentificationTableModel::rowCount(const QModelIndex &parent
                                     [[maybe_unused]]) const
{
  qDebug();
  // qDebug() << "MsIdentificationTableModel::rowCount begin ";
  if(m_identificationDataSourceSpList.size() != 0)
    {
      return (int)m_identificationDataSourceSpList.size();
    }
  return 0;
}
int
MsIdentificationTableModel::columnCount(const QModelIndex &parent
                                        [[maybe_unused]]) const
{
  qDebug();
  return (std::int8_t)msIdentificationListColumn::last;
}

int
MsIdentificationTableModel::getColumnIndex(msIdentificationListColumn column)
{
  qDebug();
  return std::distance(
    m_columnOrder.begin(),
    std::find(m_columnOrder.begin(), m_columnOrder.end(), column));
}

const QString
MsIdentificationTableModel::getTitle(std::int8_t column)
{
  qDebug();
  column = (std::int8_t)m_columnOrder[column];
  switch(column)
    {
      case(std::int8_t)msIdentificationListColumn::align_groups:
        return "Alignment groups";
        break;
      case(std::int8_t)msIdentificationListColumn::run_id:
        return "Identification Run ID";
        break;
      case(std::int8_t)msIdentificationListColumn::id_file:
        return "Identification result file";
        break;
      case(std::int8_t)msIdentificationListColumn::mzML:
        return "Ms Run File";
        break;
      case(std::int8_t)msIdentificationListColumn::engine_name:
        return "Identification engine";
        break;
      case(std::int8_t)msIdentificationListColumn::fasta_file:
        return "Fasta file(s)";
        break;
      case(std::int8_t)msIdentificationListColumn::nbr_spectrum:
        return "Total spectra used";
        break;
      case(std::int8_t)msIdentificationListColumn::total_spectra_assigned:
        return "Total spectra assigned";
        break;
      case(std::int8_t)msIdentificationListColumn::percent_assign:
        return "Assignment %";
        break;
      case(std::int8_t)msIdentificationListColumn::msrun_percent_assign:
        return "Msrun assignment %";
        break;
      case(std::int8_t)msIdentificationListColumn::msrun_total_spectra_assigned:
        return "MSrun total spectra assigned";
        break;
      case(std::int8_t)msIdentificationListColumn::id_msrun_id:
        return "Sample ID";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_1:
        return "Total MS1";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_2:
        return "Total MS2";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_3:
        return "Total MS3";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_1:
        return "TIC MS1";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_2:
        return "TIC MS2";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_3:
        return "TIC MS3";
        break;
      case(std::int8_t)msIdentificationListColumn::sequence:
        return "Sequences";
        break;
      case(std::int8_t)msIdentificationListColumn::sample_name:
        return "Sample name";
        break;
      case(std::int8_t)msIdentificationListColumn::ms_instrument_name:
        return "Instrument name";
        break;
    }
  return "";
}

const QString
MsIdentificationTableModel::getDescription(std::int8_t column)
{

  qDebug();
  column = (std::int8_t)m_columnOrder[column];
  qDebug() << "begin " << column;
  switch(column)
    {
      case(std::int8_t)msIdentificationListColumn::align_groups:
        return "Groups for Quantification (Right click to add a MsRun "
               "Alignment group)";
        break;
      case(std::int8_t)msIdentificationListColumn::run_id:
        return "Identification of RunID";
        break;
      case(std::int8_t)msIdentificationListColumn::id_file:
        return "Identification File pathway";
        break;
      case(std::int8_t)msIdentificationListColumn::mzML:
        return "peak-list data file pathway";
        break;
      case(std::int8_t)msIdentificationListColumn::engine_name:
        return "Engine Name";
        break;
      case(std::int8_t)msIdentificationListColumn::fasta_file:
        return "Fasta files used in identification";
        break;
      case(std::int8_t)msIdentificationListColumn::nbr_spectrum:
        return "Number of Spectrum";
        break;
      case(std::int8_t)msIdentificationListColumn::total_spectra_assigned:
        return "Number of assigned spectra";
        break;
      case(std::int8_t)msIdentificationListColumn::percent_assign:
        return "Percentage of Assignation";
        break;
      case(std::int8_t)msIdentificationListColumn::msrun_percent_assign:
        return "Percentage of Assignation in this MSrun (all "
               "identification engines)";
        break;
      case(std::int8_t)msIdentificationListColumn::msrun_total_spectra_assigned:
        return "Total number of spectra assigned in this MSrun (all "
               "identification engines)";
        break;
      case(std::int8_t)msIdentificationListColumn::id_msrun_id:
        return "Identification of MsRunID";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_1:
        return "Number of ms1 spectra";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_2:
        return "Number of ms2 spectra";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_ms_3:
        return "Number of ms3 spectra";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_1:
        return "Number of TIC 1";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_2:
        return "Number of TIC 2 ";
        break;
      case(std::int8_t)msIdentificationListColumn::nb_tic_3:
        return "Number of TIC 3";
        break;
      case(std::int8_t)msIdentificationListColumn::sequence:
        return "number of unique distinct peptide sequences";
        break;
      case(std::int8_t)msIdentificationListColumn::sample_name:
        return "Sample name";
        break;
      case(std::int8_t)msIdentificationListColumn::ms_instrument_name:
        return "MS instrument name";
        break;
    }
  return "";
}

QVariant
MsIdentificationTableModel::headerData(int section,
                                       Qt::Orientation orientation,
                                       int role) const
{
  qDebug();
  if(orientation == Qt::Horizontal)
    {
      switch(role)
        {
          case Qt::DisplayRole:
            // qDebug() << section;
            return QVariant(getTitle(section));
            break;
          case Qt::ToolTipRole:
            return QVariant(getDescription(section));
            break;
          case Qt::SizeHintRole:
            return QSize(getColumnWidth(section), 40);
            break;
        }
    }
  return QVariant();
}

int
MsIdentificationTableModel::getColumnWidth(int col)
{
  qDebug();
  msIdentificationListColumn column = m_columnOrder[col];
  switch(column)
    {
      case msIdentificationListColumn::align_groups:
        return 160;
        break;
      case msIdentificationListColumn::run_id:
        return 200;
        break;
      case msIdentificationListColumn::sample_name:
        return 250;
        break;
      case msIdentificationListColumn::ms_instrument_name:
        return 200;
        break;
      case msIdentificationListColumn::id_file:
        return 400;
        break;
      case msIdentificationListColumn::mzML:
        return 400;
        break;
      case msIdentificationListColumn::nbr_spectrum:
        return 160;
        break;
      case msIdentificationListColumn::total_spectra_assigned:
        return 160;
        break;
      case msIdentificationListColumn::engine_name:
        return 160;
        break;
      default:
        break;
    }
  return 120;
}

msIdentificationListColumn
MsIdentificationTableModel::getMsIdentificationListColumn(std::int8_t column)
{

  return m_columnOrder[column];
}

QVariant
MsIdentificationTableModel::data(const QModelIndex &index, int role) const
{
  qDebug();
  int row = index.row();
  int col = index.column();

  qDebug() << " row=" << row << " col=" << col;
  msIdentificationListColumn column = m_columnOrder[col];
  // col = (std::int8_t)m_columnOrder[col];

  qDebug() << " msIdentificationListColumn=" << (int8_t)column;
  switch(role)
    {
      case Qt::SizeHintRole:
        qDebug() << MsIdentificationTableModel::getColumnWidth(col);

        return QSize(MsIdentificationTableModel::getColumnWidth(col), 30);
        break;
      case Qt::DisplayRole:
        qDebug();
        switch(column)
          {
            case msIdentificationListColumn::run_id:
              qDebug();
              return QVariant(
                m_identificationDataSourceSpList.at(row)->getXmlId());
              break;

            case msIdentificationListColumn::align_groups:
              {
                qDebug();
                MsRunAlignmentGroupSp group =
                  m_identificationDataSourceSpList.at(row)
                    ->getMsRunSp()
                    ->getAlignmentGroup();
                if(group == nullptr)
                  {
                    return QVariant("Select Group");
                  }
                else
                  {
                    return QVariant(group.get()->getMsRunAlignmentGroupName());
                  }
              }
              break;


            case msIdentificationListColumn::id_file:
              {
                qDebug();
                QFileInfo fi(
                  m_identificationDataSourceSpList.at(row)->getResourceName());
                return QVariant(fi.fileName());
              }
              break;
            case msIdentificationListColumn::mzML:
              {
                qDebug();
                QFileInfo fi(m_identificationDataSourceSpList.at(row)
                               ->getMsRunSp()
                               .get()
                               ->getFileName());
                return QVariant(fi.fileName());
              }
              break;
            case msIdentificationListColumn::engine_name:
              {
                qDebug();
                return QVariant(m_identificationDataSourceSpList.at(row)
                                  ->getIdentificationEngineName() +
                                " " +
                                m_identificationDataSourceSpList.at(row)
                                  ->getIdentificationEngineVersion());
              }
              break;
            case msIdentificationListColumn::fasta_file:
              {
                qDebug();
                std::vector<FastaFileSp> fasta_list =
                  m_identificationDataSourceSpList.at(row)->getFastaFileList();
                if(fasta_list.size() == 1)
                  {
                    return QVariant(fasta_list.front()->getFilename());
                  }
                else if(fasta_list.size() == 0)
                  {
                    return QVariant("No fasta file used");
                  }
                else // more than one fasta file listed
                  {
                    return QVariant(QString::number(fasta_list.size()) +
                                    " fasta used");
                  }
              }
              break;


            case msIdentificationListColumn::id_msrun_id:
              {
                qDebug();
                return QVariant(m_identificationDataSourceSpList.at(row)
                                  ->getMsRunSp()
                                  ->getXmlId());
              }
              break;
            case msIdentificationListColumn::sample_name:
              {
                qDebug();
                return QVariant(m_identificationDataSourceSpList.at(row)
                                  ->getMsRunSp()
                                  .get()
                                  ->getMsRunReaderSPtr()
                                  .get()
                                  ->getMsRunId()
                                  ->getSampleName());
              }
              break;
            case msIdentificationListColumn::ms_instrument_name:
              {
                try
                  {
                    qDebug();
                    return QVariant(m_identificationDataSourceSpList.at(row)
                                      ->getMsRunSp()
                                      .get()
                                      ->getMsRunReaderSPtr()
                                      .get()
                                      ->getOboPsiModTermInstrumentModelName()
                                      .m_name);
                  }
                catch(pappso::ExceptionNotFound &exception_pappso)
                  {
                    return "not found";
                  }
              }
              break;
            case msIdentificationListColumn::total_spectra_assigned:
              {
                qDebug();
                QVariant count_spectra_assigned =
                  (quint32)m_identificationDataSourceSpList.at(row)
                    .get()
                    ->getPeptideEvidenceStore()
                    .countUniqueValidSpectrumIndexList();
                m_identificationDataSourceSpList.at(row)
                  ->setIdentificationEngineStatistics(
                    IdentificationEngineStatistics::total_spectra_assigned,
                    count_spectra_assigned);
                return count_spectra_assigned;
              }
              break;
            case msIdentificationListColumn::sequence:
              {
                qDebug();
                QVariant count_valid_sequences =
                  (quint32)m_identificationDataSourceSpList.at(row)
                    .get()
                    ->getPeptideEvidenceStore()
                    .countSequenceLi(ValidationState::validAndChecked);
                m_identificationDataSourceSpList.at(row)
                  ->setIdentificationEngineStatistics(
                    IdentificationEngineStatistics::total_unique_assigned,
                    count_valid_sequences);
                return count_valid_sequences;
              }
              break;
            case msIdentificationListColumn::msrun_total_spectra_assigned:
              {
                qDebug();
                auto msrun =
                  m_identificationDataSourceSpList.at(row).get()->getMsRunSp();
                QVariant count_spectra_assigned =
                  (quint32)msrun.get()->countUniqueValidSpectrumIndexList(
                    mp_project);
                msrun.get()->setMsRunStatistics(
                  MsRunStatistics::total_spectra_assigned,
                  count_spectra_assigned);
                return count_spectra_assigned;
              }
              break;
            case msIdentificationListColumn::nbr_spectrum:
              {
                qDebug();
                return QVariant(
                  m_identificationDataSourceSpList.at(row)
                    ->getIdentificationEngineStatistics(
                      IdentificationEngineStatistics::total_spectra_used));
              }
              break;

            case msIdentificationListColumn::percent_assign:
              {
                qDebug();
                std::size_t total_spectra_assigned =
                  m_identificationDataSourceSpList.at(row)
                    .get()
                    ->getPeptideEvidenceStore()
                    .countUniqueValidSpectrumIndexList();
                std::size_t total_spectra =
                  getMsRunStatisticsNumber(row,
                                           MsRunStatistics::total_spectra_ms2)
                    .toDouble();
                qDebug() << total_spectra_assigned;

                qDebug() << total_spectra;
                QVariant ratio;
                ratio.setValue((double)total_spectra_assigned /
                               (double)total_spectra);
                qDebug() << ratio;
                m_identificationDataSourceSpList.at(row)
                  ->setIdentificationEngineStatistics(
                    IdentificationEngineStatistics::assignment_ratio, ratio);

                return ratio;
              }
              break;


            case msIdentificationListColumn::msrun_percent_assign:
              {
                qDebug();
                auto msrun =
                  m_identificationDataSourceSpList.at(row).get()->getMsRunSp();
                std::size_t count_spectra_assigned =
                  msrun.get()->countUniqueValidSpectrumIndexList(mp_project);


                std::size_t total_spectra =
                  getMsRunStatisticsNumber(row,
                                           MsRunStatistics::total_spectra_ms2)
                    .toDouble();
                qDebug() << count_spectra_assigned;

                qDebug() << total_spectra;
                QVariant ratio;
                ratio.setValue((double)count_spectra_assigned /
                               (double)total_spectra);
                qDebug() << ratio;


                msrun.get()->setMsRunStatistics(
                  MsRunStatistics::assignment_ratio, ratio);

                return ratio;
              }
              break;


            case msIdentificationListColumn::nb_ms_1:
              qDebug();
              return getMsRunStatisticsNumber(
                row, MsRunStatistics::total_spectra_ms1);
              break;

            case msIdentificationListColumn::nb_ms_2:
              qDebug();
              return getMsRunStatisticsNumber(
                row, MsRunStatistics::total_spectra_ms2);
              break;

            case msIdentificationListColumn::nb_ms_3:
              qDebug();
              return getMsRunStatisticsNumber(
                row, MsRunStatistics::total_spectra_ms3);
              break;

            case msIdentificationListColumn::nb_tic_1:
              qDebug();
              return getMsRunStatisticsNumber(row,
                                              MsRunStatistics::tic_spectra_ms1);
              break;

            case msIdentificationListColumn::nb_tic_2:
              qDebug();
              return getMsRunStatisticsNumber(row,
                                              MsRunStatistics::tic_spectra_ms2);
              break;

            case msIdentificationListColumn::nb_tic_3:
              qDebug();
              return getMsRunStatisticsNumber(row,
                                              MsRunStatistics::tic_spectra_ms3);
              break;
            case msIdentificationListColumn::last:
              return QVariant();
              break;
          }

        qDebug();
        return QVariant();

      case Qt::ToolTipRole:
        if(column == msIdentificationListColumn::id_file)
          {
            qDebug();
            return QVariant(
              m_identificationDataSourceSpList.at(row)->getResourceName());
          }

        if(column == msIdentificationListColumn::mzML)
          {
            return QVariant(m_identificationDataSourceSpList.at(row)
                              ->getMsRunSp()
                              .get()
                              ->getFileName());
          }
        if(column == msIdentificationListColumn::fasta_file)
          {
            std::vector<FastaFileSp> fasta_list =
              m_identificationDataSourceSpList.at(row)->getFastaFileList();
            QString tool_tip;
            for(FastaFileSp fasta_file : fasta_list)
              {
                tool_tip += fasta_file->getAbsoluteFilePath() + "\n";
              }
            return QVariant(tool_tip);
          }
        qDebug();
        return QVariant();

      case Qt::DecorationRole:
        if(column == msIdentificationListColumn::nb_ms_1)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::nb_ms_2)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::nb_ms_3)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::nb_tic_1)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::nb_tic_2)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::nb_tic_3)
          {
            return showMsRunStatisticsStatus(row);
          }
        if(column == msIdentificationListColumn::engine_name)
          {
            return QIcon(":/icons/resources/icons/open_new");
          }
        return QVariant();

      case Qt::FontRole:
        qDebug();
        QFont font;
        if(m_identificationDataSourceSpList.at(row)
             ->getMsRunSp()
             ->getAlignmentGroup() != nullptr)
          {
            if(m_identificationDataSourceSpList.at(row)
                 ->getMsRunSp()
                 ->getAlignmentGroup()
                 ->getMsRunReference() ==
               m_identificationDataSourceSpList.at(row)->getMsRunSp())
              {
                font.setBold(true);
              }
          }
        return font;
    }
  qDebug();
  return QVariant();
}

bool
MsIdentificationTableModel::setData(const QModelIndex &index,
                                    const QVariant &new_data,
                                    int role)
{

  qDebug();
  msIdentificationListColumn column = m_columnOrder[index.column()];
  if(role == Qt::EditRole)
    {
      if(column == msIdentificationListColumn::align_groups)
        {
          emit projectStatusChanged();
          if(!new_data.isNull())
            {
              foreach(MsRunAlignmentGroupSp msrun_alignment_group,
                      mp_project->getMsRunAlignmentGroupList())
                {
                  if(msrun_alignment_group->getMsRunAlignmentGroupName() ==
                     new_data)
                    {
                      MsRunAlignmentGroupSp existing_msrun_align_group =
                        m_identificationDataSourceSpList.at(index.row())
                          ->getMsRunSp()
                          ->getAlignmentGroup();
                      if(existing_msrun_align_group != nullptr)
                        {
                          existing_msrun_align_group.get()
                            ->removeMsRunFromMsRunAlignmentGroupList(
                              m_identificationDataSourceSpList.at(index.row())
                                ->getMsRunSp());
                        }
                      m_identificationDataSourceSpList.at(index.row())
                        ->getMsRunSp()
                        ->setAlignmentGroup(msrun_alignment_group);
                      return true;
                    }
                }
            }
          else
            {
              m_identificationDataSourceSpList.at(index.row())
                ->getMsRunSp()
                ->setAlignmentGroup(nullptr);
              return true;
            }
        }
      return false;
    }
  return false;
}


Qt::ItemFlags
MsIdentificationTableModel::flags(const QModelIndex &idx) const
{

  msIdentificationListColumn column = m_columnOrder[idx.column()];
  if(column == msIdentificationListColumn::align_groups)
    {
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable; // | Qt::ItemIsEditable;
    }
  else
    {
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }
}

QVariant
MsIdentificationTableModel::getMsRunStatisticsNumber(
  int row, MsRunStatistics column) const
{
  WorkerStatus temp =
    msp_workerStatus.get()->getStatus(m_identificationDataSourceSpList.at(row));
  QVariant var_return;
  switch(temp)
    {
      case WorkerStatus::Ready:
        var_return = getMsRunStatisticsFollowingFormat(
          m_identificationDataSourceSpList.at(row)->getMsRunSp(), column);
        emit projectStatusChanged();
        break;
      case WorkerStatus::NotReady:
        var_return = QVariant("");
        break;
      case WorkerStatus::Error:
        var_return = QVariant("Error");
        break;

      case WorkerStatus::FileNotFound:
        var_return = QVariant("file not found!");
        break;

      case WorkerStatus::Running:
        var_return = QVariant("");
        break;

      case WorkerStatus::Waiting:
        var_return = QVariant("Wait");
        break;
      default:
        break;
    }
  return var_return;
}

QVariant
MsIdentificationTableModel::showMsRunStatisticsStatus(int row) const
{

  WorkerStatus temp =
    msp_workerStatus.get()->getStatus(m_identificationDataSourceSpList.at(row));
  QVariant var_return;
  switch(temp)
    {
      case WorkerStatus::Running:
        var_return = mp_movie->currentImage();
        break;

      default:
        break;
    }
  return var_return;
}


void
MsIdentificationTableModel::onMsIdentificationDataChanged()
{
  emit layoutAboutToBeChanged();
  emit layoutChanged();
}

void
MsIdentificationTableModel::changeWaitingQueue(int row)
{
  msp_workerStatus->changeWaitingQueue(row);
}

void
MsIdentificationTableModel::stopThreads()
{
  msp_workerStatus.get()->stopThreads();
}

QVariant
MsIdentificationTableModel::getMsRunStatisticsFollowingFormat(
  MsRunSp msrun_sp, MsRunStatistics column) const
{
  if(msrun_sp->getMsDataFormat() == pappso::MsDataFormat::brukerTims)
    {
      if(column == MsRunStatistics::tic_spectra_ms1 ||
         column == MsRunStatistics::tic_spectra_ms2 ||
         column == MsRunStatistics::tic_spectra_ms3)
        {
          return QVariant("NA");
        }
      else
        {
          return msrun_sp->getMsRunStatistics(column);
        }
    }
  else
    {
      return msrun_sp->getMsRunStatistics(column);
    }
}
