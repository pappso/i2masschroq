
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QAbstractTableModel>
#include "../../../core/project.h"
#include <memory>
#include "ms_identification_worker/msidlistworkerstatus.h"
#include <QMovie>


/** \def msIdentificationListColumn list of available fields to display in ms
 * Identification list
 *
 */

enum class msIdentificationListColumn : std::int8_t
{
  align_groups                 = 0,  ///< msRun ID
  run_id                       = 1,  ///< msRun ID
  id_file                      = 2,  ///< file ID
  mzML                         = 3,  ///< accession
  engine_name                  = 4,  ///< engine name
  fasta_file                   = 5,  ///< fasta files
  sequence                     = 6,  ///< unique sequence count
  nbr_spectrum                 = 7,  ///< number of spectrum
  total_spectra_assigned       = 8,  ///< total_spectra_assigned
  percent_assign               = 9,  ///< percent of assignation
  msrun_total_spectra_assigned = 10, ///< all_spectra_assigned in this Msrun
  msrun_percent_assign         = 11, ///< percent of assignation in this Msrun
  id_msrun_id                  = 12, ///< identification of MsRunId
  nb_ms_1                      = 13, ///< number of ms1
  nb_ms_2                      = 14, ///< number of ms2
  nb_ms_3                      = 15, ///< number of ms3
  nb_tic_1                     = 16, ///< number of TIC 1
  nb_tic_2                     = 17, ///< number of TIC 2
  nb_tic_3                     = 18, ///< number of TIC 3
  sample_name                  = 19, ///< sample name
  ms_instrument_name           = 20, ///< MS instrument name
  last                         = 21, ///< get list size
};

class MsIdentificationListWindow;

class MsIdentificationTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  MsIdentificationTableModel(MsIdentificationListWindow *ms_id_list_window,
                             Project *project);

  virtual ~MsIdentificationTableModel();
  virtual int
  rowCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual int
  columnCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  virtual QVariant data(const QModelIndex &index,
                        int role = Qt::DisplayRole) const override;
  virtual bool setData(const QModelIndex &index,
                       const QVariant &new_data,
                       int role = Qt::EditRole) override;
  Qt::ItemFlags flags(const QModelIndex &idx) const override;
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);
  static int getColumnIndex(msIdentificationListColumn column);
  static msIdentificationListColumn
  getMsIdentificationListColumn(std::int8_t column);

  QVariant getMsRunStatisticsNumber(int row, MsRunStatistics column) const;
  QVariant showMsRunStatisticsStatus(int row) const;

  void setIdentificationDataSourceSpList(
    std::vector<IdentificationDataSourceSp> &identificationDataSourceSpList);

  std::vector<IdentificationDataSourceSp>
  getIdentificationDataSourceSpList() const;

  /** @brief stops cleanly any threads running on identification list
   */
  void stopThreads();

  public slots:
  void onMsIdentificationDataChanged();
  void changeWaitingQueue(int row);

  signals:
  void ComputeMsNumber(IdentificationDataSourceSp identificationDataSource);
  void projectStatusChanged() const;
  void alignmentGroupChanged();

  private:
  static int getColumnWidth(int column);
  QVariant getMsRunStatisticsFollowingFormat(MsRunSp msrun_sp,
                                             MsRunStatistics column) const;

  private:
  static std::vector<msIdentificationListColumn> m_columnOrder;
  std::vector<IdentificationDataSourceSp> m_identificationDataSourceSpList;
  MsIdentificationListWindow *m_ms_id_list_window;
  std::shared_ptr<MsIdListWorkerStatus> msp_workerStatus = nullptr;
  QMovie *mp_movie;
  Project *mp_project;
};
