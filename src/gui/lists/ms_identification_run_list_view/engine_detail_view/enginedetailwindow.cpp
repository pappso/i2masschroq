
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#include "ui_engine_detail_view.h"
#include "enginedetailwindow.h"
#include "gui/widgets/engines_view/xtandemparamwidget.h"
#include "gui/widgets/engines_view/noengineparamwidget.h"
#include "../../../mainwindow.h"
//#include <QSettings>
#include <QCloseEvent>


EngineDetailWindow::EngineDetailWindow(ProjectWindow *parent)
  : QMainWindow(parent), ui(new Ui::EngineDetailView)
{
  m_project_window = parent;
  ui->setupUi(this);
  setWindowIcon(QIcon(":/i2masschroq_icon/resources/i2MassChroQ_icon.svg"));
}


EngineDetailWindow::~EngineDetailWindow()
{
  delete m_preset_dialog;
}

void
EngineDetailWindow::setIdentificationEngineParam(
  IdentificationDataSourceSp *identificationEngine)
{
  m_identificationEngine = identificationEngine;
  updateDisplay();
}

void
EngineDetailWindow::doEditXtandemParameters()
{
  m_preset_dialog = new EditTandemPresetDialog(this);

  // m_preset_dialog->newTandemParameters(m_xtandem_parameters);
  m_preset_dialog->setProjectParameters(
    m_projectIdentificationEngineParameters);
  m_preset_dialog->show();
}


void
EngineDetailWindow::updateDisplay()
{
  m_identificationEngine->get()->fillProjectParameters(
    m_projectIdentificationEngineParameters);

  ui->labelEngineName->setText(
    m_identificationEngine->get()->getIdentificationEngineName());
  ui->labelEngineVersion->setText(
    m_identificationEngine->get()->getIdentificationEngineVersion());

  if(m_identificationEngine->get()->getIdentificationEngine() ==
     IdentificationEngine::XTandem)
    {
      XtandemParamWidget *xtandem_view_widget;
      xtandem_view_widget = new XtandemParamWidget(this);
      ui->verticalLayout->insertWidget(1, xtandem_view_widget);

      xtandem_view_widget->setProjectParameters(
        m_projectIdentificationEngineParameters);
      ui->labelPreset->setText("");
      ui->actionedit_X_Tandem_parameters->setEnabled(true);
    }
  else
    {
      NoEngineParamWidget *no_engine_view_widget;
      QString message =
        m_identificationEngine->get()->getIdentificationEngineName() +
        " parameters view isn't implemented\nComing Soon";
      ui->labelPreset->setText("No preset file found");
      no_engine_view_widget = new NoEngineParamWidget(this, message);
      ui->verticalLayout->insertWidget(1, no_engine_view_widget);
    }
}


void
EngineDetailWindow::closeEvent(QCloseEvent *event)
{
  QWidget *temp_widget = ui->verticalLayout->itemAt(1)->widget();
  ui->verticalLayout->removeWidget(temp_widget);
  delete temp_widget;

  m_project_window->popEngineDetailView();
  event->accept();
}
