
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include "ui_engine_detail_view.h"
#include <core/identification_sources/identificationdatasource.h>
#include <gui/edit/edit_tandem_preset_dialog/edittandempresetdialog.h>

class ProjectWindow;

// http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui
{
class EngineDetailView;
}

class EngineDetailWindow : public QMainWindow
{

  Q_OBJECT

  public:
  explicit EngineDetailWindow(ProjectWindow *parent = 0);
  ~EngineDetailWindow();
  void setIdentificationEngineParam(
    IdentificationDataSourceSp *identificationEngine);

  public slots:
  void doEditXtandemParameters();

  protected:
  void updateDisplay();

  private:
  void closeEvent(QCloseEvent *event);

  Ui::EngineDetailView *ui;
  EditTandemPresetDialog *m_preset_dialog = nullptr;
  ProjectWindow *m_project_window;
  IdentificationDataSourceSp *m_identificationEngine = nullptr;
  pappso::ProjectParameters m_projectIdentificationEngineParameters;
};
