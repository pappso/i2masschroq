
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QAbstractTableModel>
#include "../../../core/project.h"


/** \def PeptideListColumn list of available fields to display in peptide list
 *
 */

enum class PeptideListColumn : std::int8_t
{
  checked             = 0, ///< manual checked
  peptide_grouping_id = 1, ///< manual checked
  engine              = 2,
  sample              = 3,
  scan                = 4,
  spectrum_index      = 5, ///< spectrum index
  rtmin               = 6,
  rt                  = 7,
  ion_mobility_begin  = 8,
  ion_mobility_end    = 9,
  collision_energy    = 10,
  charge              = 11,
  experimental_mz     = 12,
  sequence_nter       = 13,
  sequence            = 14,
  sequence_cter       = 15,
  chemical_formula    = 16,
  modifs              = 17,
  label               = 18,
  start               = 19,
  length              = 20,
  used                = 21,
  subgroups           = 22,
  Evalue              = 23,
  svmprob             = 24,
  qvalue              = 25,
  experimental_mhplus = 26,
  theoretical_mhplus  = 27,
  delta_mhplus        = 28,
  delta_ppm           = 29,


  peptide_prophet_probability       = 30, ///< no PSI MS description
  peptide_inter_prophet_probability = 31, ///< no PSI MS description
  tandem_hyperscore                 = 32, ///< X!Tandem hyperscore MS:1001331
  mascot_score = 33, ///< PSI-MS MS:1001171 mascot:score 56.16
  mascot_expectation_value =
    34, ///< PSI-MS MS:1001172 mascot:expectation value 2.42102904673618e-006
  omssa_evalue        = 35, ///< MS:1001328  "OMSSA E-value." [PSI:PI]
  omssa_pvalue        = 36, ///< MS:1001329  "OMSSA p-value." [PSI:PI]
  msgfplus_raw        = 37, ///< MS:1002049  "MS-GF raw score." [PSI:PI]
  msgfplus_denovo     = 38, ///< MS:1002050  "MS-GF de novo score." [PSI:PI]
  msgfplus_energy     = 39, ///< MS:1002051  "MS-GF energy score." [PSI:PI]
  msgfplus_SpecEValue = 40, ///< MS:1002052  "MS-GF spectral E-value." [PSI:PI]
  msgfplus_EValue     = 41, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  msgfplus_isotope_error = 42, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  comet_xcorr   = 43, ///< MS:1002252  "The Comet result 'XCorr'." [PSI:PI]
  comet_deltacn = 44, ///< MS:1002253  "The Comet result 'DeltaCn'." [PSI:PI]
  comet_deltacnstar =
    45, ///< MS:1002254  "The Comet result 'DeltaCnStar'." [PSI:PI]
  comet_spscore = 46, ///< MS:1002255  "The Comet result 'SpScore'." [PSI:PI]
  comet_sprank  = 47, ///< MS:1002256  "The Comet result 'SpRank'." [PSI:PI]
  comet_expectation_value =
    48, ///< MS:1002257  "The Comet result 'Expectation value'." [PSI:PI]
  deepprot_original_count      = 49, ///< number of matched peaks before specfit
  deepprot_fitted_count        = 50, ///< number of matched peaks after specfit
  deepprot_match_type          = 51, ///< DeepProt spectrum match type
  deepprot_status              = 52, ///< DeepProt PSM status
  deepprot_mass_delta          = 53, ///< DeepProt mass delta
  deepprot_delta_positions     = 54, ///< DeepProt mass delta positions
  sage_sage_discriminant_score = 55, ///<
  sage_posterior_error         = 56, ///<
  sage_spectrum_q              = 57, ///<
  sage_peptide_q               = 58, ///<
  sage_predicted_rt            = 59, ///<
  sage_isotope_error           = 60, ///<

  last = 61
};

class PeptideListWindow;

class PeptideTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PeptideTableModel(PeptideListWindow *parent);
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  static const QString getTitle(PeptideListColumn column);
  static const QString getDescription(PeptideListColumn column);
  static PeptideListColumn getPeptideListColumn(std::int8_t column);
  static int getColumnIndex(PeptideListColumn column);


  bool hasColumn(PeptideListColumn column);

  void setProteinMatch(ProteinMatch *p_protein_match);
  ProteinMatch *getProteinMatch();
  signals:
  void peptideEvidenceClicked(PeptideEvidence *p_peptide_evidence);

  public slots:
  void onPeptideDataChanged();

  private:
  static int getColumnWidth(PeptideListColumn column);

  private:
  static std::vector<PeptideListColumn> m_columnOrder;
  const std::int8_t m_nonIdentificationEngineColumnLimit = 30;
  ProteinMatch *_p_protein_match                         = nullptr;
  PeptideListWindow *_p_peptide_list_window              = nullptr;
  // contains columns to display (present in this peptide match)
  std::set<PeptideListColumn> _engine_columns_to_display;
};
