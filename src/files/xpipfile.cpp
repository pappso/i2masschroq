/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "xpipfile.h"

XpipFile::XpipFile(const QUrl &xpip_source) : _xpip_source(xpip_source)
{
}


XpipFile::XpipFile(const QFileInfo &xpip_source)
  : _xpip_source(xpip_source.absoluteFilePath())
{
  qDebug() << "XpipFile::XpipFile begin " << _xpip_source.toString() << " "
           << xpip_source.absoluteFilePath();
}
XpipFile::XpipFile(const XpipFile &other) : _xpip_source(other._xpip_source)
{
}
XpipFile::~XpipFile()
{
}

ProjectSp
XpipFile::getProjectSp(WorkMonitorInterface *p_monitor) const
{
  qDebug() << "XpipFile::getProjectSp begin " << _xpip_source.toString();
  ProjectSp project_sp = Project().makeProjectSp();

  project_sp.get()->readXpipFile(p_monitor, QFileInfo(_xpip_source.toString()));

  return (project_sp);
}
