
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "groupingpeptidemass.h"
#include "../core/proteinmatch.h"
#include "../core/peptidematch.h"
#include <QDebug>
#include <pappsomspp/grouping/grpgroupingmonitor.h>


class GrpGroupingMonitor : public pappso::GrpGroupingMonitorInterface
{
  public:
  GrpGroupingMonitor(WorkMonitorInterface *p_work_monitor)
  {
    _p_work_monitor = p_work_monitor;
  };
  ~GrpGroupingMonitor(){};
  virtual void
  startGrouping(std::size_t total_number_protein,
                std::size_t total_number_peptide) override
  {
    _p_work_monitor->message(
      QObject::tr("grouping %1 proteins containing %2 unique peptides")
        .arg(total_number_protein)
        .arg(total_number_peptide));
    _total_number_proteins = total_number_protein;
  };
  virtual void groupingProtein() override{};
  virtual void
  startRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group) override
  {
    _total_number_group = total_number_group;
  };
  virtual void
  stopRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group) override
  {
    _total_number_group = total_number_group;
  };
  virtual void
  removingNonInformativeSubGroupsInGroup() override
  {
    _p_work_monitor->message(
      QObject::tr("removing non informative subgroups in %1 groups "
                  "representing %2 proteins")
        .arg(_total_number_group)
        .arg(_total_number_proteins));
  };
  virtual void
  startNumberingAllGroups(std::size_t total_number_group
                          [[maybe_unused]]) override
  {
    _p_work_monitor->message(
      QObject::tr("numbering in %1 groups representing %2 proteins")
        .arg(_total_number_group)
        .arg(_total_number_proteins));
  };
  virtual void stopGrouping() override{};

  private:
  WorkMonitorInterface *_p_work_monitor;
  std::size_t _total_number_proteins;
  std::size_t _total_number_group;
};

GroupingPeptideMass::GroupingPeptideMass(
  ContaminantRemovalMode contaminantRemovalMode,
  WorkMonitorInterface *p_work_monitor)
  : GroupingExperiment(contaminantRemovalMode, p_work_monitor)
{

  //_p_monitor = new pappso::GrpGroupingMonitor();
  _p_monitor        = new GrpGroupingMonitor(_p_work_monitor);
  _p_grp_experiment = new pappso::GrpExperiment(_p_monitor);
}

GroupingPeptideMass::~GroupingPeptideMass()
{
  delete _p_grp_experiment;
  delete _p_monitor;
}

pappso::GrpProteinSp &
GroupingPeptideMass::getGrpProteinSp(ProteinMatch *p_protein_match)
{
  return _p_grp_experiment->getGrpProteinSp(
    p_protein_match->getProteinXtpSp().get()->getAccession(),
    p_protein_match->getProteinXtpSp().get()->getDescription());
}
pappso::GrpPeptideSp &
GroupingPeptideMass::setGrpPeptide(pappso::GrpProteinSp proteinSp,
                                   PeptideEvidence *p_peptide_evidence)
{

  return _p_grp_experiment->setGrpPeptide(
    proteinSp,
    p_peptide_evidence->getPeptideXtpSp().get()->getSequence(),
    p_peptide_evidence->getPeptideXtpSp().get()->getGroupingMass());
}

void
GroupingPeptideMass::startGrouping()
{
  qDebug() << "GroupingPeptideMass::startGrouping begin";
  _p_grp_experiment->startGrouping();
  qDebug() << "GroupingPeptideMass::startGrouping end";
}

void
GroupingPeptideMass::addPostGroupingGrpProteinSpRemoval(
  pappso::GrpProteinSp sp_protein)
{
  _p_grp_experiment->addPostGroupingGrpProteinSpRemoval(sp_protein);
}
