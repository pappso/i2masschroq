/**
 * \file grouping/ptm/ptmmethylation.cpp
 * \date 16/06/2020
 * \author Olivier Langella
 * \brief PTM methylation
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "ptmacetylation.h"

PtmAcetylation::PtmAcetylation()
{
  //"MOD:00394" cvRef="PSI-MOD" name="acetylated residue"
  mp_acetylation = pappso::AaModification::getInstance("MOD:00394");
}

PtmAcetylation::PtmAcetylation(const PtmAcetylation &other)
  : PtmInterface(other)
{
}

PtmAcetylation::~PtmAcetylation()
{
}


std::vector<unsigned int>
PtmAcetylation::getPtmPositions(const PeptideMatch &peptide_match) const
{
  std::vector<unsigned int> position_list;
  std::vector<char> aa_list;
  aa_list.push_back('K');


  // find acetylations on Lysines :
  for(unsigned int position :
      peptide_match.getPeptideEvidence()
        ->getPeptideXtpSp()
        .get()
        ->getModificationPositionList(mp_acetylation, aa_list))
    {
      position_list.push_back(position);
    }

  // find acetylation on NTer AminoAcid
  const std::vector<pappso::AaModificationP> &first_aa_mod_list =
    peptide_match.getPeptideEvidence()
      ->getPeptideXtpSp()
      .get()
      ->getConstAa(0)
      .getModificationList();

  if(std::find(first_aa_mod_list.begin(),
               first_aa_mod_list.end(),
               mp_acetylation) != first_aa_mod_list.end())
    {
      position_list.push_back(0);
    }

  std::sort(position_list.begin(), position_list.end());
  auto last = std::unique(position_list.begin(), position_list.end());
  position_list.erase(last, position_list.end());
  return position_list;
}
