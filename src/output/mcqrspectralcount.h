/**
 * \file /output/mcqrspectralcount.h
 * \date 23/3/2018
 * \author Olivier Langella
 * \brief write simple TSV file for MassChroqR spectral count studies
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <odsstream/calcwriterinterface.h>
#include "../core/project.h"


/** \def ExternalDatabase external database references
 *
 */
enum class McqrSpectralCountProteinSet : std::int8_t
{
  allGroupedProteins  = 1, ///< all grouped proteins
  allProteinSubgroups = 2, ///< all protein subgroups
};


class McqrSpectralCount
{
  public:
  McqrSpectralCount(CalcWriterInterface *p_writer,
                    const Project *p_project,
                    McqrSpectralCountProteinSet protein_set);

  void writeSheet();

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeOneProtein(const GroupingGroup *p_group,
                       const ProteinMatch *p_protein_match);
  void writeOneProteinLabel(const pappso::GrpProtein *p_grp_protein,
                            const MsRun *p_msrun,
                            const Label *p_label,
                            const ProteinXtp *p_protein,
                            const ProteinMatch *p_protein_match);

  private:
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
  std::vector<MsRunSp> _ms_run_sp_list;
  LabelingMethodSp _sp_labelling_method;
  std::vector<Label *> _label_list;
  McqrSpectralCountProteinSet m_proteinSet;
};
