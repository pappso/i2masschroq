/**
 * \file /output/tidd/tidddataoutput.h
 * \date 29/9/2022
 * \author Olivier Langella
 * \brief write data in TIDD data table manner
 * (https://pubmed.ncbi.nlm.nih.gov/35354356/)
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>
#include <pappsomspp/psm/features/psmfeatures.h>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>


#pragma once

/**
 * @todo write docs
 */
class TiddDataOutput
{
  public:
  /**
   * Default constructor
   */
  TiddDataOutput(pappso::UiMonitorInterface *p_monitor,
                 CalcWriterInterface *p_writer,
                 const Project *p_project,
                 pappso::PrecisionPtr ms2precision,
                 double minimumMz);

  /**
   * Destructor
   */
  virtual ~TiddDataOutput();

  const std::vector<PeptideEvidence *> &getPeptideEvidenceList() const;

  private:
  void writeHeaders();
  void writeFeatures(const PeptideEvidence *p_peptide_evidence,
                     pappso::XtandemSpectrumProcess &spectrum_process);

  std::vector<std::shared_ptr<PeptideEvidence>>
  generatePeptideEvidenceSolutions(
    const PeptideEvidence *peptide_evidence) const;

  double checkInf(double input) const;

  void sortPeptideEvidenceList();

  private:
  struct MassSpectrumCache
  {
    pappso::MassSpectrumCstSPtr m_MassSpectrumCstSPtrCache;
    const MsRun *mp_msrun;
    std::size_t m_spectrum_index;
  };
  pappso::PsmFeatures m_psmFeatures;
  pappso::UiMonitorInterface *mp_monitor;
  const Project *mp_project;
  CalcWriterInterface *mp_writer;
  std::vector<PeptideEvidence *> m_peptideEvidenceList;


  std::list<pappso::PeptideIon> m_ionList;
  pappso::PrecisionPtr m_ms2precision;
  double m_minimumMz = 150;

  MassSpectrumCache m_MassSpectrumCache;
};
