/**
 * \file /output/mcqr/mcqrmetadataods.cpp
 * \date 30/05/2023
 * \author Olivier Langella
 * \brief generate metadata file for MCQR
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mcqrmetadataods.h"

McqrMetadataOds::McqrMetadataOds(CalcWriterInterface *p_writer,
                                 const McqrMetadataSet &metadata_set)
  : m_mcqrMetadataSet(metadata_set)
{
  qDebug();
  mp_writer = p_writer;

  qDebug();
  mp_writer->writeSheet("metadata");
  qDebug();
  writeHeaders();
  writeMcqrMetadata();
  qDebug();
}

McqrMetadataOds::~McqrMetadataOds()
{
}


void
McqrMetadataOds::writeHeaders()
{
  qDebug();
  mp_writer->writeLine();
  mp_writer->writeCell("msrun");
  mp_writer->writeCell("msrunfile");

  QStringList columns = m_mcqrMetadataSet.getOtherDataColumns();

  for(QString column : columns)
    {
      mp_writer->writeCell(column);
    }
  /*
  if(m_method == McqrLoadDataMode::label || m_method == McqrLoadDataMode::both)
    {
      mp_writer->writeCell("label");
    }
  if(m_method == McqrLoadDataMode::fraction ||
     m_method == McqrLoadDataMode::both)
    {
      mp_writer->writeCell("fraction");
      mp_writer->writeCell("fraction_order");
      mp_writer->writeCell("track");
      mp_writer->writeCell("track_order");
    }
    */
  qDebug();
}

void
McqrMetadataOds::writeMcqrMetadata()
{
  QStringList columns = m_mcqrMetadataSet.getOtherDataColumns();
  for(const McqrMetadata &metadata : m_mcqrMetadataSet.getMcqrMetadataList())
    {
      mp_writer->writeLine();
      mp_writer->writeCell(metadata.m_msrunId);
      mp_writer->writeCell(metadata.m_msrunFile);

      for(QString column : columns)
        {
          mp_writer->writeCell(metadata.m_otherData.at(column).toString());
        }
    }
}
