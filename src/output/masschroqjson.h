/**
 * \file /output/masschroqjson.h
 * \date 04/02/2025
 * \author Olivier Langella
 * \brief JSON file for MassChroQ
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../core/project.h"
#include "../core/labeling/labelingmethod.h"
#include "../utils/groupstore.h"
#include "../grouping/groupinggroup.h"
#include "../gui/xic_view/xic_widgets/zivywidget.h"
#include "../core/masschroq_run/masschroqfileparameters.h"
#include <QJsonDocument>

/**
 * @todo write docs
 */
class MassChroQjson
{
  public:
  MassChroQjson(const ProjectSp &sp_project);
  virtual ~MassChroQjson();


  void populateJsonDocument(QJsonDocument &document) const;

  private:
  void writeOneGroupStore(const GroupStore &group_store);
  void writePeptideListInGroupingGroup(const GroupingGroup &grouping_group);
  void writeActions(const MasschroqFileParameters &mcq_params);

  struct McqObserved
  {
    bool
    operator==(const McqObserved &other) const
    {
      return (msrun_p == other.msrun_p && scan == other.scan &&
              charge == other.charge &&
              is_spectrumIndex == other.is_spectrumIndex);
    };
    QString peptide_id;
    const MsRun *msrun_p;
    std::size_t scan;
    unsigned int charge;
    bool is_spectrumIndex;
    const Label *label_p;
  };

  struct McqPeptide
  {
    QString id;
    QStringList mods;
    QStringList prot_ids;
    QString seq;
    std::map<const Label *, QString> map_label;
    const pappso::Peptide *native_peptide;
    std::vector<McqObserved> observed_in;
  };


  private:
  LabelingMethod *mp_labelingMethod = nullptr;
  QJsonObject m_jsonProjectParameters;
  QJsonObject m_jsonAlignmentMethod;

  QJsonObject m_jsonQuantificationMethod;

  QJsonObject m_jsonMsRunList;
  QJsonObject m_jsonPeptideList;
  QJsonObject m_jsonProteinList;
  QJsonObject m_jsonMsRunPeptideList;
  QJsonObject m_jsonActions;


  std::map<const MsRun *, std::vector<McqObserved>>
    m_mapMsrun2peptideObservation;
};
