/**
 * \file /output/ods/validpsmsheet.h
 * \date 12/10/2022
 * \author Olivier Langella
 * \brief write all valid PSMs (not only grouped ones)
 * this displays target and decoy information
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "../../gui/lists/peptide_list_view/peptidelistwindow.h"
#include "odsexport.h"

/**
 * @todo write docs
 */
class ValidPsmSheet
{
  public:
  /**
   * @todo write docs
   */
  ValidPsmSheet(OdsExport *p_ods_export,
                CalcWriterInterface *p_writer,
                const Project *p_project);

  /**
   * @todo write docs
   */
  virtual ~ValidPsmSheet();


  protected:
  ValidPsmSheet(OdsExport *p_ods_export,
                CalcWriterInterface *p_writer,
                const Project *p_project,
                const QString &sheet_name);
  void writeHeaders();
  void writePeptideEvidence(const PeptideEvidence *p_peptide_evidence);
  void writeCellHeader(PeptideListColumn column);

  protected:
  OdsExport *mp_odsExport;
  const Project *mp_project;
  CalcWriterInterface *mp_writer;
  std::vector<PeptideEvidence *> m_peptideEvidenceList;
  std::vector<PeptideEvidence *> m_peptideEvidenceListTarget;
  bool m_displaySpecOmsInformations = true;
};
