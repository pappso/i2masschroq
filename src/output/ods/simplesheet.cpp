/**
 * \file output/ods/simplesheet.h
 * \date 11/4/2017
 * \author Olivier Langella
 * \brief ODS simple sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "simplesheet.h"
#include "../../core/identificationgroup.h"
#include "../../utils/utils.h"

SimpleSheet::SimpleSheet(CalcWriterInterface *p_writer,
                         const Project *p_project,
                         SubMonitor &sub_monitor)
  : mp_project(p_project)
{
  mp_writer = p_writer;
  p_writer->writeSheet("simple");

  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();

  for(IdentificationGroup *p_ident : identification_list)
    {
      sub_monitor.setStatus("writing");
      writeIdentificationGroup(p_ident);
    }
}

void
SimpleSheet::writeIdentificationGroup(IdentificationGroup *p_ident)
{
  qDebug();
  mp_writer->writeCell("msrun");
  mp_writer->writeCell("sample name");
  mp_writer->writeCell("spectrum index");
  mp_writer->writeCell("identification engine");
  mp_writer->writeCell("grouping id");
  mp_writer->writeCell("sequenceLI");
  mp_writer->writeCell("sequence");
  mp_writer->writeCell("modif");
  mp_writer->writeCell("charge");
  mp_writer->writeCell("delta");
  mp_writer->writeCell("decoy");
  mp_writer->writeCell("target");
  mp_writer->writeCell("accessions");
  mp_writer->writeLine();


  for(IdentificationDataSource *p_identDataSource :
      p_ident->getIdentificationDataSourceList())
    {
      qDebug();
      PeptideEvidenceStore &pe_store =
        p_identDataSource->getPeptideEvidenceStore();
      for(const PeptideEvidenceSp &pesp : pe_store.getPeptideEvidenceList())
        {
          if(pesp.get()->getValidationState() >= ValidationState::valid)
            {
              qDebug();
              mp_writer->writeCell(pesp.get()->getMsRunP()->getXmlId());
              mp_writer->writeCell(pesp.get()->getMsRunP()->getSampleName());
              mp_writer->writeCell(pesp.get()->getSpectrumIndexByScanNumber());
              mp_writer->writeCell(Utils::getIdentificationEngineName(
                pesp.get()->getIdentificationEngine()));
              if(pesp.get()->getGrpPeptideSp().get() != nullptr)
                {
                  mp_writer->writeCell(
                    pesp.get()->getGrpPeptideSp().get()->getGroupingId());
                }
              else
                {
                  mp_writer->writeEmptyCell();
                }
              mp_writer->writeCell(
                pesp.get()->getPeptideXtpSp().get()->getSequenceLi());
              mp_writer->writeCell(
                pesp.get()->getPeptideXtpSp().get()->getSequence());
              mp_writer->writeCell(
                pesp.get()->getPeptideXtpSp().get()->getModifString());
              mp_writer->writeCell((int)pesp.get()->getCharge());
              mp_writer->writeCell(pesp.get()->getDeltaMass());


              writeInfoOnProteins(pesp.get(), p_ident->getProteinMatchList());
              mp_writer->writeLine();
            }
        }
    }
  qDebug();
}

void
SimpleSheet::writeInfoOnProteins(
  PeptideEvidence *p_peptide_evidence,
  std::vector<ProteinMatch *> &protein_match_list)
{
  bool is_decoy  = false;
  bool is_target = false;

  QStringList accession_list;

  for(ProteinMatch *protein_match : protein_match_list)
    {
      if(protein_match->contains(p_peptide_evidence))
        {
          const ProteinXtp *p_protein = protein_match->getProteinXtpSp().get();
          if(p_protein->isDecoy())
            {
              is_decoy = true;
            }
          else
            {
              is_target = true;
            }
          accession_list << p_protein->getAccession();
        }
    }

  mp_writer->writeCell(is_decoy);
  mp_writer->writeCell(is_target);
  mp_writer->writeCell(accession_list.join(" "));
}
