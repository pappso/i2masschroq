/**
 * \file output/ods/mcqrmetadasheet.h
 * \date 13/01/2021
 * \author Thomas
 * \brief ODS with Mcqr mandatory metadata template
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <odsstream/calcwriterinterface.h>
#include "../../core/project.h"
#include "../../utils/types.h"

class McqrMetadataSheet
{
  public:
  McqrMetadataSheet(CalcWriterInterface *p_writer,
                    std::vector<MsRunSp> msruns,
                    McqrLoadDataMode method);

  private:
  void writeHeaders();
  void writeMsRunUsed();

  private:
  CalcWriterInterface *mp_writer;
  std::vector<MsRunSp> m_msrunSpList;
  McqrLoadDataMode m_method;
};
