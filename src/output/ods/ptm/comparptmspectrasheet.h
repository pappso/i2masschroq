/**
 * \file output/ods/ptm/comparptmspectrasheet.h
 * \date 17/10/2024
 * \author Olivier Langella
 * \brief ods output to compare phospho ptm spectra
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once


#include "../../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../../grouping/ptm/ptmisland.h"
#include "../odsexport.h"
#include "../spectrasheet.h"


/**
 * @todo write docs
 */
class ComparPtmSpectraSheet
{
  public:
  /**
   * Default constructor
   */
  ComparPtmSpectraSheet(OdsExport *p_ods_export,
                        CalcWriterInterface *p_writer,
                        const Project *p_project,
                        SubMonitor &sub_monitor);

  /**
   * Destructor
   */
  virtual ~ComparPtmSpectraSheet();


  void writeSheet();

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void
  writePtmIslandSubgroupSp(const PtmIslandSubgroupSp &ptm_island_subgroup_sp);
  void writeBestPeptideEvidence(
    const PtmSampleScan &ptm_sample_scan,
    std::set<const PtmIsland *> &ptmisland_occurence_list);

  protected:
  OdsExport *mp_odsExport;
  const Project *mp_project;

  SubMonitor &m_subMonitor;
  CalcWriterInterface *mp_calcWriterInterface;
  QString m_titleSheet;

  QString m_firstCellCoordinate;

  IdentificationGroup *mp_currentIdentificationGroup;

  std::vector<MsRunSp> m_currentMsRunSpList;
  const PtmGroupingExperiment *mp_currentPtmGroupingExperiment;

  LabelingMethodSp msp_labelingMethod;

  std::vector<Label *> m_labelPtrList;
};
