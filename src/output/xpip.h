/**
 * \file output/xpip.h
 * \date 13/4/2017
 * \author Olivier Langella
 * \brief XPIP writer
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef XPIP_H
#define XPIP_H

#include <QXmlStreamWriter>
#include <QFile>
#include <QString>
#include "../core/project.h"

class Xpip
{
  public:
  Xpip(const QString &out_filename);
  ~Xpip();

  void write(ProjectSp sp_project);
  void close();

  private:
  void writeFilterParameters(const AutomaticFilterParameters &filters);
  void writeDescription();
  void writeCounts();

  void writeMsrunList(const MsRunStore &msrun_store);
  void writeMsrunAlignmentGroup(const MsRunAlignmentGroupSp alignment_group,
                                int count_group);
  void
  writeAlignmentGroupParameters(const MsRunAlignmentGroupSp alignment_group);

  void writeIdentificationDataSourceList(
    const IdentificationDataSourceStore &ident_store);
  void writeFastaFileList(const FastaFileStore &fasta_store);
  void writeDoubleAttribute(const QString &attribute,
                            pappso::pappso_double value);
  void writeBooleanAttribute(const QString &attribute, bool value);

  void writeProteinList();
  void writePeptideList();
  void writeLabelingMethod();
  void writeIdentificationGroupList();
  void
  writeIdentificationGroup(const IdentificationGroup *p_identification_group);
  void
  writePeptideEvidenceList(const IdentificationDataSource *p_ident_data_source);
  void writePeptideMatch(const PeptideMatch &peptide_match);
  void writePeptideEvidence(const PeptideEvidence *p_peptide_evidence);

  QString getPeptideId(std::size_t crc_peptide) const;

  private:
  QFile *_output_file;
  QXmlStreamWriter *_output_stream;
  ProjectSp _sp_project;

  std::map<pappso::AaModificationP, QString> _map_modifications;
  std::map<const PeptideXtp *, QString> _map_peptides;
  std::map<const PeptideEvidence *, QString> _map_peptide_evidences;
};

#endif // XPIP_H
