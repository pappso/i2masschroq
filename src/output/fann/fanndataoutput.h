/**
 * \file /output/fann/fanndataoutput.h
 * \date 18/07/2022
 * \author Olivier Langella
 * \brief write data for fann c++ library
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>
#include <pappsomspp/psm/features/psmfeatures.h>


#pragma once


/**
 * @todo write docs
 */
class FannDataOutput
{
  public:
  /**
   * Default constructor
   */
  FannDataOutput(CalcWriterInterface *p_writer, const Project *p_project);

  /**
   * Destructor
   */
  ~FannDataOutput();

  private:
  void writeFeatures(const PeptideEvidence *p_peptide_evidence,
                     pappso::XtandemSpectrumProcess &spectrum_process);


  std::vector<std::shared_ptr<PeptideEvidence>>
  generatePeptideEvidenceSolutions(
    const PeptideEvidence *peptide_evidence) const;

  private:
  const Project *mp_project;
  CalcWriterInterface *mp_writer;


  pappso::PsmFeatures m_psmFeatures;
};
