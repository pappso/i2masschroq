# CMake script for i2MassChroQ
# Author: Olivier Langella
# Created: 03/03/2015

#set(EXPERIMENTAL_FEATURES_ON 0)
if(EXPERIMENTAL_FEATURES_ON MATCHES "1")
    set(EXPERIMENTAL_FEATURES_ON 1)
endif(EXPERIMENTAL_FEATURES_ON MATCHES "1")

if(CMAKE_BUILD_TYPE MATCHES "Debug")
    set(EXPERIMENTAL_FEATURES_ON 1)
endif(CMAKE_BUILD_TYPE MATCHES "Debug")

SET(i2MassChroQ_RCCS i2masschroq.qrc)
# Appeler automatique le moc quand nécessaire
#SET(CMAKE_AUTOMOC ON)
# Les fichiers générés par le moc sont générés dans le dossier bin, dire à CMake de toujours 	
# inclure les entêtes de ce dossier
#SET(CMAKE_INCLUDE_CURRENT_DIR ON)

qt6_add_resources(i2MassChroQ_RCC_SRCS ${i2MassChroQ_RCCS})

if(QCustomPlotQt6_FOUND)
  message("QCustomPlotQt6_INCLUDES found ${QCustomPlotQt6_INCLUDE_DIR}")
else(QCustomPlotQt6_FOUND)
  message("QCustomPlotQt6_FOUND library not found")
endif(QCustomPlotQt6_FOUND)


if(Cutelee6Qt6_FOUND)
else(Cutelee6Qt6_FOUND)
  message("Cutelee6Qt6_FOUND library not found")
endif(Cutelee6Qt6_FOUND)


#sudo apt-get install libpappsomspp-dev
if(PappsoMSpp_FOUND)
else(PappsoMSpp_FOUND)
  message("PAPPSOms++ library not found")
  message("did you apt-get install libpappsomspp-dev ?")
endif(PappsoMSpp_FOUND)


if(PappsoMSppWidget_FOUND)
else(PappsoMSppWidget_FOUND)
  message("PAPPSOms++ widget library not found")
  message("did you apt-get install libpappsomspp-widget-dev ?")
endif(PappsoMSppWidget_FOUND)

message("PAPPSOms++ library : ${PappsoMSpp_LIBRARIES}")


add_subdirectory(cli) 


configure_file(${CMAKE_SOURCE_DIR}/src/config.h.cmake ${CMAKE_SOURCE_DIR}/src/config.h)

# File list
set(CPP_FILES

  cli/export/cborstreamreaderbase.cpp
  cli/export/cborstreamreadercompartsv.cpp
  cli/export/cborstreamreadertsv.cpp

  core/automaticfilterparameters.cpp
  core/condor_process/condorprocess.cpp
  core/condor_process/condorqueueparser.cpp
  core/identificationgroup.cpp
  core/identification_sources/identificationdatasource.cpp
  core/identification_sources/identificationmascotdatfile.cpp
  core/identification_sources/identificationmzidentmlfile.cpp
  core/identification_sources/identificationpepxmlfile.cpp
  core/identification_sources/identificationpwizfile.cpp
  core/identification_sources/identificationsagejsonfile.cpp
  core/identification_sources/identificationxtandemfile.cpp
  core/labeling/label.cpp
  core/labeling/labelingmethod.cpp
  core/masschroq_run/masschroqbatchprocess.cpp
  core/masschroq_run/masschroqfileparameters.cpp
  core/masschroq_run/masschroqslurmprocess.cpp
  core/mcqr/mcqrmetadata.cpp
  core/mcqr/mcqrmetadataset.cpp
  core/qvalue/computeqvalues.cpp
  core/tandem_run/tandembatchprocess.cpp
  core/tandem_run/tandemcondorprocess.cpp
  core/tandem_run/tandemparameters.cpp
  core/tandem_run/tandemrun.cpp
  core/tandem_run/tandemslurmprocess.cpp
  core/alignmentgroup.cpp
  core/msrun.cpp
  core/peptideevidence.cpp
  core/peptidematch.cpp
  core/peptidextp.cpp
  core/project.cpp
  core/proteinmatch.cpp
  core/proteinxtp.cpp
  core/sequencedatabase.cpp
  core/slurm_process/slurmprocess.cpp
  files/fastafile.cpp
  files/tandemparametersfile.cpp
  files/xpipfile.cpp
  grouping/groupingexperiment.cpp
  grouping/ptm/ptmacetylation.cpp
  grouping/ptm/ptminterface.cpp
  grouping/groupinggroup.cpp
  grouping/groupingpeptidemass.cpp
  grouping/ptm/ptmgroupingexperiment.cpp
  grouping/ptm/ptmisland.cpp
  grouping/ptm/ptmislandgroup.cpp
  grouping/ptm/ptmislandsubgroup.cpp
  grouping/ptm/ptmphospho.cpp
  grouping/ptm/ptmsamplescan.cpp
  input/mascot/mascotdatparser.cpp
  input/mascot/mimeparser.cpp
  input/masschroq/masschroqcborinfoparser.cpp
  input/masschroq/masschroqmlinfoparser.cpp
  input/mcqr/metadataodsreader.cpp
  input/msstats/msstatstsvtranslator.cpp
  input/msstats/tsvtranslator.cpp
  input/mzidentml/msrunlocationreader.cpp
  input/mzidentml/mzidentmlreader.cpp
  input/pepxml/pepxmlreader.cpp
  input/sage/sagefilereader.cpp
  input/sage/sagereader.cpp
  input/sage/sagetsvhandler.cpp
  input/tandem/tandeminfoparser.cpp
  input/tandem/tandemparamparser.cpp
  input/tandem/tandemreader.cpp
  input/tidd/tiddresultreader.cpp
  #  input/identificationpwizreader.cpp
  # input/pepxmlsaxhandler.cpp
  input/xpipreader.cpp
  #input/xpipsaxhandler.cpp
  # input/xtandemsaxhandler.cpp
  # input/xtpxpipsaxhandler.cpp
  # output/fann/fanndataoutput.cpp
  output/mcqr/mcqrmetadataods.cpp
  output/mcqr/mcqrscprotein.cpp
  output/mcqr/mcqrscpeptide.cpp
  output/ods/mass_delta/validmassdeltasheet.cpp
  output/ods/mass_delta/notvalidmassdeltasheet.cpp
  output/ods/ptm/comparptmspectrasheet.cpp
  output/ods/ptm/ptmislandsheet.cpp
  output/ods/ptm/ptmspectrasheet.cpp
  output/ods/comparbasesheet.cpp
  output/ods/comparspecificspectrasheet.cpp
  output/ods/comparspectrabypeptide.cpp
  output/ods/comparspectrasheet.cpp
  output/ods/groupingsheet.cpp
  output/ods/infosheet.cpp
  output/ods/mcqrmetadatasheet.cpp
  output/ods/odsexport.cpp
  output/ods/peptidepossheet.cpp
  output/ods/peptidesheet.cpp
  output/ods/proteinsheet.cpp
  output/ods/qvaluessheet.cpp
  output/ods/samplesheet.cpp
  output/ods/simplesheet.cpp
  output/ods/spectrasheet.cpp
  output/ods/spectrasheetall.cpp
  output/ods/validpsmsheet.cpp
  output/ods/xicareasheet.cpp
  output/ods/xicinfosheet.cpp
  output/tidd/tidddataoutput.cpp
  output/exportfastafile.cpp
  output/masschroqjson.cpp
  output/masschroqml.cpp
  output/masschroqprm.cpp
  output/mcqrspectralcount.cpp
  output/proticdbml.cpp
  output/xpip.cpp
  utils/fastafilestore.cpp
  utils/identificationdatasourcestore.cpp
  utils/groupstore.cpp
  utils/httpversion.cpp
  utils/msrunstatisticshandler.cpp
  utils/msrunstore.cpp
  utils/peptideevidencestore.cpp
  utils/peptidestore.cpp
  utils/proteinstore.cpp
  utils/ptmbuilder.cpp
  utils/utils.cpp
  )

# we need this to be able to include headers produced by uic in our code
#(CMAKE_BINARY_DIR holds a path to the build directory, while INCLUDE_DIRECTORIES() works just like INCLUDEPATH from qmake)

set(i2MassChroQ_SRCS
  ./core/mcqr/mcqrqprocess.cpp
  ./core/tidd/tiddcomputing.cpp
  ./gui/about_dialog/aboutdialog.cpp
  ./gui/choose_modification_dialog/choosemodificationdialog.cpp
  ./gui/edit/deepprot_gui/modification_browser/modificationbrowser.cpp
  ./gui/edit/deepprot_gui/deepprotgui.cpp
  ./gui/edit/edit_label_methods/editlabelmethods.cpp
  ./gui/edit/edit_modifications/editmodifications.cpp
  ./gui/edit/edit_settings/editsettings.cpp
  ./gui/edit/edit_tandem_preset_dialog/edittandempresetdialog.cpp
  ./gui/export/export_spreadsheet_dialog/exportspreadsheetdialog.cpp
  ./gui/load_results_dialog/loadresultsdialog.cpp
  ./gui/lists/ms_identification_run_list_view/msidentificationlistwindow.cpp
  ./gui/lists/ms_identification_run_list_view/msidentificationtablemodel.cpp
  ./gui/lists/ms_identification_run_list_view/msidentificationtableproxymodel.cpp
  ./gui/lists/ms_identification_run_list_view/ms_identification_worker/msidlistworkerstatus.cpp
  ./gui/lists/ms_identification_run_list_view/engine_detail_view/enginedetailwindow.cpp
  ./gui/lists/peptide_list_view/peptidelistwindow.cpp
  ./gui/lists/peptide_list_view/peptidetablemodel.cpp
  ./gui/lists/peptide_list_view/peptidetableproxymodel.cpp
  ./gui/lists/protein_list_view/proteinlistwindow.cpp
  ./gui/lists/protein_list_view/proteintablemodel.cpp
  ./gui/lists/protein_list_view/proteintableproxymodel.cpp
  ./gui/masschroq_run_dialog/masschroqrundialog.cpp
  ./gui/mainwindow.cpp
  ./gui/peptide_detail_view/peptidewindow.cpp
  ./gui/project_view/identification_group_widget/identificationgroupwidget.cpp
  ./gui/project_view/projectwindow.cpp
  ./gui/protein_view/proteinwindow.cpp
  ./gui/ptm_island_list_view/ptmislandlistwindow.cpp
  ./gui/ptm_island_list_view/ptmislandtablemodel.cpp
  ./gui/ptm_island_list_view/ptmislandproxymodel.cpp
  ./gui/ptm_peptide_list_view/ptmpeptidelistwindow.cpp
  ./gui/ptm_peptide_list_view/ptmpeptidetablemodel.cpp
  ./gui/ptm_peptide_list_view/ptmpeptidetableproxymodel.cpp
  ./gui/ptm_peptide_list_view/ptmsequencedelegate.cpp
  ./gui/run/rworkflowrunviewbase.cpp
  ./gui/run/mcqr/mcqrrunviewng.cpp
  ./gui/run/mcqr_spectral_count/mcqrscrunview.cpp
  ./gui/run/msstats/msstatsrunview.cpp
  ./gui/tandem_run_dialog/tandemrundialog.cpp
  ./gui/waiting_message_dialog/waitingmessagedialog.cpp
  ./gui/widgets/Alignment_group_menu/alignmentgroupmenu.cpp
  ./gui/widgets/automatic_filter_widget/automaticfilterwidget.cpp
  ./gui/widgets/engines_view/xtandemparamwidget.cpp
  ./gui/widgets/engines_view/noengineparamwidget.cpp
  ./gui/widgets/qcp_massdelta/massdeltawidget.cpp
  ./gui/widgets/qcp_massdelta/qcpmassdelta.cpp
  ./gui/widgets/contaminant_widget/contaminantwidget.cpp
  ./gui/widgets/decoy_widget/decoywidget.cpp
  ./gui/widgets/xic_extraction_method_widget/xicextractionmethodwidget.cpp
  ./gui/widgets/massitemdelegate.cpp
  ./gui/widgets/widget_rprocess/mcqrmetadatafactorcombobox.cpp
  ./gui/widgets/widget_rprocess/mcqrmetadatafactorlistview.cpp
  ./gui/widgets/widget_rprocess/mcqrmetadatamsruncombobox.cpp
  ./gui/widgets/widget_rprocess/mcqrmetadatamsrunlistview.cpp
  ./gui/widgets/widget_rprocess/widgetrprocess.cpp
  ./gui/xic_view/xic_box/xicbox.cpp
  ./gui/xic_view/xic_widgets/zivydialog.cpp
  ./gui/xic_view/xic_widgets/zivywidget.cpp
  ./gui/xic_view/xic_widgets/xicexportparam.cpp
  ./gui/xic_view/xicwindow.cpp
  ./gui/xic_view/xicworkerthread.cpp
  ./gui/workerthread.cpp
  ./utils/workmonitor.cpp
  )

set(GUI_UIS
  ./gui/about_dialog/about_dialog.ui
  ./gui/choose_modification_dialog/choose_modification_dialog.ui
  ./gui/edit/deepprot_gui/modification_browser/uimodificationbrowser.ui
  ./gui/edit/deepprot_gui/deepprot_gui.ui
  ./gui/edit/edit_alignment_param/masschroq_param_view.ui
  ./gui/edit/edit_label_methods/edit_label_methods.ui
  ./gui/edit/edit_modifications/edit_modifications.ui
  ./gui/edit/edit_settings/edit_settings.ui
  ./gui/edit/edit_tandem_preset_dialog/edit_tandem_preset_dialog.ui
  ./gui/export/export_spreadsheet_dialog/export_spreadsheet_dialog.ui
  ./gui/lists/ms_identification_run_list_view/ms_identification_run_view.ui
  ./gui/lists/ms_identification_run_list_view/engine_detail_view/engine_detail_view.ui
  ./gui/lists/peptide_list_view/peptide_view.ui
  ./gui/lists/protein_list_view/protein_view.ui
  ./gui/load_results_dialog/load_results_dialog.ui
  ./gui/masschroq_run_dialog/masschroq_run_dialog.ui
  ./gui/mcqr_param_dialog/mcqr_param_all_quanti_dialog.ui
  ./gui/main.ui
  ./gui/peptide_detail_view/peptide_detail_view.ui
  ./gui/project_view/identification_group_widget/identification_group_widget.ui
  ./gui/project_view/project_view.ui
  ./gui/protein_view/protein_detail_view.ui
  ./gui/ptm_island_list_view/ptm_island_list_view.ui
  ./gui/ptm_peptide_list_view/ptm_peptide_list_view.ui
  ./gui/run/mcqr/mcqr_run_view_ng.ui
  ./gui/run/mcqr_spectral_count/mcqr_sc_run_view.ui
  ./gui/run/msstats/msstats_run_view.ui
  ./gui/tandem_run_dialog/tandem_run_dialog.ui
  ./gui/waiting_message_dialog/waiting_message_dialog.ui
  ./gui/widgets/automatic_filter_widget/automatic_filter_widget.ui
  ./gui/widgets/engines_view/xtandem_view_widget.ui
  ./gui/widgets/engines_view/no_engine_view_widget.ui
  ./gui/widgets/qcp_massdelta/qcpmassdelta.ui
  ./gui/widgets/contaminant_widget/contaminant_widget.ui
  ./gui/widgets/decoy_widget/decoy_widget.ui
  ./gui/widgets/widget_rprocess/widget_r_process.ui
  ./gui/xic_view/xic_box/xic_box.ui
  ./gui/xic_view/xic_widgets/zivy_widget.ui
  ./gui/xic_view/xic_widgets/xic_export_param.ui
  ./gui/xic_view/xic_window.ui
  )


# this will run uic on .ui files:
qt6_wrap_ui( GUI_UI_HDRS ${GUI_UIS} )


#message("i2MassChroQ_SRCS:  ${i2MassChroQ_SRCS}")




if(UNIX AND NOT APPLE)

  add_executable(${TARGET} main.cpp ${CPP_FILES} ${i2MassChroQ_SRCS} ${GUI_UI_HDRS} ${i2MassChroQ_RCC_SRCS})
  add_executable(mzxmlconverter mzxmlconverter.cpp)


elseif(APPLE)

  add_executable(${TARGET} main.cpp ${CPP_FILES} ${i2MassChroQ_SRCS} ${GUI_UI_HDRS} ${i2MassChroQ_RCC_SRCS}
    ${PLATFORM_SPECIFIC_SOURCES}
    ../src/resources/i2MassChroQ_icon.icns
    )

  set_target_properties(${TARGET} PROPERTIES 
    MACOSX_BUNDLE_ICON_FILE i2MassChroQ_icon.icns)

elseif(MXE)

  add_executable(i2masschroq main.cpp ${CPP_FILES} ${i2MassChroQ_SRCS} ${GUI_UI_HDRS} ${i2MassChroQ_RCC_SRCS})
  add_executable(mzxmlconverter mzxmlconverter.cpp)

  target_compile_options(${TARGET} PRIVATE -mwindows)
  set_target_properties(${TARGET} PROPERTIES LINK_FLAGS "-Wl,--subsystem,windows ${CMAKE_EXE_LINKER_FLAGS}")
endif()



target_link_libraries(mzxmlconverter 
    Qt6::Core
    Qt6::Xml
    Qt6::Sql
    PappsoMSpp::Core
    )

target_include_directories(${TARGET} PUBLIC
  )

target_link_libraries(${TARGET}
  OdsStream::Core
  PappsoMSpp::Core
  PappsoMSpp::Widget
  Cutelee::Templates
  Qt6::Core
  Qt6::Gui
  Qt6::Xml
  Qt6::Svg
  Qt6::PrintSupport
  Qt6::Network
  Qt6::Sql
  Qt6::Concurrent
  ZLIB::ZLIB
  QCustomPlotQt6::QCustomPlotQt6
  QuaZip::QuaZip
  )




install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${TARGET} DESTINATION bin)
install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/mzxmlconverter DESTINATION bin)
#configure_file(${CMAKE_SOURCE_DIR}/templates/share/applications/pt-peptideviewer.desktop.cmake ${CMAKE_BINARY_DIR}/templates/share/applications/pt-peptideviewer.desktop)

