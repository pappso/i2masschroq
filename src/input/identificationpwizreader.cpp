/**
 * \file input/identificationpwizreader.cpp
 * \date 17/6/2017
 * \author Olivier Langella
 * \brief read identification files using pwiz library
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "identificationpwizreader.h"

#include <QDebug>
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QString>
#include <QRegularExpression>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionoutofrange.h>

#include <pwiz/data/common/ParamTypes.hpp>
#include <pwiz/data/identdata/IdentData.hpp>
#include <pwiz/data/msdata/MSData.hpp>
#include <pwiz/data/identdata/IdentDataFile.hpp>
#include "../core/peptidematch.h"
#include <locale>
#include "../core/proteinxtp.h"
#include "../core/peptidextp.h"
#include "../utils/utils.h"


pwiz::identdata::IdentDataFile *getPwizIdentDataFile(const QString &filename);


pwiz::identdata::IdentDataFile *
getPwizIdentDataFile(const QString &filename)
{
  qDebug() << "getPwizIdentDataFile opening file " << filename;
  std::string env;
  env              = setlocale(LC_ALL, "");
  struct lconv *lc = localeconv();
  qDebug() << " env=" << env.c_str() << " lc->decimal_point "
           << lc->decimal_point;
  setlocale(LC_ALL, "C");
  // lc = localeconv ();
  // qDebug() << " env=" << localeconv () << " lc->decimal_point " <<
  // lc->decimal_point;
  pwiz::identdata::IdentDataFile *dataFile;
  try
    {
      QByteArray byte_array = filename.toLocal8Bit();
      std::string res       = "";
      for(char c : byte_array)
        {
          res += c;
        }
      dataFile = new pwiz::identdata::IdentDataFile(res);
    }
  catch(std::exception &error)
    {
      qDebug() << "getPwizIdentDataFile std error ";
      throw pappso::PappsoException(
        QObject::tr("Error reading file (%1) using proteowizard library : %2")
          .arg(filename)
          .arg(error.what()));
    }
  setlocale(LC_ALL, env.c_str());

  return dataFile;
}

IdentificationPwizReader::IdentificationPwizReader(const QFileInfo &filename)
  : _identfile(filename)
{

  _pwiz_ident_data_file = getPwizIdentDataFile(filename.absoluteFilePath());
}

IdentificationPwizReader::~IdentificationPwizReader()
{
  delete _pwiz_ident_data_file;
}

const QString
IdentificationPwizReader::getMsrunName() const
{
  qDebug() << "IdentificationPwizReader::getMsrunName begin";
  // qDebug() << "IdentificationPwizReader::getMsrunQFileInfo begin" <<
  // _pwiz_ident_data_file->analysisSampleCollection.size();
  if(_pwiz_ident_data_file->analysisSampleCollection.samples.size() > 1)
    {
      throw pappso::PappsoException(
        QObject::tr("Unable to handle more than one (%1) MS run per "
                    "identification file %2")
          .arg(_pwiz_ident_data_file->analysisSampleCollection.samples.size())
          .arg(_identfile.absoluteFilePath()));
    }
  if(_pwiz_ident_data_file->analysisSampleCollection.samples.size() == 0)
    {
      return _identfile.baseName();
    }

  pwiz::identdata::Sample *pwiz_msrun =
    _pwiz_ident_data_file->analysisSampleCollection.samples[0].get();
  return QString::fromStdString(pwiz_msrun->name);
}


IdentificationEngine
IdentificationPwizReader::getIdentificationEngine() const
{

  /*
  <AnalysisSoftwareList xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
      <AnalysisSoftware version="X!Tandem Vengeance (2015.12.15.2)"
  name="X!Tandem" id="as1"> <SoftwareName> <cvParam accession="MS:1001476"
  cvRef="PSI-MS" name="X!Tandem"/>
          </SoftwareName>
      </AnalysisSoftware>
  </AnalysisSoftwareList>
  */
  IdentificationEngine identification_engine = IdentificationEngine::unknown;
  for(pwiz::identdata::AnalysisSoftwarePtr software_ptr :
      _pwiz_ident_data_file->analysisSoftwareList)
    {
      identification_engine = getIdentificationEngine(software_ptr);
    }
  return identification_engine;
}

IdentificationEngine
IdentificationPwizReader::getIdentificationEngine(const QString &xml_id) const
{

  /*
  <AnalysisSoftwareList xmlns="http://psidev.info/psi/pi/mzIdentML/1.1">
      <AnalysisSoftware version="X!Tandem Vengeance (2015.12.15.2)"
  name="X!Tandem" id="as1"> <SoftwareName> <cvParam accession="MS:1001476"
  cvRef="PSI-MS" name="X!Tandem"/>
          </SoftwareName>
      </AnalysisSoftware>
  </AnalysisSoftwareList>
  */
  IdentificationEngine identification_engine = IdentificationEngine::unknown;
  for(pwiz::identdata::AnalysisSoftwarePtr software_ptr :
      _pwiz_ident_data_file->analysisSoftwareList)
    {
      if(software_ptr.get()->id == xml_id.toStdString())
        {
          identification_engine = getIdentificationEngine(software_ptr);
        }
    }

  /*
    throw pappso::PappsoException(
      QObject::tr("Analysis software (%1 %2) not known to i2MassChroQ")
        .arg(analysis_software_p->id.c_str())
        .arg(analysis_software_p->name.c_str()));
        */

  return identification_engine;
}


IdentificationEngine
IdentificationPwizReader::getIdentificationEngine(
  const pwiz::identdata::AnalysisSoftwarePtr analysis_software_p) const
{

  IdentificationEngine identification_engine = IdentificationEngine::unknown;
  // qDebug() << "IdentificationPwizReader::getIdentificationEngine " <<
  // QString::fromStdString(software_ptr.get()->cvParam(param).name);
  if(analysis_software_p.get()->softwareName.hasCVParam(pwiz::cv::MS_X_Tandem))
    {
      identification_engine = IdentificationEngine::XTandem;
    }
  else if(analysis_software_p.get()->softwareName.hasCVParam(
            pwiz::cv::MS_Mascot_DAT_format))
    {
      identification_engine = IdentificationEngine::mascot;
    }
  else if(analysis_software_p.get()->softwareName.hasCVParam(
            pwiz::cv::MS_MS_GF_))
    {
      identification_engine = IdentificationEngine::MSGFplus;
    }
  else
    {
      return IdentificationEngine::unknown;
    }

  return identification_engine;
}

IdentificationEngine
IdentificationPwizReader::
  getIdentificationEngineBySpectrumIdentificationListPtr(
    pwiz::identdata::SpectrumIdentificationListPtr spectrum_ident_list_ptr)
    const
{
  for(pwiz::identdata::SpectrumIdentificationPtr spectrum_ptr :
      _pwiz_ident_data_file->analysisCollection.spectrumIdentification)
    {
      if(spectrum_ptr.get()->spectrumIdentificationListPtr.get() ==
         spectrum_ident_list_ptr.get())
        {
          return getIdentificationEngine(
            spectrum_ptr.get()
              ->spectrumIdentificationProtocolPtr.get()
              ->analysisSoftwarePtr);
        }
    }

  return IdentificationEngine::unknown;
}

QVariant
getQVariantDoubleParam(pwiz::data::ParamContainer *item, pwiz::cv::CVID param)
{
  if(item->hasCVParam(param))
    {
      return QVariant(
        QString::fromStdString(item->cvParam(param).value).toDouble());
    }
  else
    {
      return QVariant();
    }
}


void
IdentificationPwizReader::read(
  IdentificationDataSource *p_identification_data_source,
  Project *p_project,
  IdentificationGroup *p_identification_group)
{
  qDebug() << "IdentificationPwizReader::read begin";

  _p_identification_group       = p_identification_group;
  _p_identification_data_source = p_identification_data_source;
  _p_project                    = p_project;
  _sp_msrun                     = p_identification_data_source->getMsRunSp();

  try
    {
      ProteinStore &protein_store = _p_project->getProteinStore();
      PeptideStore &peptide_store = _p_project->getPeptideStore();
      std::map<QString, ProteinMatch *> map_id2protein;
      std::map<QString, PeptideXtpSp> map_id2peptide;

      /* MISSING
       *
       <Inputs>
    <SearchDatabase numDatabaseSequences="136828"
  location="/home/thierry/test/MS-GF+/Genome_Z_mays_v5a_conta.fasta"
  id="SearchDB_1"> <FileFormat> <cvParam cvRef="PSI-MS" accession="MS:1001348"
  name="FASTA format"/>
      </FileFormat>
      <DatabaseName>
        <userParam name="Genome_Z_mays_v5a_conta.fasta"/>
      </DatabaseName>
    </SearchDatabase>
    <SpectraData
  location="/home/thierry/test/MS-GF+/20170308_test_dig_Liquide_01.mzXML"
  id="SID_1" name="20170308_test_dig_Liquide_01.mzXML"> <FileFormat> <cvParam
  cvRef="PSI-MS" accession="MS:1000566" name="ISB mzXML file"/>
      </FileFormat>
      <SpectrumIDFormat>
        <cvParam cvRef="PSI-MS" accession="MS:1000776" name="scan number only
  nativeID format"/>
      </SpectrumIDFormat>
    </SpectraData>
  </Inputs>
       *
       */
      for(pwiz::identdata::DBSequencePtr seq_ptr :
          _pwiz_ident_data_file->sequenceCollection.dbSequences)
        {
          pwiz::identdata::DBSequence *sequence_pwiz = seq_ptr.get();

          ProteinXtp protein;
          protein.setCompleteDescription(
            QString::fromStdString(sequence_pwiz->accession));

          if(sequence_pwiz->hasCVParam(pwiz::cv::MS_protein_description))
            {
              protein.setDescription(
                sequence_pwiz->cvParam(pwiz::cv::MS_protein_description)
                  .value.c_str());
            }
          protein.setSequence(QString::fromStdString(sequence_pwiz->seq));
          ProteinXtpSp protein_sp(protein.makeProteinXtpSp());
          protein_sp = protein_store.getInstance(protein_sp);


          ProteinMatch *protein_match_p =
            _p_identification_group->getProteinMatchInstance(
              protein_sp.get()->getAccession());
          protein_match_p->setProteinXtpSp(protein_sp);
          protein_match_p->setChecked(true);


          map_id2protein.insert(std::pair<QString, ProteinMatch *>(
            QString::fromStdString(sequence_pwiz->id), protein_match_p));
        }

      for(pwiz::identdata::PeptidePtr seq_ptr :
          _pwiz_ident_data_file->sequenceCollection.peptides)
        {
          pwiz::identdata::Peptide *peptide_pwiz = seq_ptr.get();
          PeptideXtp peptide(
            QString::fromStdString(peptide_pwiz->peptideSequence));


          //<Modification monoisotopicMassDelta="42.01056468" location="1">
          //    <cvParam accession="MOD:00394" cvRef="PSI-MOD" name="acetylated
          //    residue"/>
          //<cvParam cvRef="UNIMOD" accession="UNIMOD:28"
          // name="Gln-&gt;pyro-Glu"/>
          //</Modification>
          for(pwiz::identdata::ModificationPtr mod_ptr :
              peptide_pwiz->modification)
            {
              unsigned int location = mod_ptr.get()->location;
              pappso::pappso_double mass_delta =
                mod_ptr.get()->monoisotopicMassDelta;

              if(location > 0)
                {
                  location = location - 1;
                }

              pappso::AaModificationP modification_ptr = nullptr;

              // std::vector<CVParam> cvParams;
              for(const pwiz::data::CVParam &param : mod_ptr.get()->cvParams)
                {
                  qDebug() << "IdentificationPwizReader::read param "
                           << QString::fromStdString(param.name());
                  //<cvParam cvRef="UNIMOD" accession="UNIMOD:28"
                  // name="Gln-&gt;pyro-Glu"/>
                  if(pwiz::cv::cvTermInfo(param.cvid).prefix() == "UNIMOD")
                    {
                      modification_ptr =
                        Utils::translateAaModificationFromUnimod(
                          QString(pwiz::cv::cvTermInfo(param.cvid).id.c_str()));
                      /*
                    if(modification_ptr == nullptr)
                      {
                        throw pappso::PappsoException(
                          QObject::tr("UNIMOD %1 not found ")
                            .arg(pwiz::cv::cvTermInfo(param.cvid).id.c_str()));
                      }*/
                    }
                  /*
                  if (pwiz::cv::cvTermInfo(param.cvid).prefix() == "PSI-MOD") {
                  }
                  else {
                      throw pappso::PappsoException(QObject::tr("Error reading
                  file (%1) using proteowizard library : cvParam %2 not
                  handled").arg(_identfile.absoluteFilePath()).arg(QString::fromStdString(
                  pwiz::cv::cvTermInfo(param.cvid).id)));
                  }
                  */
                }
              if(modification_ptr == nullptr)
                {
                  modification_ptr =
                    Utils::guessAaModificationPbyMonoisotopicMassDelta(
                      mass_delta);
                }
              try
                {
                  peptide.addAaModification(modification_ptr, location);
                }
              catch(pappso::PappsoException &error)
                {
                  throw pappso::PappsoException(
                    QObject::tr("ERROR adding modification mass delta %1 to "
                                "peptide %2 at "
                                "position %3 :\n %4")
                      .arg(mass_delta)
                      .arg(peptide.toAbsoluteString())
                      .arg(location)
                      .arg(error.qwhat()));
                }

              for(const pwiz::data::UserParam &param :
                  mod_ptr.get()->userParams)
                {
                  qDebug() << "IdentificationPwizReader::read UserParams "
                           << QString::fromStdString(param.name);
                }
            }

          PeptideXtpSp peptide_sp(peptide.makePeptideXtpSp());
          peptide_sp = peptide_store.getInstance(peptide_sp);
          map_id2peptide.insert(std::pair<QString, PeptideXtpSp>(
            QString::fromStdString(peptide_pwiz->id), peptide_sp));
        }
      for(pwiz::identdata::PeptideEvidencePtr seq_ptr :
          _pwiz_ident_data_file->sequenceCollection.peptideEvidence)
        {
        }

      // std::vector<SpectrumIdentificationListPtr> spectrumIdentificationList
      // std::vector<SpectrumIdentificationResultPtr>
      // spectrumIdentificationResult
      for(pwiz::identdata::SpectrumIdentificationListPtr
            spectrum_ident_list_ptr :
          _pwiz_ident_data_file->dataCollection.analysisData
            .spectrumIdentificationList)
        {
          IdentificationEngine current_identification_engine =
            getIdentificationEngineBySpectrumIdentificationListPtr(
              spectrum_ident_list_ptr);
          for(pwiz::identdata::SpectrumIdentificationResultPtr spectrum_ptr :
              spectrum_ident_list_ptr.get()->spectrumIdentificationResult)
            {
              unsigned int scan = 0;

              if(spectrum_ptr.get()->hasCVParam(
                   pwiz::cv::MS_scan_number_s__OBSOLETE))
                {
                  scan =
                    std::stoi(spectrum_ptr.get()
                                ->cvParam(pwiz::cv::MS_scan_number_s__OBSOLETE)
                                .value);
                }
              else
                {
                  // spectrumID="20170616_test_digestion_liquide_2_2_1_1H4.mzXML
                  // scan 2271 (charge 2)"
                  QRegularExpression re("^.* scan (\\d+) .*$");
                  QRegularExpressionMatch match =
                    re.match(spectrum_ptr.get()->spectrumID.c_str());
                  if(match.hasMatch())
                    {
                      // this is a bad hack to circumvent buggy spectrum id...
                      scan = match.captured(1).toULong();
                    }
                  else
                    {
                      // last try : regular proteowizard spectrum id
                      try
                        {
                          scan =
                            QString(pwiz::msdata::id::value(
                                      spectrum_ptr.get()->spectrumID, "scan")
                                      .c_str())
                              .toULong();
                        }
                      catch(std::runtime_error &pwiz_error)
                        {
                          throw pappso::PappsoException(
                            QObject::tr("Pwiz runtime error :\n%1\n parsing "
                                        "spectrum ID :\n%2")
                              .arg(pwiz_error.what())
                              .arg(spectrum_ptr.get()->spectrumID.c_str()));
                        }
                    }
                  /* QString(pwiz::msdata::id::translateNativeIDToScanNumber(
                             pwiz::cv::CVID::MS_mzIdentML_format,
                             spectrum_ptr.get()->spectrumID)
                             .c_str())
                     .toULong();*/
                  if(scan == 0)
                    {
                      throw pappso::PappsoException(
                        QObject::tr(
                          "Failure parsing spectrumID %1 to find scan number")
                          .arg(spectrum_ptr.get()->spectrumID.c_str()));
                    }
                }
              //<cvParam accession="MS:1001115" cvRef="PSI-MS" value="4925"
              // name="scan number(s)"/>

              QVariant rt = getQVariantDoubleParam(spectrum_ptr.get(),
                                                   pwiz::cv::MS_retention_time);
              if(rt.isNull())
                {
                  // throw pappso::PappsoException(QObject::tr("Scan number not
                  // found"));
                  //<cvParam cvRef="PSI-MS" accession="MS:1000016" name="scan
                  // start time" value="4075.63" unitAccession="UO:0000010"
                  // unitName="second" unitCvRef="UO"/>
                  rt = getQVariantDoubleParam(spectrum_ptr.get(),
                                              pwiz::cv::MS_scan_start_time);
                }


              //<SpectrumIdentificationItem passThreshold="true" rank="1"
              // experimentalMassToCharge="388.2189331" id="sii2"
              // chargeState="2" calculatedMassToCharge="388.2161235">

              //      <PeptideEvidenceRef
              //      peptideEvidence_ref="dbseq821_pep947_414"/>
              //    <PeptideEvidenceRef
              //    peptideEvidence_ref="dbseq821_pep947_414"/>
              //  <cvParam accession="MS:1001330" cvRef="PSI-MS"
              //  value="0.000452757388" name="X!Tandem:expect"/>
              //<cvParam accession="MS:1001331" cvRef="PSI-MS" value="24.275219"
              // name="X!Tandem:hyperscore"/>
              //    </SpectrumIdentificationItem>
              for(pwiz::identdata::SpectrumIdentificationItemPtr spid_item :
                  spectrum_ptr.get()->spectrumIdentificationItem)
                {
                  PeptideEvidence *p_peptide_evidence =
                    new PeptideEvidence(_sp_msrun.get(), scan);
                  p_peptide_evidence->setIdentificationEngine(
                    current_identification_engine);
                  p_peptide_evidence->setRetentionTime(rt.toDouble());
                  p_peptide_evidence->setCharge(spid_item->chargeState);
                  pappso::pappso_double exp_mass =
                    spid_item->experimentalMassToCharge *
                      ((double)spid_item->chargeState) -
                    (((double)spid_item->chargeState) * pappso::MHPLUS);
                  p_peptide_evidence->setExperimentalMass(exp_mass);
                  p_peptide_evidence->setIdentificationDataSource(
                    p_identification_data_source);

                  p_peptide_evidence->setChecked(true);
                  QVariant evalue = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_X_Tandem_expect);
                  if(!evalue.isNull())
                    {
                      p_peptide_evidence->setEvalue(evalue.toDouble());
                    }
                  QVariant hyperscore = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_X_Tandem_hyperscore);
                  if(!hyperscore.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::tandem_hyperscore, hyperscore);
                    }


                  //<cvParam cvRef="PSI-MS" accession="MS:1002049"
                  // name="MS-GF:RawScore" value="164"/>
                  QVariant MSGFplus_rawscore = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_MS_GF_RawScore);
                  if(!MSGFplus_rawscore.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::msgfplus_raw, MSGFplus_rawscore);
                    }
                  //<cvParam cvRef = "PSI-MS" accession = "MS:1002050" name
                  //="MS-GF:DeNovoScore" value        = "170" />

                  QVariant MSGFplus_denovoscore = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_MS_GF_DeNovoScore);
                  if(!MSGFplus_denovoscore.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::msgfplus_denovo,
                        MSGFplus_denovoscore);
                    }

                  //<cvParam cvRef = "PSI-MS" accession = "MS:1002052" name
                  //="MS-GF:SpecEValue" value         = "5.4876387E-19"/>

                  QVariant MSGFplus_specevalue = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_MS_GF_SpecEValue);
                  if(!MSGFplus_specevalue.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::msgfplus_SpecEValue,
                        MSGFplus_specevalue);
                    }
                  //<cvParam cvRef = "PSI-MS" accession = "MS:1002053" name =
                  //"MS-GF:EValue" value             = "1.0822937E-11" />

                  QVariant MSGFplus_evalue = getQVariantDoubleParam(
                    spid_item.get(), pwiz::cv::MS_MS_GF_EValue);
                  if(!MSGFplus_evalue.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::msgfplus_EValue, MSGFplus_evalue);
                      p_peptide_evidence->setEvalue(MSGFplus_evalue.toDouble());
                    }

                  //<userParam name="IsotopeError" value="0"/>
                  QVariant isotope_error =
                    QVariant(QString::fromStdString(
                               spid_item.get()->userParam("IsotopeError").value)
                               .toInt());
                  if(!isotope_error.isNull())
                    {
                      p_peptide_evidence->setParam(
                        PeptideEvidenceParam::msgfplus_isotope_error,
                        isotope_error);
                    }

                  for(pwiz::identdata::PeptideEvidencePtr peptide_evidence_sp :
                      spid_item.get()->peptideEvidencePtr)
                    {
                      PeptideMatch peptide_match;
                      peptide_match.setStart(peptide_evidence_sp.get()->start -
                                             1);


                      auto it_pep = map_id2peptide.find(QString::fromStdString(
                        peptide_evidence_sp.get()->peptidePtr.get()->id));
                      if(it_pep == map_id2peptide.end())
                        {
                          throw pappso::ExceptionOutOfRange(
                            QObject::tr("key %1 not found in map_id2peptide, "
                                        "reading SpectrumIdentificationItem %2")
                              .arg(QString::fromStdString(
                                peptide_evidence_sp.get()
                                  ->peptidePtr.get()
                                  ->id))
                              .arg(
                                QString::fromStdString(spid_item.get()->id)));
                        }


                      p_peptide_evidence->setPeptideXtpSp(it_pep->second);
                      PeptideEvidenceSp peptide_evidence_sp_pappso =
                        p_identification_data_source->getPeptideEvidenceStore()
                          .getInstance(p_peptide_evidence);

                      peptide_match.setPeptideEvidenceSp(
                        peptide_evidence_sp_pappso);

                      auto it = map_id2protein.find(QString::fromStdString(
                        peptide_evidence_sp.get()->dbSequencePtr.get()->id));
                      if(it == map_id2protein.end())
                        {
                          throw pappso::ExceptionOutOfRange(
                            QObject::tr("key %1 not found in map_id2protein")
                              .arg(QString::fromStdString(
                                peptide_evidence_sp.get()
                                  ->dbSequencePtr.get()
                                  ->id)));
                        }
                      ProteinMatch *protein_match_p = it->second;

                      protein_match_p->addPeptideMatch(peptide_match);
                    }

                  delete p_peptide_evidence;
                }
            }
        }
    }
  catch(pappso::PappsoException &error)
    {
      qDebug() << "IdentificationPwizReader::read error ";
      throw pappso::PappsoException(
        QObject::tr("Error reading file (%1) using proteowizard library : %2")
          .arg(_identfile.absoluteFilePath())
          .arg(error.qwhat()));
    }

  catch(std::exception &error)
    {
      qDebug() << "IdentificationPwizReader::read std error ";
      throw pappso::PappsoException(
        QObject::tr("Error reading file (%1) using proteowizard library : %2")
          .arg(_identfile.absoluteFilePath())
          .arg(error.what()));
    }
  qDebug() << "IdentificationPwizReader::read end";
}
