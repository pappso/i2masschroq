/**
 * \file input/sage/sagefilereader.h
 * \date 11/10/2024
 * \author Olivier Langella
 * \brief read data files from Sage output containin multiple samples
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "../../core/project.h"
#include "../../utils/workmonitor.h"
#include <pappsomspp/fasta/fastahandlerinterface.h>
#include <QJsonDocument>
/**
 * @todo write docs
 */
class SageFileReader
{
  public:
  /**
   * Default constructor
   */
  SageFileReader(pappso::UiMonitorInterface *p_monitor,
                 Project *p_project,
                 const QFileInfo &sage_json_file);

  /**
   * Destructor
   */
  virtual ~SageFileReader();

  const QJsonDocument &getJsonDocument() const;

  void addFastaFile(FastaFileSp file) const;

  MsRunSp getMsRunSpWithFileName(const QString &msrun_filename) const;
  IdentificationSageJsonFileSp getIdentificationSageJsonFileSpWithFileName(
    const QString &msrun_filename) const;
  IdentificationGroup *
  getIdentificationGroupPtrWithFileName(const QString &msrun_filename) const;

  private:
  void readJson(Project *p_project);

  private:
  const QFileInfo m_sageJsonFile;
  QJsonDocument m_jsonData;

  IdentificationEngine m_identificationEngine;

  std::map<QString, MsRunSp> m_mapFilePath2MsRunSp;
  std::map<QString, IdentificationGroup *> m_mapFilePath2IdentificationGroupPtr;
  std::map<QString, IdentificationSageJsonFileSp>
    m_mapFilePath2IdentificationSageJsonFileSp;
  QString m_sageVersion;
};
