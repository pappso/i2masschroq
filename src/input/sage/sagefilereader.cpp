/**
 * \file input/sage/sagefilereader.cpp
 * \date 11/10/2024
 * \author Olivier Langella
 * \brief read data files from Sage output containin multiple samples
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "sagefilereader.h"
#include "sagereader.h"
#include "../../utils/identificationdatasourcestore.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/processing/project/projectparameters.h>
#include <pappsomspp/exception/exceptionnotrecognized.h>
#include <QJsonObject>

SageFileReader::SageFileReader(pappso::UiMonitorInterface *p_monitor,
                               Project *p_project,
                               const QFileInfo &sage_json_file)
  : m_sageJsonFile(sage_json_file)
{
  qDebug() << m_sageJsonFile.absoluteFilePath() << "'";
  try
    {


      QDir::setCurrent(sage_json_file.absolutePath());
      m_identificationEngine = IdentificationEngine::unknown;
      readJson(p_project);

      p_monitor->setStatus(QObject::tr("reading Sage json file %1")
                             .arg(sage_json_file.absoluteFilePath()));
      std::vector<IdentificationGroup *> identification_list =
        p_project->getIdentificationGroupList();
      if(p_project->getProjectMode() == ProjectMode::combined)
        {
          IdentificationGroup *identification_group_p = nullptr;
          if(identification_list.size() == 0)
            {
              identification_group_p = p_project->newIdentificationGroup();
            }
          else
            {
              identification_group_p = identification_list[0];
            }

          for(auto &filePair : m_mapFilePath2MsRunSp)
            {
              m_mapFilePath2IdentificationGroupPtr.insert(
                {filePair.first, identification_group_p});
            }
        }
      else
        {

          for(auto &filePair : m_mapFilePath2MsRunSp)
            {
              IdentificationGroup *identification_group_p = nullptr;
              for(IdentificationGroup *identification_p_flist :
                  identification_list)
                {
                  if(identification_p_flist->containSample(
                       filePair.second.get()->getSampleName()))
                    {
                      identification_group_p = identification_p_flist;
                      break;
                    }
                }
              if(identification_group_p == nullptr)
                {
                  identification_group_p = p_project->newIdentificationGroup();
                }

              m_mapFilePath2IdentificationGroupPtr.insert(
                {filePair.first, identification_group_p});
            }
        }

      // p_monitor->setTotalSteps(m_mapFilePath2IdentificationGroupPtr.size());
      std::size_t i = p_project->getIdentificationDataSourceStore()
                        .getIdentificationDataSourceList()
                        .size();
      for(auto &identGroupPair : m_mapFilePath2IdentificationGroupPtr)
        {
          auto sage_identification_source_sp =
            p_project->getIdentificationDataSourceStore()
              .buildIdentificationSageJsonFileSp(
                sage_json_file,
                i,
                m_jsonData,
                getMsRunSpWithFileName(identGroupPair.first));
          identGroupPair.second->addIdentificationDataSourceP(
            sage_identification_source_sp.get());
          i += 1;
          m_mapFilePath2IdentificationSageJsonFileSp.insert(
            {identGroupPair.first, sage_identification_source_sp});

          sage_identification_source_sp.get()->setIdentificationEngineVersion(
            m_sageVersion);
        }

      pappso::ProjectParameters parameters;
      if(m_mapFilePath2IdentificationSageJsonFileSp.size() == 0)
        {
          throw pappso::PappsoException(
            QObject::tr("Error no identification found"));
        }
      m_mapFilePath2IdentificationSageJsonFileSp.begin()
        ->second.get()
        ->fillProjectParameters(parameters);

      //  "decoy_tag": "rev_",
      //  "generate_decoys": true,

      qDebug() << parameters.getValue(
        pappso::ProjectParamCategory::identification,
        "sage_database_decoy_tag");

      SageReader sage_reader(p_monitor, p_project, *this);
      sage_reader.read();
      m_identificationEngine = IdentificationEngine::sage;

      if(parameters
           .getValue(pappso::ProjectParamCategory::identification,
                     "sage_database_generate_decoys")
           .toBool())
        {
          p_project->getProteinStore().setDecoySelectionType(
            DecoySelectionType::regexp);

          p_project->getProteinStore().setRegexpDecoyPattern(QString("^%1").arg(
            parameters
              .getValue(pappso::ProjectParamCategory::identification,
                        "sage_database_decoy_tag")
              .toString()));
        }
    }
  catch(pappso::ExceptionNotRecognized &err)
    {
      throw err;
    }
  catch(pappso::PappsoException &other_err)
    {
      throw pappso::PappsoException(
        QObject::tr("Error reading Sage JSON file %1:\n%2")
          .arg(m_sageJsonFile.absoluteFilePath())
          .arg(other_err.qwhat()));
    }
}

SageFileReader::~SageFileReader()
{
}


void
SageFileReader::readJson(Project *p_project)
{
  QFile mfile(m_sageJsonFile.absoluteFilePath());
  if(!mfile.open(QFile::ReadOnly))
    {
      throw pappso::ExceptionNotRecognized(
        QObject::tr("Unable to read Sage JSON file %1")
          .arg(m_sageJsonFile.absoluteFilePath()));
    }
  QByteArray iContents = mfile.readAll();

  QJsonParseError parseError;
  m_jsonData = QJsonDocument::fromJson(iContents, &parseError);
  if(parseError.error != QJsonParseError::NoError)
    {
      throw pappso::ExceptionNotRecognized(
        QObject::tr("Error reading Sage JSON file %1 at %2:%3")
          .arg(m_sageJsonFile.absoluteFilePath())
          .arg(parseError.offset)
          .arg(parseError.errorString()));
    }

  QJsonValue mzml_value = m_jsonData.object().value("mzml_paths");

  if(mzml_value.isNull())
    {
      throw pappso::ExceptionNotRecognized(
        QObject::tr("Sage JSON file %1 does not contain mzml_paths")
          .arg(m_sageJsonFile.absoluteFilePath()));
    }
  for(const QJsonValue &value :
      m_jsonData.object().value("mzml_paths").toArray())
    {
      qDebug() << value.toString();

      MsRunSp msrun_sp =
        p_project->getMsRunStore().getInstance(value.toString());
      m_mapFilePath2MsRunSp.insert(
        {QFileInfo(value.toString()).fileName(), msrun_sp});
    }


  m_sageVersion = m_jsonData.object().value("version").toString();
}

const QJsonDocument &
SageFileReader::getJsonDocument() const
{
  return m_jsonData;
}

void
SageFileReader::addFastaFile(FastaFileSp file) const
{
  if(!file.get()->getQFileInfo().exists())
    {
      throw pappso::PappsoException(
        QObject::tr("fasta file %1 not found")
          .arg(file.get()->getQFileInfo().absoluteFilePath()));
    }
  for(auto &sage_source : m_mapFilePath2IdentificationSageJsonFileSp)
    {
      sage_source.second.get()->addFastaFile(file);
    }
}

MsRunSp
SageFileReader::getMsRunSpWithFileName(const QString &msrun_filename) const
{
  auto it = m_mapFilePath2MsRunSp.find(msrun_filename);
  if(it != m_mapFilePath2MsRunSp.end())
    return it->second;


  throw pappso::PappsoException(
    QObject::tr("msrun filename %1 not found").arg(msrun_filename));
}

IdentificationSageJsonFileSp
SageFileReader::getIdentificationSageJsonFileSpWithFileName(
  const QString &msrun_filename) const
{
  auto it = m_mapFilePath2IdentificationSageJsonFileSp.find(msrun_filename);
  if(it != m_mapFilePath2IdentificationSageJsonFileSp.end())
    return it->second;


  throw pappso::PappsoException(
    QObject::tr("Error reading Sage JSON file %1 msrun filename %2 not found")
      .arg(m_sageJsonFile.absoluteFilePath())
      .arg(msrun_filename));
}

IdentificationGroup *
SageFileReader::getIdentificationGroupPtrWithFileName(
  const QString &msrun_filename) const
{
  auto it = m_mapFilePath2IdentificationGroupPtr.find(msrun_filename);
  if(it != m_mapFilePath2IdentificationGroupPtr.end())
    return it->second;


  throw pappso::PappsoException(
    QObject::tr("Error reading Sage JSON file %1 msrun filename %2 not found")
      .arg(m_sageJsonFile.absoluteFilePath())
      .arg(msrun_filename));
}
