/**
 * \file input/sage/sagereader.cpp
 * \date 21/08/2024
 * \author Olivier Langella
 * \brief read data files from Sage output
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "sagereader.h"
#include <QJsonObject>
#include <odsstream/tsvreader.h>
#include <odsstream/odsexception.h>
#include "sagetsvhandler.h"
#include <pappsomspp/utils.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include "../../utils/utils.h"


SageReader::SageReader(pappso::UiMonitorInterface *p_monitor,
                       Project *p_project,
                       const SageFileReader &sage_file_reader)
  : m_sageFileReader(sage_file_reader)
{
  mp_monitor = p_monitor;
  mp_project = p_project;
}

SageReader::~SageReader()
{
}

FastaFileSp
SageReader::getFastaFileSp() const
{
  return msp_fastaFile;
}

Project *
SageReader::getProjectPtr() const
{
  return mp_project;
}


void
SageReader::read()
{
  readTsvFile();
  // collect protein sequences


  QFile fastaFile(getFastaFileSp().get()->getAbsoluteFilePath());
  SageReader::FastaSeq seq(this);
  pappso::FastaReader reader(seq);
  reader.parse(fastaFile);
}

SageReader::FastaSeq::FastaSeq(SageReader *self)
{
  mp_self    = self;
  m_decoyTag = mp_self->getDecoyTag();
}

void
SageReader::FastaSeq::setSequence(const QString &description_in,
                                  const QString &sequence_in)
{

  QString accession = description_in.split(" ", Qt::SkipEmptyParts).at(0);
  auto it =
    mp_self->mp_project->getProteinStore().getProteinMap().find(accession);
  if(it != mp_self->mp_project->getProteinStore().getProteinMap().end())
    {
      it->second.get()->setSequence(sequence_in);
    }

  pappso::Protein rev_protein(description_in, sequence_in);
  rev_protein.reverse();

  accession = QString("%1%2").arg(m_decoyTag).arg(accession);

  it = mp_self->mp_project->getProteinStore().getProteinMap().find(accession);
  if(it != mp_self->mp_project->getProteinStore().getProteinMap().end())
    {
      it->second.get()->setSequence(rev_protein.getSequence());
    }
}


void
SageReader::readTsvFile()
{


  msp_fastaFile = mp_project->getFastaFileStore().getInstance(
    FastaFile(getFastaFilePath(m_sageFileReader.getJsonDocument())));

  m_sageFileReader.addFastaFile(msp_fastaFile);

  // getTsvFilePath(mp_identificationDataSource->getJsonDocument().object());
  QFileInfo tsv_file_info(getTsvFilePath(m_sageFileReader.getJsonDocument()));
  try
    {
      SageTsvHandler handler(mp_monitor, *this);
      TsvReader tsv_reader(handler);

      QFile tsv_file(tsv_file_info.absoluteFilePath());
      tsv_reader.parse(tsv_file);
    }
  catch(OdsException &error_ods)
    {
      throw pappso::PappsoException(QObject::tr("Error reading %1 file:\n %2")
                                      .arg(tsv_file_info.absoluteFilePath())
                                      .arg(error_ods.qwhat()));
    }
}

QString
SageReader::getTsvFilePath(const QJsonDocument &json_doc)
{
  QString path;
  QJsonObject sage_object = json_doc.object();
  QJsonValue output_path  = sage_object.value("output_paths");
  if(output_path.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("output_paths not found in Sage json document"));
    }

  if(!output_path.isArray())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("output_paths is not an array"));
    }
  for(auto element : output_path.toArray())
    {
      if(element.isString())
        {
          if(element.toString().endsWith(".tsv"))
            {
              path = element.toString();
            }
        }
    }
  return path;
}

QString
SageReader::getFastaFilePath(const QJsonDocument &json_doc)
{
  QString path;
  QJsonObject sage_object = json_doc.object();
  QJsonValue database     = sage_object.value("database");
  if(database.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("database not found in Sage json document"));
    }
  path = database.toObject().value("fasta").toString();
  if(path.isEmpty())
    {
      throw pappso::ExceptionNotFound(QObject::tr("fasta value is empty"));
    }
  return path;
}


std::vector<SageReader::SageModification>
SageReader::getStaticModificationList() const
{
  std::vector<SageReader::SageModification> list;
  QJsonObject sage_object = m_sageFileReader.getJsonDocument().object();
  QJsonValue database     = sage_object.value("database");
  if(database.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("database not found in Sage json document"));
    }

  QJsonValue static_mods = database.toObject().value("static_mods");
  if(static_mods.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("static_mods not found in Sage json document"));
    }
  for(QString residue_str : static_mods.toObject().keys())
    {
      SageModification modif;
      modif.residue = residue_str.at(0);
      modif.modification =
        pappso::Utils::guessAaModificationPbyMonoisotopicMassDelta(
          (pappso::AminoAcidChar)modif.residue.toLatin1(),
          static_mods.toObject().value(residue_str).toDouble());
      modif.strModification = QString::number(
        static_mods.toObject().value(residue_str).toDouble(), 'f', 6);
      if(modif.strModification.isEmpty())
        {
          throw pappso::PappsoException(
            QObject::tr(" modif.strModification is empty"));
        }
      if(modif.modification->getMass() < 0)
        {
          modif.strModification = QString("[%1]").arg(modif.strModification);
        }
      else
        {
          modif.strModification = QString("[+%1]").arg(modif.strModification);
        }
      list.push_back(modif);
    }
  return list;
}

std::vector<SageReader::SageModification>
SageReader::getVariableModificationList() const
{
  std::vector<SageReader::SageModification> list;
  QJsonObject sage_object = m_sageFileReader.getJsonDocument().object();
  QJsonValue database     = sage_object.value("database");
  if(database.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("database not found in Sage json document"));
    }

  QJsonValue var_mods = database.toObject().value("variable_mods");
  if(var_mods.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("static_mods not found in Sage json document"));
    }
  for(QString residue_str : var_mods.toObject().keys())
    {
      SageModification modif;
      modif.residue = residue_str.at(0);
      for(QJsonValue one_mass :
          var_mods.toObject().value(residue_str).toArray())
        {
          modif.modification =
            pappso::Utils::guessAaModificationPbyMonoisotopicMassDelta(
              (pappso::AminoAcidChar)modif.residue.toLatin1(),
              one_mass.toDouble());
          modif.strModification = QString::number(one_mass.toDouble(), 'f', 6);
          if(modif.strModification.isEmpty())
            {
              throw pappso::PappsoException(
                QObject::tr(" modif.strModification is empty"));
            }
          if(modif.modification->getMass() < 0)
            {
              modif.strModification =
                QString("[%1]").arg(modif.strModification);
            }
          else
            {
              modif.strModification =
                QString("[+%1]").arg(modif.strModification);
            }
          list.push_back(modif);
        }
    }
  return list;
}

QString
SageReader::getDecoyTag() const
{
  QString path;
  QJsonObject sage_object = m_sageFileReader.getJsonDocument().object();
  QJsonValue database     = sage_object.value("database");
  if(database.isUndefined())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("database not found in Sage json document"));
    }
  path = database.toObject().value("decoy_tag").toString();
  if(path.isEmpty())
    {
      throw pappso::ExceptionNotFound(QObject::tr("decoy_tag value is empty"));
    }
  return path;
}

const SageFileReader &
SageReader::getSageFileReader() const
{
  return m_sageFileReader;
}
