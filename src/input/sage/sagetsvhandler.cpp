/**
 * \file input/sage/sagereader.h
 * \date 21/08/2024
 * \author Olivier Langella
 * \brief read data files from Sage output
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "sagetsvhandler.h"
#include <pappsomspp/exception/exceptionnotimplemented.h>
#include <pappsomspp/exception/exceptionnotpossible.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptioninterrupted.h>
#include <pappsomspp/peptide/peptideproformaparser.h>
#include <pappsomspp/msrun/msrunreader.h>
#include "../../core/peptideevidence.h"


SageTsvHandler::SageTsvHandler(pappso::UiMonitorInterface *p_monitor,
                               const SageReader &sage_reader)
  : m_sageReader(sage_reader)
{
  mp_monitor                 = p_monitor;
  m_staticModificationList   = sage_reader.getStaticModificationList();
  m_variableModificationList = sage_reader.getVariableModificationList();
  m_decoyTag                 = sage_reader.getDecoyTag();
}

SageTsvHandler::~SageTsvHandler()
{
}

void
SageTsvHandler::endDocument()
{
}

void
SageTsvHandler::endLine()
{
  if(m_line.charge != 0)
    recordLine();
  m_lineNumber++;
}

void
SageTsvHandler::endSheet()
{
}

void
SageTsvHandler::setCell(const OdsCell &cell)
{
  if(m_lineNumber == 0)
    {
      // header
      QString tag = cell.getStringValue();
      if(tag == "psm_id")
        {
          m_columnTypeList.push_back(Columns::psm_id);
        }
      else if(tag == "peptide")
        {
          m_columnTypeList.push_back(Columns::peptide);
        }
      else if(tag == "proteins")
        {
          m_columnTypeList.push_back(Columns::proteins);
        }
      else if(tag == "num_proteins")
        {
          m_columnTypeList.push_back(Columns::num_proteins);
        }
      else if(tag == "filename")
        {
          m_columnTypeList.push_back(Columns::filename);
        }
      else if(tag == "scannr")
        {
          m_columnTypeList.push_back(Columns::scannr);
        }
      else if(tag == "rank")
        {
          m_columnTypeList.push_back(Columns::rank);
        }
      else if(tag == "label")
        {
          m_columnTypeList.push_back(Columns::label);
        }
      else if(tag == "expmass")
        {
          m_columnTypeList.push_back(Columns::expmass);
        }

      else if(tag == "calcmass")
        {
          m_columnTypeList.push_back(Columns::calcmass);
        }
      else if(tag == "charge")
        {
          m_columnTypeList.push_back(Columns::charge);
        }
      else if(tag == "peptide_len")
        {
          m_columnTypeList.push_back(Columns::peptide_len);
        }
      else if(tag == "missed_cleavages")
        {
          m_columnTypeList.push_back(Columns::missed_cleavages);
        }
      else if(tag == "semi_enzymatic")
        {
          m_columnTypeList.push_back(Columns::semi_enzymatic);
        }
      else if(tag == "isotope_error")
        {
          m_columnTypeList.push_back(Columns::isotope_error);
        }
      else if(tag == "precursor_ppm")
        {
          m_columnTypeList.push_back(Columns::precursor_ppm);
        }
      else if(tag == "fragment_ppm")
        {
          m_columnTypeList.push_back(Columns::fragment_ppm);
        }
      else if(tag == "hyperscore")
        {
          m_columnTypeList.push_back(Columns::hyperscore);
        }
      else if(tag == "delta_next")
        {
          m_columnTypeList.push_back(Columns::delta_next);
        }
      else if(tag == "delta_best")
        {
          m_columnTypeList.push_back(Columns::delta_best);
        }
      else if(tag == "rt")
        {
          m_columnTypeList.push_back(Columns::rt);
        }
      else if(tag == "aligned_rt")
        {
          m_columnTypeList.push_back(Columns::aligned_rt);
        }
      else if(tag == "predicted_rt")
        {
          m_columnTypeList.push_back(Columns::predicted_rt);
        }
      else if(tag == "delta_rt_model")
        {
          m_columnTypeList.push_back(Columns::delta_rt_model);
        }
      else if(tag == "ion_mobility")
        {
          m_columnTypeList.push_back(Columns::ion_mobility);
        }
      else if(tag == "predicted_mobility")
        {
          m_columnTypeList.push_back(Columns::predicted_mobility);
        }
      else if(tag == "delta_mobility")
        {
          m_columnTypeList.push_back(Columns::delta_mobility);
        }
      else if(tag == "matched_peaks")
        {
          m_columnTypeList.push_back(Columns::matched_peaks);
        }
      else if(tag == "longest_b")
        {
          m_columnTypeList.push_back(Columns::longest_b);
        }
      else if(tag == "longest_y")
        {
          m_columnTypeList.push_back(Columns::longest_y);
        }
      else if(tag == "longest_y_pct")
        {
          m_columnTypeList.push_back(Columns::longest_y_pct);
        }
      else if(tag == "matched_intensity_pct")
        {
          m_columnTypeList.push_back(Columns::matched_intensity_pct);
        }
      else if(tag == "scored_candidates")
        {
          m_columnTypeList.push_back(Columns::scored_candidates);
        }
      else if(tag == "poisson")
        {
          m_columnTypeList.push_back(Columns::poisson);
        }
      else if(tag == "sage_discriminant_score")
        {
          m_columnTypeList.push_back(Columns::sage_discriminant_score);
        }
      else if(tag == "posterior_error")
        {
          m_columnTypeList.push_back(Columns::posterior_error);
        }
      else if(tag == "spectrum_q")
        {
          m_columnTypeList.push_back(Columns::spectrum_q);
        }
      else if(tag == "peptide_q")
        {
          m_columnTypeList.push_back(Columns::peptide_q);
        }
      else if(tag == "protein_q")
        {
          m_columnTypeList.push_back(Columns::protein_q);
        }
      else if(tag == "ms2_intensity")
        {
          m_columnTypeList.push_back(Columns::ms2_intensity);
        }
      else
        {
          throw pappso::ExceptionNotPossible(
            QObject::tr("column \"%1\" not defined").arg(tag));
        }
    }
  else
    {
      if(m_columnNumber >= m_columnTypeList.size())
        {
          throw pappso::ExceptionOutOfRange(
            QObject::tr("the value %1 is out of range")
              .arg(cell.getStringValue()));
        }
      Columns column_type = m_columnTypeList[m_columnNumber];
      switch(column_type)
        {
          case Columns::psm_id:
            break;
          case Columns::peptide:
            parsePeptide(cell.toString());
            break;
          case Columns::proteins:
            parseProteins(cell.toString());
            break;
          case Columns::num_proteins:
            if((std::size_t)cell.getDoubleValue() != m_proteinXtpSpList.size())
              {
                throw pappso::PappsoException(
                  QObject::tr("column \"num_proteins\"!=%1")
                    .arg(m_proteinXtpSpList.size()));
              }
            break;
          case Columns::filename:
            parseMsRunFilename(cell.toString());
            break;
          case Columns::scannr:
            parseSpectrumStringId(cell.toString());
            break;
          case Columns::rank:
            m_line.rank = cell.getDoubleValue();
            break;
          case Columns::label:
            m_line.label = cell.getDoubleValue();
            break;
          case Columns::expmass:
            m_line.expmass = cell.getDoubleValue();
            break;
          case Columns::calcmass:
            m_line.calcmass = cell.getDoubleValue();
            break;
          case Columns::charge:
            m_line.charge = cell.getDoubleValue();
            break;
          case Columns::peptide_len:
            m_line.peptide_len = cell.getDoubleValue();
            break;
          case Columns::missed_cleavages:
            m_line.missed_cleavages = cell.getDoubleValue();
            break;
          case Columns::semi_enzymatic:
            m_line.semi_enzymatic = cell.getDoubleValue();
            break;
          case Columns::isotope_error:
            m_line.isotope_error = cell.getDoubleValue();
            break;

          case Columns::precursor_ppm:
            m_line.precursor_ppm = cell.getDoubleValue();
            break;
          case Columns::fragment_ppm:
            m_line.fragment_ppm = cell.getDoubleValue();
            break;
          case Columns::hyperscore:
            m_line.hyperscore = cell.getDoubleValue();
            break;
          case Columns::delta_next:
            m_line.delta_next = cell.getDoubleValue();
            break;
          case Columns::delta_best:
            m_line.delta_best = cell.getDoubleValue();
            break;
          case Columns::rt:
            m_line.rt = cell.getDoubleValue() *
                        60; // to convert retention time in seconds
            break;
          case Columns::aligned_rt:
            m_line.aligned_rt = cell.getDoubleValue();
            break;
          case Columns::predicted_rt:
            m_line.predicted_rt = cell.getDoubleValue();
            break;
          case Columns::delta_rt_model:
            m_line.delta_rt_model = cell.getDoubleValue();
            break;
          case Columns::ion_mobility:
            m_line.ion_mobility = cell.getDoubleValue();
            break;
          case Columns::predicted_mobility:
            m_line.predicted_mobility = cell.getDoubleValue();
            break;
          case Columns::delta_mobility:
            m_line.delta_mobility = cell.getDoubleValue();
            break;
          case Columns::matched_peaks:
            m_line.matched_peaks = cell.getDoubleValue();
            break;
          case Columns::longest_b:
            m_line.longest_b = cell.getDoubleValue();
            break;
          case Columns::longest_y:
            m_line.longest_y = cell.getDoubleValue();
            break;
          case Columns::longest_y_pct:
            m_line.longest_y_pct = cell.getDoubleValue();
            break;
          case Columns::matched_intensity_pct:
            m_line.matched_intensity_pct = cell.getDoubleValue();
            break;
          case Columns::scored_candidates:
            m_line.scored_candidates = cell.getDoubleValue();
            break;
          case Columns::poisson:
            m_line.poisson = cell.getDoubleValue();
            break;
          case Columns::sage_discriminant_score:
            m_line.sage_discriminant_score = cell.getDoubleValue();
            break;
          case Columns::posterior_error:
            m_line.posterior_error = cell.getDoubleValue();
            break;
          case Columns::spectrum_q:
            m_line.spectrum_q = cell.getDoubleValue();
            break;
          case Columns::peptide_q:
            m_line.peptide_q = cell.getDoubleValue();
            break;
          case Columns::protein_q:
            m_line.protein_q = cell.getDoubleValue();
            break;
          case Columns::ms2_intensity:
            m_line.ms2_intensity = cell.getDoubleValue();
            break;
          default:
            qDebug() << "m_line.calcmass=" << m_line.calcmass;
            throw pappso::ExceptionNotImplemented(
              QObject::tr("column type %1 not implemented")
                .arg((std::uint8_t)column_type));
            break;
        }

      /*

    2333	TMISDSDYTEFENFTK
    GRMZM2G018197_P01;GRMZM2G068952_P01;GRMZM5G822976_P01	3
    20120906_balliau_extract_1_A01_urnb-1.mzML	controllerType=0
    controllerNumber=1 scan=12542	1	1	1926.8225	1926.8193	2	16	0	0
    0.0	1.6471838	1.9796097	54.06803492297634	28.049970383419556	0.0	38.192993
    0.76385987	0.7671368	0.0032769442	0.0	0.0	0.0	16	2	14	0.875	32.54396 380
    -13.375352220427656	1.1570586	-34.13482	0.00016041065	0.00022231363
    0.00040124074	1271951.1
    */
    }
  m_columnNumber++;
}

void
SageTsvHandler::startLine()
{
  m_columnNumber                   = 0;
  msp_msrun                        = nullptr;
  msp_peptide                      = nullptr;
  msp_identificationSageJsonFileSp = nullptr;
  mp_identificationGroup           = nullptr;
  m_proteinXtpSpList.clear();
  m_line = Line();
}

void
SageTsvHandler::startSheet(const QString &sheet_name)
{
  m_columnNumber = 0;
  m_lineNumber   = 0;
  mp_monitor->setStatus(QObject::tr("reading Sage TSV file"));

  if(mp_monitor->shouldIstop())
    {
      throw pappso::ExceptionInterrupted(
        QObject::tr("Sage TSV data reading process interrupted"));
    }
}

void
SageTsvHandler::parsePeptide(const QString &peptide_str)
{
  qDebug();
  QString peptide_str_verif = peptide_str;
  // fixed modifications :
  for(SageReader::SageModification modif : m_staticModificationList)
    {
      qDebug() << modif.strModification;
      qDebug() << modif.modification->getAccession();
      peptide_str_verif = peptide_str_verif.replace(
        modif.strModification,
        QString("[%1]").arg(modif.modification->getAccession()));
    }
  // variable modifications :
  for(SageReader::SageModification modif : m_variableModificationList)
    {
      qDebug() << modif.strModification;
      qDebug() << modif.modification->getAccession();
      peptide_str_verif = peptide_str_verif.replace(
        modif.strModification,
        QString("[%1]").arg(modif.modification->getAccession()));
    }

  qDebug() << peptide_str_verif;
  // LPMFGC[+57.0216]NDATQVYK
  pappso::PeptideSp parse_pep =
    pappso::PeptideProFormaParser::parseString(peptide_str_verif);

  msp_peptide = PeptideXtp(*(parse_pep.get())).makePeptideXtpSp();
  qDebug();
  // variable modifications :
  /*
  setVariableModifications(peptide_sp,
                           peptide_line.peptide_string_list.at(6));
*/
  qDebug() << msp_peptide.get()->toProForma();


  msp_peptide =
    m_sageReader.getProjectPtr()->getPeptideStore().getInstance(msp_peptide);
}


void
SageTsvHandler::parseProteins(const QString &proteins_str)
{
  QStringList protein_list = proteins_str.split(";");
  m_proteinXtpSpList.clear();
  for(QString accession : protein_list)
    {
      ProteinXtpSp protein_sp = ProteinXtp().makeProteinXtpSp();
      protein_sp.get()->setAccession(accession);
      protein_sp.get()->setFastaFileP(m_sageReader.getFastaFileSp().get());
      protein_sp =
        m_sageReader.getProjectPtr()->getProteinStore().getInstance(protein_sp);
      if(accession.startsWith(m_decoyTag))
        {
          protein_sp.get()->setIsDecoy(true);
        }
      m_proteinXtpSpList.push_back(protein_sp);
    }
}

void
SageTsvHandler::parseSpectrumStringId(const QString &spectrum_string_id)
{
  qDebug() << spectrum_string_id;
  // controllerType=0 controllerNumber=1 scan=176056

  bool is_ok            = false;
  QStringList scan_list = spectrum_string_id.split("scan=");
  if(scan_list.size() == 2)
    {
      // we bet that there is a scan number, easy to parse
      m_spectrumIndex = scan_list.at(1).toULongLong(&is_ok);
      if(m_spectrumIndex > 0)
        m_spectrumIndex--;
    }
  if(is_ok == false)
    {
      if(msp_previousMsrun != msp_msrun)
        {
          mp_monitor->setStatus(QObject::tr("Reading mz data file %1")
                                  .arg(msp_msrun.get()->getFileName()));
          msp_previousMsrun = msp_msrun;
        }
      pappso::MsRunReader *msrunreader_p =
        msp_msrun.get()->getMsRunReaderSPtr().get();
      if(msrunreader_p->getMsRunId()->getMsDataFormat() ==
         pappso::MsDataFormat::brukerTims)
        {
          m_spectrumIndex = spectrum_string_id.toInt() * 2 - 1;
        }
      else
        {
          m_spectrumIndex =
            msrunreader_p->spectrumStringIdentifier2SpectrumIndex(
              spectrum_string_id);
        }
    }
  qDebug() << spectrum_string_id;
}

void
SageTsvHandler::parseMsRunFilename(const QString &msrun_filename)
{
  msp_msrun =
    m_sageReader.getSageFileReader().getMsRunSpWithFileName(msrun_filename);
  qDebug() << msp_msrun.get()->getFileName();

  msp_identificationSageJsonFileSp =
    m_sageReader.getSageFileReader()
      .getIdentificationSageJsonFileSpWithFileName(msrun_filename);

  mp_identificationGroup =
    m_sageReader.getSageFileReader().getIdentificationGroupPtrWithFileName(
      msrun_filename);
  qDebug() << msp_msrun.get()->getFileName();
}

void
SageTsvHandler::recordLine()
{
  PeptideEvidence pe(msp_msrun.get(), m_spectrumIndex, true);
  pe.setCharge(m_line.charge);
  pe.setChecked(true);
  pe.setExperimentalMass(m_line.expmass);
  pe.setPeptideXtpSp(msp_peptide);
  pe.setIdentificationDataSource(msp_identificationSageJsonFileSp.get());
  pe.setIdentificationEngine(m_identificationEngine);
  pe.setRetentionTime(m_line.rt);
  pe.setParam(PeptideEvidenceParam::tandem_hyperscore, m_line.hyperscore);
  pe.setParam(PeptideEvidenceParam::sage_sage_discriminant_score,
              m_line.sage_discriminant_score);
  pe.setParam(PeptideEvidenceParam::sage_peptide_q, m_line.peptide_q);
  pe.setParam(PeptideEvidenceParam::sage_posterior_error,
              m_line.posterior_error);
  pe.setParam(PeptideEvidenceParam::sage_spectrum_q, m_line.spectrum_q);
  pe.setParam(PeptideEvidenceParam::sage_predicted_rt, m_line.predicted_rt);
  pe.setParam(PeptideEvidenceParam::sage_isotope_error, m_line.isotope_error);


  PeptideMatch peptide_match;
  // peptide_match.setStart(mz_peptide_evidence.start);
  peptide_match.setPeptideEvidenceSp(msp_identificationSageJsonFileSp.get()
                                       ->getPeptideEvidenceStore()
                                       .getInstance(&pe));


  for(ProteinXtpSp protein_sp : m_proteinXtpSpList)
    {
      ProteinMatch *p_protein_match =
        mp_identificationGroup->getProteinMatchInstance(
          protein_sp.get()->getAccession());

      p_protein_match->setChecked(true);
      // qDebug() << "startElement_protein p_protein_match 3 " <<
      // _p_protein_match;
      p_protein_match->setProteinXtpSp(protein_sp);
      p_protein_match->addPeptideMatch(peptide_match);
      p_protein_match->setParam(ProteinParam::q_score, m_line.protein_q);
    }

  std::size_t progress = m_lineNumber / 10000;
  if(progress > m_progressIndex)
    {
      if(mp_monitor->shouldIstop())
        {
          throw pappso::ExceptionInterrupted(
            QObject::tr("Sage TSV data reading process interrupted"));
        }
      m_progressIndex = progress;
      mp_monitor->setStatus(QString("%1K ").arg(m_progressIndex * 10));
    }
}
