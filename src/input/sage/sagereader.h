/**
 * \file input/sage/sagereader.h
 * \date 21/08/2024
 * \author Olivier Langella
 * \brief read data files from Sage output
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "../../core/project.h"
#include "../../utils/workmonitor.h"
#include "../../core/identification_sources/identificationsagejsonfile.h"
#include <pappsomspp/fasta/fastahandlerinterface.h>
#include "sagefilereader.h"

/**
 * @todo write docs
 */
class SageReader
{
  public:
  /**
   * Default constructor
   */
  SageReader(pappso::UiMonitorInterface *p_monitor,
             Project *p_project,
             const SageFileReader &sage_file_reader);

  void read();
  /**
   * Destructor
   */
  virtual ~SageReader();


  struct SageModification
  {
    pappso::AaModificationP modification;
    QChar residue;
    QString strModification;
  };

  std::vector<SageModification> getStaticModificationList() const;
  std::vector<SageModification> getVariableModificationList() const;
  FastaFileSp getFastaFileSp() const;
  Project *getProjectPtr() const;
  QString getDecoyTag() const;

   const SageFileReader & getSageFileReader() const;

  private:
  void readTsvFile();
  QString getTsvFilePath(const QJsonDocument &json_doc);
  QString getFastaFilePath(const QJsonDocument &json_doc);


  class FastaSeq : public pappso::FastaHandlerInterface
  {
    public:
    FastaSeq(SageReader *self);
    void setSequence(const QString &description_in,
                     const QString &sequence_in) override;

    private:
    SageReader *mp_self;
    QString m_decoyTag;
  };

  private:
  const SageFileReader &m_sageFileReader;
  pappso::UiMonitorInterface *mp_monitor;
  Project *mp_project;
  FastaFileSp msp_fastaFile;
};
