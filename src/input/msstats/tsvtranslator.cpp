/**
 * \file /input/msstats/tsvtranslator.cpp
 * \date 14/02/2024
 * \author Olivier Langella
 * \brief translates TSV file to ODS
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tsvtranslator.h"

TsvTranslator::TsvTranslator(CalcWriterInterface *p_writer)
  : MsstatsTsvTranslator(p_writer)
{
}

TsvTranslator::~TsvTranslator()
{
}

void
TsvTranslator::startLine()
{
  m_firstCell = false;
}

void
TsvTranslator::endSheet()
{
}
