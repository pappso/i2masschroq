/**
 * \file /input/msstats/msstatstsvtranslator.cpp
 * \date 30/05/2023
 * \author Olivier Langella
 * \brief translates TSV MSstats file to ODS
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "msstatstsvtranslator.h"

MsstatsTsvTranslator::MsstatsTsvTranslator(CalcWriterInterface *p_writer)
{
  mp_writer = p_writer;
}

MsstatsTsvTranslator::~MsstatsTsvTranslator()
{
  qDebug();
}


void
MsstatsTsvTranslator::startSheet(const QString &sheet_name)
{
  qDebug() << sheet_name;

  mp_writer->writeSheet(sheet_name);

  OdsTableSettings table_settings;
  table_settings.setVerticalSplit(1);
  mp_writer->setCurrentOdsTableSettings(table_settings);
}

/**
 * callback that indicates the end of the current data sheet. Override it if
 * needed
 */
void
MsstatsTsvTranslator::endSheet()
{
  //_p_writer-> endSheet();

  qDebug();
  if(!m_firstCellCoordinate.isEmpty())
    {
      qDebug() << " " << m_firstCellCoordinate << " " << m_lastCellCoordinate;
      OdsColorScale color_scale(m_firstCellCoordinate, m_lastCellCoordinate);
      mp_writer->addColorScale(color_scale);
      m_firstCellCoordinate = "";
    }
}

/**
 * callback that indicates a new line start. Override it if needed.
 */
void
MsstatsTsvTranslator::startLine()
{
  //_p_writer-> writeLine();
  m_firstCell = true;
}

/**
 * callback that indicates a line ending. Override it if needed.
 */
void
MsstatsTsvTranslator::endLine()
{
  mp_writer->writeLine();
  m_firstLine = false;
  m_firstCell = true;
}

/**
 * callback that report the content of the current cell in a dedicated Cell
 * object. Override it if you need to retrieve cell content.
 */
void
MsstatsTsvTranslator::setCell(const OdsCell &cell)
{
  if(!m_firstLine && m_firstCell)
    {
      m_firstCell = false;
      return;
    }
  m_firstCell = false;
  qDebug() << cell.toString();
  if(cell.isBoolean())
    {
      mp_writer->writeCell(cell.getBooleanValue());
    }
  else if(cell.isDate())
    {
      mp_writer->writeCell(cell.getDateTimeValue());
    }
  else if(cell.isDouble())
    {
      mp_writer->writeCell(cell.getDoubleValue());
    }
  else if(cell.isEmpty())
    {
      mp_writer->writeEmptyCell();
    }
  else
    {

      qDebug() << cell.toString();
      mp_writer->writeCell(cell.toString());
    }

  m_lastCellCoordinate = mp_writer->getOdsCellCoordinate();
  if(m_firstCellCoordinate.isEmpty())
    {
      m_firstCellCoordinate = mp_writer->getOdsCellCoordinate();
    }
}

/**
 * callback that report the end of the ODS document. Override it if you need
 * to know that reading is finished.
 */
void
MsstatsTsvTranslator::endDocument()
{
  qDebug() << mp_writer->getOdsCellCoordinate();
  endSheet();
}
