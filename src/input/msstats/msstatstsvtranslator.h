/**
 * \file /input/msstats/msstatstsvtranslator.h
 * \date 30/05/2023
 * \author Olivier Langella
 * \brief translates TSV MSstats file to ODS
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <odsstream/odsdochandlerinterface.h>
#include <odsstream/calcwriterinterface.h>


class MsstatsTsvTranslator : public OdsDocHandlerInterface
{
  public:
  MsstatsTsvTranslator(CalcWriterInterface *p_writer);
  virtual ~MsstatsTsvTranslator();

  virtual void startSheet(const QString &sheet_name) override;

  virtual void endSheet() override;

  virtual void startLine() override;

  virtual void endLine() override;

  virtual void setCell(const OdsCell &cell) override;

  virtual void endDocument() override;

  protected:
  CalcWriterInterface *mp_writer;
  bool m_firstLine = true;
  bool m_firstCell = true;

  QString m_firstCellCoordinate = "";
  QString m_lastCellCoordinate = "";
};
