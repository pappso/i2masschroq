/**
 * \file /input/mascot/mimeparser.h
 * \date 17/2/2018
 * \author Olivier Langella
 * \brief basic mime multipart parser
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QIODevice>
#include <QTextStream>

class MimeParser
{
  public:
  MimeParser(QIODevice *p_inputstream);
  virtual ~MimeParser();
  bool open();
  bool close();
  bool goToFirstFile();
  bool goToNextFile();
  const QString &getCurrentMimeType() const;
  const QString &getCurrentFileName() const;
  QTextStream &getCurrentTextStream();

  private:
  bool readFile();

  private:
  QString _current_file_name;
  QString _current_mime_type;
  QIODevice *_p_inputstream = nullptr;
  QTextStream *_real_in     = nullptr;
  QString _boundary;
  QTextStream *_p_current_file_content = nullptr;
  QString _priv_file_string;
};
