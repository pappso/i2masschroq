/**
 * \file src/input/tandem/tandeminfoparser.h
 * \date 23/12/2021
 * \author Olivier Langella
 * \brief reads tandem xml result files to parse various informations
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <pappsomspp/types.h>
#include <pappsomspp/amino_acid/aa.h>
#include <pappsomspp/processing/project/projectparameters.h>

/**
 * @todo write docs
 */
class TandemInfoParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  TandemInfoParser();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TandemInfoParser(const TandemInfoParser &other);

  /**
   * Destructor
   */
  ~TandemInfoParser();


  const QString &getSpectraDataLocation() const;

  std::size_t getModelCount() const;

  pappso::MsDataFormat getMsDataFormat() const;

  const std::map<pappso::AminoAcidChar, pappso::AaModificationP> &
  getResidueMassModifications() const;


  const pappso::ProjectParameters &getProjectParameters() const;

  protected:
  virtual void readStream() override;


  private:
  void readResidueMassParameters();

  void setMapAminoAcidChar2AaModificationP(pappso::AminoAcidChar amino_acid,
                                           double mass);

  private:
  QString m_spectrum_path;
  std::size_t m_modelCount = 0;

  pappso::MsDataFormat m_msDataFormat = pappso::MsDataFormat::unknown;

  std::map<pappso::AminoAcidChar, pappso::AaModificationP>
    m_mapAminoAcidChar2AaModificationP;
  pappso::ProjectParameters m_projectParameters;
};
