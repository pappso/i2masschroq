/**
 * \file src/input/tandem/tandemreader.h
 * \date 29/8/2022
 * \author Olivier Langella
 * \brief new method to read X!Tandem XML files
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../../core/project.h"
#include "../../utils/workmonitor.h"
#include "../../core/identification_sources/identificationxtandemfile.h"

#pragma once

/**
 * @todo write docs
 */
class TandemReader : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  TandemReader(pappso::UiMonitorInterface *p_monitor,
               Project *p_project,
               IdentificationGroup *p_identification_group,
               IdentificationXtandemFile *p_identification_data_source,
               std::map<pappso::AminoAcidChar, pappso::AaModificationP>
                 residue_modifications);

  /**
   * Destructor
   */
  virtual ~TandemReader();


  protected:
  virtual void readStream() override;

  private:
  bool readGroup();
  struct TandemGroup
  {
    QString label;
    QString type;
    unsigned int scan;
    double mhplusObser;
    unsigned int charge;
    double retentionTime;
    ProteinXtp protein;
    ProteinMatch *p_proteinMatch = nullptr;
    PeptideXtpSp peptide_sp;
    PeptideMatch peptideMatch;
  };
  void readDomain(TandemGroup &tandem_group);
  void readGroupTypeParameters(TandemGroup &tandem_group);

  private:
  pappso::UiMonitorInterface *mp_monitor;
  Project *mp_project;
  IdentificationGroup *mp_identificationGroup;
  IdentificationXtandemFile *mp_identificationDataSource;
  MsRunSp msp_msrun;
  std::map<pappso::AminoAcidChar, pappso::AaModificationP>
    m_residueMassModificationMap;
};
