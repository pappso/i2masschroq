/**
 * \file /input/tidd/tiddresultreader.cpp
 * \date 30/9/2022
 * \author Olivier Langella
 * \brief reads tsv output file from TIDD R process
 * (https://pubmed.ncbi.nlm.nih.gov/35354356/)
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tiddresultreader.h"
#include <pappsomspp/pappsoexception.h>

TiddResultReader::TiddResultReader(pappso::UiMonitorInterface *p_monitor)
{
  mp_monitor = p_monitor;
}

TiddResultReader::~TiddResultReader()
{
}

void
TiddResultReader::endDocument()
{
  std::size_t peptide_evidence_index = m_lineNumber - 1;
  if(peptide_evidence_index < m_peptideEvidenceList.size())
    {
      throw pappso::PappsoException(
        QObject::tr("more peptide evidences (%1) than SVM results (%2)")
          .arg(m_peptideEvidenceList.size())
          .arg(peptide_evidence_index));
    }
}

void
TiddResultReader::endLine()
{
  m_lineNumber++;
}

void
TiddResultReader::endSheet()
{
}

void
TiddResultReader::setCell(const OdsCell &cell)
{
  if(m_lineNumber == 0)
    {
      if(cell.toString().startsWith("SVM_Prob_"))
        {
          m_svmProbColumnIsOk = true;
          m_svmProbColumn     = m_columnNumber + 1;
        }
    }
  else
    {
      if(m_columnNumber == m_svmProbColumn)
        {
          double svm_prob                    = cell.getDoubleValue();
          std::size_t peptide_evidence_index = m_lineNumber - 1;
          if(peptide_evidence_index >= m_peptideEvidenceList.size())
            {
              throw pappso::PappsoException(
                QObject::tr(
                  "more SVM results (%1) than original peptide evidences (%2)")
                  .arg(peptide_evidence_index)
                  .arg(m_peptideEvidenceList.size()));
            }
          m_peptideEvidenceList[peptide_evidence_index]->setParam(
            PeptideEvidenceParam::svmProb, svm_prob);
        }
    }

  m_columnNumber++;
}

void
TiddResultReader::startLine()
{
  m_columnNumber = 0;
}

void
TiddResultReader::startSheet([[maybe_unused]] const QString &sheet_name)
{
  m_columnNumber = 0;
  m_lineNumber   = 0;
  mp_monitor->setStatus(QObject::tr("reading SVM probabilities"));
}

void
TiddResultReader::setPeptideEvidenceList(
  const std::vector<PeptideEvidence *> &peptide_evidence_list)
{
  m_peptideEvidenceList = peptide_evidence_list;
}
