/**
 * \file /input/tidd/tiddresultreader.h
 * \date 30/9/2022
 * \author Olivier Langella
 * \brief reads tsv output file from TIDD R process
 * (https://pubmed.ncbi.nlm.nih.gov/35354356/)
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <odsstream/odsdochandlerinterface.h>
#include "../../core/peptideevidence.h"

/**
 * @todo write docs
 */
class TiddResultReader : public OdsDocHandlerInterface
{
  public:
  /**
   * Default constructor
   */
  TiddResultReader(pappso::UiMonitorInterface *p_monitor);

  /**
   * Destructor
   */
  virtual ~TiddResultReader();

  /**
   * callback that indicates the begining of a data sheet. Override it in
   * order to retrieve information about the current data sheet.
   *
   */
  virtual void startSheet(const QString &sheet_name) override;

  /**
   * callback that indicates the end of the current data sheet. Override it if
   * needed
   */
  virtual void endSheet() override;

  /**
   * callback that indicates a new line start. Override it if needed.
   */

  virtual void startLine() override;

  /**
   * callback that indicates a line ending. Override it if needed.
   */

  virtual void endLine() override;

  /**
   * callback that report the content of the current cell in a dedicated Cell
   * object. Override it if you need to retrieve cell content.
   */
  virtual void setCell(const OdsCell &cell) override;

  /**
   * callback that report the end of the ODS document. Override it if you need
   * to know that reading is finished.
   */
  virtual void endDocument() override;


  void setPeptideEvidenceList(
    const std::vector<PeptideEvidence *> &peptide_evidence_list);

  private:
  pappso::UiMonitorInterface *mp_monitor = nullptr;
  std::size_t m_lineNumber               = 0;
  std::size_t m_columnNumber             = 0;
  std::size_t m_svmProbColumn            = 0;
  bool m_svmProbColumnIsOk               = false;
  std::vector<PeptideEvidence *> m_peptideEvidenceList;
};
