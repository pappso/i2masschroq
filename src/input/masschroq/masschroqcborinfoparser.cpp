/**
 * \file src/input/masschroq/masschroqcborinfoparser.cpp
 * \date 14/02/2025
 * \author Olivier Langella
 * \brief read cbor stream for MCQR
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "masschroqcborinfoparser.h"
#include "../../cli/export/cborstreamreadertsv.h"
#include <pappsomspp/pappsoexception.h>

MassChroqCborInfoParser::MassChroqCborInfoParser()
{
}

MassChroqCborInfoParser::~MassChroqCborInfoParser()
{
}

const pappso::ProjectParameters &
MassChroqCborInfoParser::getProjectParameters() const
{
  return m_projectParameters;
}


void
MassChroqCborInfoParser::setMcqrWorkingDirectory(const QString &mcqr_directory)
{
  m_mcqrWorkingDirectory = mcqr_directory;
}

void
MassChroqCborInfoParser::readProjectParameters()
{
  pappso::ProjectParameters project_parameters(
    QCborValue::fromCbor(m_cborReader).toJsonValue().toObject());
  m_projectParameters.merge(project_parameters);
}

void
MassChroqCborInfoParser::writePeptideProteinFiles(
  const QString &cbor_file, pappso::UiMonitorInterface &monitor)
{
  mcql::CborStreamReaderTsv tsv_out(m_mcqrWorkingDirectory);
  m_isOk = false;

  QFile f(cbor_file);
  if(f.open(QIODevice::ReadOnly))
    {
      qDebug();
      tsv_out.readCbor(&f, monitor);
      m_quantifiedGroupIdList = tsv_out.getQuantifiedGroupIdList();
      m_isOk                  = true;
    }
  else
    {

      throw pappso::PappsoException(
        QObject::tr("failure opening file %1")
          .arg(QFileInfo(cbor_file).absolutePath()));
    }
}

void
MassChroqCborInfoParser::readIdentificationData(
  pappso::UiMonitorInterface &monitor)
{

  m_cborReader.enterContainer();
  while(getExpectedString())
    {
      if(m_expectedString == "msrun_list")
        {
          QJsonObject jmsrun_list =
            QCborValue::fromCbor(m_cborReader).toJsonValue().toObject();
          for(auto &msrun : jmsrun_list.keys())
            {
              qDebug() << msrun;
              QString file =
                jmsrun_list.value(msrun).toObject().value("file").toString();

              //<data_file id="msruna1" format="mzxml"
              //
              // path="/gorgone/pappso/data_extraction_pappso/mzXML/20120906_balliau_extract_1_A01_urnb-1.mzXML"
              // type="centroid"/>
              McqrMetadata metadata;
              metadata.m_msrunId = msrun;

              metadata.m_msrunFile = QFileInfo(file).fileName();
              m_mcqrMetadataSet.push_back(metadata);
              // m_qxmlStreamReader.skipCurrentElement();
            }
        }
      else
        {
          m_cborReader.next();
        }
    }
  m_cborReader.leaveContainer();
}

const McqrMetadataSet &
MassChroqCborInfoParser::getMcqrMetadataSet() const
{
  return m_mcqrMetadataSet;
}

bool
MassChroqCborInfoParser::isOk() const
{
  return m_isOk;
}

const QString
MassChroqCborInfoParser::getProteinTsvResultFilename() const
{
  if(!isOk())
    {
    }
  return (QDir(m_mcqrWorkingDirectory).absoluteFilePath("proteins.tsv"));
}

const QString
MassChroqCborInfoParser::getPeptideTsvResultFilename(
  const QString &group_id) const
{
  if(!isOk())
    {
    }
  return (QDir(m_mcqrWorkingDirectory)
            .absoluteFilePath(QString("peptides_%1.tsv").arg(group_id)));
}

const QString
MassChroqCborInfoParser::getSingleGroupId() const
{
  isOk();
  if(m_quantifiedGroupIdList.size() == 1)
    {
      return m_quantifiedGroupIdList.at(0);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("There must be only one group id (%1)")
          .arg(m_quantifiedGroupIdList.size()));
    }
}
