/**
 * \file src/input/masschroq/masschroqcborinfoparser.h
 * \date 14/02/2025
 * \author Olivier Langella
 * \brief read cbor stream for MCQR
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../cli/export/cborstreamreaderbase.h"
#include <pappsomspp/processing/project/projectparameters.h>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include "../../core/mcqr/mcqrmetadataset.h"
/**
 * @todo write docs
 */
class MassChroqCborInfoParser : public CborStreamReaderBase
{
  public:
  /**
   * Default constructor
   */
  MassChroqCborInfoParser();

  /**
   * Destructor
   */
  ~MassChroqCborInfoParser();

  void setMcqrWorkingDirectory(const QString &mcqr_directory);

  const pappso::ProjectParameters &getProjectParameters() const;

  void writePeptideProteinFiles(const QString &cbor_file,
                                pappso::UiMonitorInterface &monitor);

  const McqrMetadataSet &getMcqrMetadataSet() const;

  const QString getPeptideTsvResultFilename(const QString &group_id) const;
  const QString getProteinTsvResultFilename() const;

  const QString getSingleGroupId() const;

  bool isOk() const;

  protected:
  virtual void readProjectParameters() override;
  virtual void
  readIdentificationData(pappso::UiMonitorInterface &monitor) override;

  private:
  QString m_mcqrWorkingDirectory;
  pappso::ProjectParameters m_projectParameters;
  McqrMetadataSet m_mcqrMetadataSet;
  bool m_isOk = false;
  std::vector<QString> m_quantifiedGroupIdList;
};
