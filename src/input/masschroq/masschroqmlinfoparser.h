/**
 * \file /input/masschroq/masscgroqmlinfoparser.h
 * \date 27/05/2023
 * \author Olivier Langella
 * \brief extracts important informations from masschroqML file
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <QFileInfo>
#include "../../core/mcqr/mcqrmetadataset.h"
#include <map>
#include <pappsomspp/processing/project/projectparameters.h>

/**
 * @todo write docs
 */
class MasschroqMlInfoParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  MasschroqMlInfoParser();

  /**
   * Destructor
   */
  ~MasschroqMlInfoParser();


  virtual bool readFile(const QString &fileName) override;

  const QString &getTsvResultDirectoryPath() const;

  QString getProteinTsvResultFilename(const QString &q_id) const;
  QString getPeptideTsvResultFilename(const QString &q_id) const;

  const McqrMetadataSet &getMcqrMetadataSet() const;
  const std::map<QString, QString> &getMapQuantifyIdToGroup() const;

  McqrMetadataSet getMcqrMetadataSet(const QString &group_id) const;

  const QString &getSingleQid() const;


  const QString &getGroupIdbyQid(const QString &q_id) const;

  /** @brief tell if masschroqML file was correctly parsed
   * @return bool true if OK
   */
  bool isOk() const;


  /** @brief get project parameters
   */
  pappso::ProjectParameters getProjectParameters() const;

  protected:
  void read_project_parameters();
  void read_rawdata();
  void read_groups();
  void read_quantification();
  void read_alignments();
  void read_quantification_methods();

  void getMcqVersion();


  protected:
  virtual void readStream() override;

  private:
  McqrMetadataSet m_mcqrMetadataSet;
  QString m_tsvdir;
  std::map<QString, QStringList> m_mapGroups;
  std::map<QString, QString> m_mapQuantifyIdToGroup;
  bool m_isOk = false;

  pappso::ProjectParameters m_projectParameters;
};
