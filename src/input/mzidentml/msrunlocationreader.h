/**
 * \file src/input/mzidentml/msrunlocationreader.h
 * \date 16/7/2022
 * \author Olivier Langella
 * \brief parse mzIdentML result file to only get the msrun
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <pappsomspp/processing/project/projectparameters.h>


#pragma once

/**
 * @todo write docs
 */
class MsRunLocationReader : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  MsRunLocationReader();

  /**
   * Destructor
   */
  ~MsRunLocationReader();
  struct SpectraData
  {
    QString location;
    QString id;
    QString name = "";
  };

  const std::vector<SpectraData> &getSpectraDataList() const;
  const pappso::ProjectParameters &getProjectParameters() const;

  protected:
  virtual void readStream() override;


  private:
  void readAnalysisSoftwareList();
  void readSpectraData();
  void readAdditionalSearchParams();
  pappso::ProjectParam readUserParam() const;
  pappso::ProjectParam readCvParam(pappso::ProjectParamCategory category,
                                   const QString &name) const;

  private:
  std::vector<SpectraData> m_spectraDataList;
  pappso::ProjectParameters m_projectParameters;
};
