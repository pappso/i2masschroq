/**
 * \file src/input/pepxml/pepxmlreader.h
 * \date 19/9/2022
 * \author Olivier Langella
 * \brief new method to read pep XML files
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../../core/project.h"
#include "../../utils/workmonitor.h"
#include "../../core/identification_sources/identificationpepxmlfile.h"
#include <map>

#pragma once

/**
 * @todo write docs
 */
class PepXmlReader : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  PepXmlReader(pappso::UiMonitorInterface *p_monitor,
               Project *p_project,
               IdentificationGroup *p_identification_group,
               IdentificationPepXmlFile *p_identification_data_source);

  /**
   * Destructor
   */
  virtual ~PepXmlReader();


  protected:
  virtual void readStream() override;


  private:
  void readMsms_pipeline_analysis();
  void readMsms_run_summary();
  void readSearch_summary();
  void readSpectrum_query();

  struct SpectrumQuery
  {
    QString spectrum_ref;
    unsigned int start_scan;
    unsigned int end_scan;
    unsigned int scan;
    unsigned int charge;
    double retention_time;
    double precursor_neutral_mass;
    double ion_mobility = 0;
  };


  void readSearch_hit(const SpectrumQuery &spectrum_query);
  void readSearch_score(PeptideEvidence *peptide_evidence);

  private:
  pappso::UiMonitorInterface *mp_monitor;
  Project *mp_project;
  IdentificationGroup *mp_identificationGroup;
  IdentificationPepXmlFile *mp_identificationDataSource;
  MsRunSp msp_msrun;


  QString m_currentCompleteMsrunFilePath;
  IdentificationEngine m_searchEngine;
  pappso::MsDataFormat m_msDataFormat = pappso::MsDataFormat::unknown;

  std::map<double, pappso::AaModificationP> m_mapNterModifications;
  std::map<double, pappso::AaModificationP> m_mapModifications;
  std::map<pappso::AminoAcidChar, pappso::AaModificationP> m_mapFixedModifications;
  std::vector<double> m_fixedModificationsMassList;
  // QString m_currentSearchEngine;
};
