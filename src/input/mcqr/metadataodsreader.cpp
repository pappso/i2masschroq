/**
 * \file /input/mcqr/metadataodsreader.cpp
 * \date 14/09/2023
 * \author Olivier Langella
 * \brief read metadata ods file
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "metadataodsreader.h"
#include <pappsomspp/pappsoexception.h>

MetadataOdsReader::MetadataOdsReader()
{
}

MetadataOdsReader::~MetadataOdsReader()
{
}

void
MetadataOdsReader::endDocument()
{
  if(!m_currentMetadata.m_msrunId.isEmpty())
    {
      m_mcqrMetadataSet.push_back(m_currentMetadata);
    }
  m_isContent = true;
  m_currentMetadata.m_otherData.clear();
  m_currentMetadata.m_msrunId = "";
}
void
MetadataOdsReader::endLine()
{
  if(!m_currentMetadata.m_msrunId.isEmpty())
    {
      /*
      if(m_currentMetadata.isMetadataEmpty())
        {
          throw pappso::PappsoException(
            QObject::tr("msrun %1 %2 has no metadata")
              .arg(m_currentMetadata.m_msrunId)
              .arg(m_currentMetadata.m_msrunFile));
        }*/
      m_mcqrMetadataSet.push_back(m_currentMetadata);
    }
  m_isContent = true;
  m_currentMetadata.m_otherData.clear();
  m_currentMetadata.m_msrunId = "";
}
void
MetadataOdsReader::endSheet()
{
  if(!m_currentMetadata.m_msrunId.isEmpty())
    {
      m_mcqrMetadataSet.push_back(m_currentMetadata);
    }
  m_isContent = true;
  m_currentMetadata.m_otherData.clear();
  m_currentMetadata.m_msrunId = "";
}
McqrMetadataSet
MetadataOdsReader::getMcqrMetadataSet() const
{
  return m_mcqrMetadataSet;
}
void
MetadataOdsReader::setCell(const OdsCell &cell)
{
  if(!m_isContent)
    {

      if(cell.isEmpty())
        {
        }
      else
        {
          m_headers << cell.getStringValue();
        }
    }
  else
    {
      if(m_column < (std::size_t)m_headers.size())
        {
          QString current_column_name = m_headers[m_column];
          if(current_column_name == "msrun")
            {
              if(!cell.isEmpty())
                {
                  m_currentMetadata.m_msrunId = cell.getStringValue();
                }
            }
          else if(current_column_name == "msrunfile")
            {
              if(!cell.isEmpty())
                {
                  m_currentMetadata.m_msrunFile = cell.getStringValue();
                }
            }
          else
            {
              if(!cell.isEmpty())
                {
                  m_currentMetadata.m_otherData.insert(
                    std::pair<QString, QString>(current_column_name,
                                                cell.toString()));
                }
            }
        }
    }
  m_column++;
}

void
MetadataOdsReader::startLine()
{
  m_column = 0;
  if(m_headers.size() == 0)
    { // header
    }
  else
    {
      if(!m_currentMetadata.m_msrunId.isEmpty())
        {
          m_mcqrMetadataSet.push_back(m_currentMetadata);
        }
      m_isContent = true;
      m_currentMetadata.m_otherData.clear();
      m_currentMetadata.m_msrunId = "";
    }
}
void
MetadataOdsReader::startSheet(const QString &sheet_name [[maybe_unused]])
{
}
