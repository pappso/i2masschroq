/**
 * \file src/input/xpipreader.h
 * \date 23/8/2022
 * \author Olivier Langella
 * \brief new method to read XPIP i2MassChroQ XML files
 */


/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../core/project.h"
#include "../utils/workmonitor.h"

#pragma once

/**
 * @todo write docs
 */
class XpipReader : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  XpipReader(pappso::UiMonitorInterface *p_monitor, Project *p_project);

  /**
   * Destructor
   */
  ~XpipReader();


  bool isI2masschroqXpip() const;

  protected:
  virtual void readStream() override;

  private:
  void readDescription();
  void readCounts();
  void readFilterParams();
  void readFastaFileList();
  void readContaminants();
  void readDecoys();
  void readMsrunList();
  bool readMsrunAlignmentGroupList();
  void readStats(MsRunSp sp_msrun);
  void readIdentificationSourceList();
  void
  readParamsStats(IdentificationDataSourceSp sp_current_identification_source);
  void readProteinList();
  bool readProtein();
  void readPeptideList();
  void readLabelMethodModificationList();
  bool readLabel(LabelingMethodSp labelingMethodSp);
  bool readPeptide();
  bool readIdentificationGroup();
  bool readPeptideEvidence(IdentificationDataSourceSp identification_source_sp);
  bool readProteinMatch(IdentificationGroup *identification_group_p);
  void readAlignmentParams();
  void readQuantiParams();
  void readMasschroqParams(MsRunAlignmentGroupSp sp_alignment_group);
  void readParams(PeptideEvidenceSp peptide_evidence_sp);
  void readParams(ProteinMatch *p_protein_match);

  static QVariant string2QVariant(const QString &value);

  private:
  bool m_isI2masschroqXpip = false;
  pappso::UiMonitorInterface *mp_monitor;
  Project *mp_project;

  std::size_t m_totalProteins;
  std::size_t m_totalPeptides;
  std::size_t m_totalPeptideEvidences;
  std::size_t m_totalProteinMatches;
  std::size_t m_countProteins;
  std::size_t m_countPeptides;
  std::size_t m_countPeptideEvidences;
  std::size_t m_countProteinMatches;
  std::size_t m_countTotal;
  std::size_t m_total;


  AutomaticFilterParameters m_automaticFilterParameters;
  std::map<QString, FastaFileSp> m_mapFastaFiles;
  std::map<QString, MsRunSp> m_mapMsruns;
  std::map<QString, IdentificationDataSourceSp> m_mapIdentSources;
  std::map<QString, ProteinXtpSp> m_mapProteins;
  std::map<QString, pappso::AaModificationP> m_mapModifs;
  std::map<QString, PeptideXtpSp> m_mapPeptides;
  std::map<QString, PeptideEvidenceSp> m_mapPeptideEvidences;


  LabelingMethodSp msp_labelingMethod;
};
