
/**
 * \file mcql/input/masschroqml2jsonparser.h
 * \date 29/12/2024
 * \author Olivier Langella
 * \brief masschroqML parser conversion 2 json for MassChroqLight
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/peptide/peptide.h>
#include <QFileInfo>
#include <QJsonObject>

namespace mcql
{

/**
 * @todo write docs
 */
class MassChroqMl2JsonParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  MassChroqMl2JsonParser();

  /**
   * Destructor
   */
  virtual ~MassChroqMl2JsonParser();

  void populateJsonDocument(QJsonDocument &document) const;


  virtual bool readFile(const QString &fileName) override;

  const QJsonObject &getJsonProjectParameters() const;
  const QJsonObject &getJsonProteinList() const;
  const QJsonObject &getJsonMsRunList() const;
  const QJsonObject &getJsonPeptideList() const;
  const QJsonObject &getJsonMsRunPeptideList() const;
  const QJsonObject &getJsonAlignmentMethod() const;
  const QJsonObject &getJsonQuantificationMethod() const;

  const QJsonObject &getJsonGroupList() const;
  const QJsonObject &getJsonQuantifyGroup() const;


  protected:
  virtual void readStream() override;


  private:
  // void readRawdata();
  void readProjectParameters();
  void readAlignments();
  void readAlignmentMethods();
  void readQuantificationMethods();
  void readGroups();
  void readProteinList();
  void read_protein();
  void readRawdata();
  void readPeptideList();
  void read_peptide();
  void read_psimod(pappso::Peptide &peptide);
  void read_observed_in(const QString &peptide_id);
  void read_alignment_method();
  void read_align();
  void read_ms2();
  void read_quantification_method();
  void read_xic_filters();
  void read_peak_detection();
  void read_detection_zivy();
  void read_detection_moulon();
  void read_xic_filter(pappso::FilterSuiteString &filter_suite);


  private:
  QJsonObject m_jsonProjectParameters;
  QJsonObject m_jsonProteinList;
  QJsonObject m_jsonMsRunList;
  QJsonObject m_jsonPeptideList;
  QJsonObject m_jsonMsRunPeptideList;
  QJsonObject m_jsonAlignmentMethod;

  QJsonObject m_jsonQuantificationMethod;
  QJsonObject m_jsonGroupList;
  /*
   *
    "quantify_group": {"g1": {
        "alignment_reference":  "msruna1"
    }*/
  QJsonObject m_jsonQuantifyGroup;

  std::map<QString, std::map<QString, QJsonArray>>
    m_msrunPeptideJsonObservationMap;
};
} // namespace mcql
