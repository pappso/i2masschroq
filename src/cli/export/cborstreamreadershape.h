/**
 * \file src/cli/export/cborstreamreadershape.h
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief read cbor stream for shape output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <odsstream/tsvoutputstream.h>
#include "cborstreamreaderbase.h"
/**
 * @todo write docs
 */
class CborStreamReaderShape : public CborStreamReaderBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderShape(const QString &output_directory);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderShape();

  virtual void reportMsRunRetentionTime(
    const pappso::MsRunRetentionTime<QString> &rt_line) override;


  protected:
  virtual void reportPeakLine(const PeakStruct &peak) override;
  virtual void readPeakShape() override;
  virtual void reportMsRun() override;

  private:
  TsvDirectoryWriter *mpa_calcWriterInterface;

  std::map<QString, pappso::MsRunRetentionTime<QString>> m_msrun2rt;
  std::size_t m_peakIndex = 0;

  pappso::MsRunRetentionTime<QString> *m_msrunRetentionTimePtr = nullptr;
};
