/**
 * \file src/cli/export/cborstreamreaderbase.cpp
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief read cbor stream from masschroq3
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "cborstreamreaderbase.h"
#include <pappsomspp/pappsoexception.h>

CborStreamReaderBase::CborStreamReaderBase()
{
}

CborStreamReaderBase::~CborStreamReaderBase()
{
}

void
CborStreamReaderBase::startGroup()
{
}

void
CborStreamReaderBase::readCbor(QFile *cborp,
                               pappso::UiMonitorInterface &monitor)
{
  qDebug();
  initCborReader(cborp);

  qDebug();
  if(m_cborReader.isMap())
    {
      readRoot(monitor);
    }
  qDebug();
}

void
CborStreamReaderBase::readInformations(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.next();
}

void
CborStreamReaderBase::readQuantificationData(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();


  while(!m_cborReader.lastError() && m_cborReader.hasNext())
    {
      readQuantificationDataObject(monitor);
      // m_cborReader.next();
      //  dumpOne(monitor, 0);
      //  skipCurrentElement();
    }
  qDebug();
  m_cborReader.leaveContainer();
}


void
CborStreamReaderBase::checkError(const QString &element_name)
{
  if(m_cborReader.type() == QCborStreamReader::Invalid)
    {
      throw pappso::PappsoException(QObject::tr("ERROR: %1 in %2")
                                      .arg(m_cborReader.lastError().toString())
                                      .arg(element_name));
    }
}
void
CborStreamReaderBase::readRoot(pappso::UiMonitorInterface &monitor)
{

  qDebug();
  m_cborReader.enterContainer();

  getExpectedString();
  if(m_expectedString == "informations")
    {
      readInformations(monitor);
    }
  else
    {
      throw pappso::PappsoException("ERROR: expecting informations element");
    }


  qDebug();
  getExpectedString();
  if(m_expectedString == "project_parameters")
    {
      readProjectParameters();
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting project_parameters element");
    }

  qDebug();

  getExpectedString();
  if(m_expectedString == "masschroq_methods")
    {
      readMasschroqMethods(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting masschroq_methods element");
    }

  qDebug();
  getExpectedString();
  if(m_expectedString == "identification_data")
    {
      readIdentificationData(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        "ERROR: expecting identification_data element");
    }

  qDebug();
  getExpectedString();
  if(m_expectedString == "actions")
    {
      readActions(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting actions element not %1")
          .arg(m_expectedString));
    }

  qDebug();
  getExpectedString();
  if(m_expectedString == "alignment_data")
    {
      m_cborReader.enterContainer(); // array
      while(!m_cborReader.lastError() && m_cborReader.hasNext())
        {
          readAlignmentData(monitor);
        }
      m_cborReader.leaveContainer(); // array
      getExpectedString();
    }

  if(m_expectedString == "quantification_data")
    {
      readQuantificationData(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting quantification_data element not %1")
          .arg(m_expectedString));
    }

  qDebug();
  getExpectedString();
  if(m_expectedString == "end")
    {
      readEnd(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting end element not %1")
          .arg(m_expectedString));
    }
  qDebug();
}


void
CborStreamReaderBase::readAlignment()
{
  // m_cborReader.next();
  if(!m_cborReader.enterContainer())
    {
      checkError("alignment");
    }
  getExpectedString();
  if(m_expectedString == "msrun_ref")
    {
      getExpectedString();
      qDebug() << m_expectedString;
      m_msrunReference = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting msrun_ref element not %1")
          .arg(m_expectedString));
    }
  getExpectedString();
  if(m_expectedString == "corrections")
    {

      if(!m_cborReader.enterContainer())
        {
          checkError("corrections");
        }
      while(getExpectedString())
        {
          m_msrunId = m_expectedString;
          m_cborReader.enterContainer();
          getExpectedString();
          std::vector<double> original_rt;
          std::vector<double> aligned_rt;
          if(m_expectedString == "original")
            {
              readVector(original_rt);
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting original element not %1")
                  .arg(m_expectedString));
            }
          getExpectedString();
          if(m_expectedString == "aligned")
            {
              readVector(aligned_rt);
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting aligned element not %1")
                  .arg(m_expectedString));
            }
          pappso::MsRunRetentionTime<QString> rt_line(original_rt);
          rt_line.setAlignedRetentionTimeVector(aligned_rt);
          reportMsRunRetentionTime(rt_line);
          m_cborReader.leaveContainer();
        }
      m_cborReader.leaveContainer();
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting corrections element not %1")
          .arg(m_expectedString));
    }
  m_cborReader.leaveContainer();
  qDebug();
}

void
CborStreamReaderBase::reportMsRunRetentionTime(
  const pappso::MsRunRetentionTime<QString> &rt_line)
{
}

void
CborStreamReaderBase::readAlignmentData(pappso::UiMonitorInterface &monitor)
{
  // m_cborReader.next();
  if(!m_cborReader.enterContainer())
    {
      checkError("alignment_data");
    }
  qDebug() << m_expectedString;
  getExpectedString();
  if(m_expectedString == "alignment_id")
    {
      getExpectedString();
      qDebug() << m_expectedString;
      m_alignmentId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting alignment_id element not %1")
          .arg(m_expectedString));
    }
  getExpectedString();
  if(m_expectedString == "group_id")
    {
      getExpectedString();
      m_groupId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting group_id element not %1")
          .arg(m_expectedString));
    }
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  if(m_expectedString == "alignment")
    {
      readAlignment();
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting alignment element not %1")
          .arg(m_expectedString));
    }
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  m_cborReader.leaveContainer();
  qDebug();
}


void
CborStreamReaderBase::readQuantificationDataObject(
  pappso::UiMonitorInterface &monitor)
{

  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  if(m_expectedString == "quantify_id")
    {
      getExpectedString();
      m_quantificationId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting quantify_id element not %1")
          .arg(m_expectedString));
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  getExpectedString();
  if(m_expectedString == "group_id")
    {
      getExpectedString();
      m_groupId = m_expectedString;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting group_id element not %1")
          .arg(m_expectedString));
    }

  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  startGroup();
  getExpectedString();

  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  if(m_expectedString == "first_pass")
    {
      startFirstPass();
      readGroupQuantificationPass(monitor);
      stopFirstPass();
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting first_pass element not %1")
          .arg(m_expectedString));
    }

  getExpectedString();

  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }

  if(m_expectedString == "second_pass")
    {
      startSecondPass();
      readGroupQuantificationPass(monitor);
      stopSecondPass();
    }
  else
    {
      m_cborReader.next();
    }

  getExpectedString();
  if(m_expectedString.startsWith("timestamp"))
    {
      m_cborReader.next();
      getExpectedString();
    }
  qDebug();
  // skipCurrentElement();
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
  stopGroup();
}

void
CborStreamReaderBase::readGroupQuantificationPass(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();

  std::size_t size = m_cborReader.length();
  std::size_t i    = 0;
  m_cborReader.enterContainer();
  while(getExpectedString())
    {
      m_msrunId = m_expectedString;
      readQrDataBlock(monitor);
      i++;
      // m_cborReader.next();
    }

  if(i < size)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 msruns out of %2 expected, "
                    "groupid %3")
          .arg(i)
          .arg(size)
          .arg(m_groupId));
    }
  qDebug();
  // skipCurrentElement();
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
  qDebug();
}


void
CborStreamReaderBase::readQrDataBlock(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();
  getExpectedString();
  if(m_expectedString == "msrun")
    {
      readMsrun(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting msrun element not %1")
          .arg(m_expectedString));
    }
  // retention_time_correction
  getExpectedString();
  if(m_expectedString == "retention_time_correction")
    {
      m_cborReader.next();
      getExpectedString();
    }
  if(m_expectedString == "peptide_measurements")
    {
      readPeptideMeasurements(monitor);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: expecting peptide_measurements element not %1")
          .arg(m_expectedString));
    }
  qDebug();
  // skipCurrentElement();
  m_cborReader.leaveContainer();
}


void
CborStreamReaderBase::readMsrun(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer();
  m_msrunId = "";
  while(getExpectedString())
    {
      if(m_expectedString == "id")
        {
          getExpectedString();
          m_msrunId = m_expectedString;
        }

      else if(m_expectedString == "filename")
        {
          getExpectedString();
          m_msrunFileName = m_expectedString;
        }
      else if(m_expectedString == "sample")
        {
          getExpectedString();
          m_msrunSampleName = m_expectedString;
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in msrun")
              .arg(m_expectedString));
        }
    }
  reportMsRun();
  m_cborReader.leaveContainer();
}

void
CborStreamReaderBase::readPeptideMeasurements(
  pappso::UiMonitorInterface &monitor)
{
  qDebug() << m_cborReader.length();
  monitor.setTotalSteps(m_cborReader.length());


  if(!m_cborReader.isLengthKnown())
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR: peptide_measurements map size not known in peptideid %3")
          .arg(m_peptideId));
    }
  std::size_t size = m_cborReader.length();
  m_cborReader.enterContainer(); // map
  std::size_t i = 0;
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      m_peptideId = m_expectedString;
      readPeptideMeasurement(monitor);
      i++;
      // m_cborReader.next();
    }

  qDebug() << size << " " << i;
  if(i < size)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 peptide_measurements out of %2 expected, "
                    "peptideid %3")
          .arg(i)
          .arg(size)
          .arg(m_peptideId));
    }
  m_cborReader.leaveContainer();
  qDebug() << size;
}

void
CborStreamReaderBase::readPeptideMeasurement(
  pappso::UiMonitorInterface &monitor)
{
  qDebug();
  m_cborReader.enterContainer();

  m_rtTarget = 0;
  m_mods     = "";
  while(getExpectedString())
    {
      if(m_expectedString == "proforma")
        {
          getExpectedString();
          m_proForma = m_expectedString;
        }
      else if(m_expectedString == "mods")
        {
          getExpectedString();
          m_mods = m_expectedString;
        }
      else if(m_expectedString == "rt_target")
        {

          if(m_cborReader.isDouble())
            {
              m_rtTarget = m_cborReader.toDouble();
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for rt_target"));
            }
          m_cborReader.next();
        }
      else if(m_expectedString == "xics")
        {
          if(!m_cborReader.isLengthKnown())
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: xics array size not known in peptideid %3")
                  .arg(m_peptideId));
            }
          std::size_t size = m_cborReader.length();
          std::size_t i    = 0;
          m_cborReader.enterContainer(); // array


          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug();
              readXic(monitor);
              i++;
              // m_cborReader.next();

              qDebug();
            }
          if(i < size)
            {
              throw pappso::PappsoException(
                QObject::tr(
                  "ERROR: only %1 xics out of %2 expected, peptideid %3")
                  .arg(i)
                  .arg(size)
                  .arg(m_peptideId));
            }
          qDebug();
          m_cborReader.leaveContainer();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in PeptideMeasurement")
              .arg(m_expectedString));
        }
    }
  qDebug();
  m_cborReader.leaveContainer();
}

void
CborStreamReaderBase::readXic(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  // dumpOne(monitor, 0);
  if(!m_cborReader.enterContainer())
    {
      checkError("xic");
    }
  m_charge  = 0;
  m_mz      = 0;
  m_isotope = 0;
  m_rank    = 0;
  m_thRatio = 0;
  m_quality = "";
  m_label   = "";
  PeakStruct peak;
  while(getExpectedString())
    {
      if(m_expectedString == "charge")
        {
          if(m_cborReader.isInteger())
            {
              m_charge = m_cborReader.toInteger();
              qDebug() << "m_charge=" << m_charge;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for charge"));
            }

          m_cborReader.next();
        }
      else if(m_expectedString == "mz")
        {

          if(m_cborReader.isDouble())
            {
              m_mz = m_cborReader.toDouble();
              qDebug() << "m_mz=" << m_mz;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for mz"));
            }
          m_cborReader.next();
        }
      else if(m_expectedString == "xic_coord")
        {
          m_cborReader.next();
        }
      else if(m_expectedString == "isotope")
        {
          if(m_cborReader.isInteger())
            {
              m_isotope = m_cborReader.toInteger();
              qDebug() << "m_isotope=" << m_isotope;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for isotope"));
            }
          m_cborReader.next();
        }

      else if(m_expectedString == "rank")
        {
          if(m_cborReader.isInteger())
            {
              m_rank = m_cborReader.toInteger();
              qDebug() << "m_rank=" << m_rank;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting integer for rank"));
            }
          m_cborReader.next();
        }
      else if(m_expectedString == "th_ratio")
        {
          if(m_cborReader.isDouble())
            {
              m_thRatio = m_cborReader.toDouble();
              qDebug() << "th_ratio=" << m_thRatio;
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for m_thRatio"));
            }
          m_cborReader.next();
        }
      else if(m_expectedString == "quality")
        {

          getExpectedString();
          m_quality = m_expectedString;
        }
      else if(m_expectedString == "label")
        {

          getExpectedString();
          m_label = m_expectedString;
        }


      else if(m_expectedString == "peak")
        {
          peak = readPeak(monitor);
        }
      else if(m_expectedString == "trace")
        {
          readTrace();
        }
      else if(m_expectedString == "peak_shape")
        {
          readPeakShape();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in Xic")
              .arg(m_expectedString));
        }
    }
  reportPeakLine(peak);
  m_cborReader.leaveContainer();
}


CborStreamReaderBase::PeakStruct
CborStreamReaderBase::readPeak(pappso::UiMonitorInterface &monitor)
{
  qDebug();
  // dumpOne(monitor, 0);
  PeakStruct peak;
  m_cborReader.enterContainer();
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      if(m_expectedString == "area")
        {
          if(m_cborReader.isDouble())
            {
              peak.area = m_cborReader.toDouble();
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for area"));
            }
          m_cborReader.next();
        }
      else if(m_expectedString == "max_intensity")
        {
          // dumpOne(monitor, 0);
          if(m_cborReader.isDouble())
            {
              peak.max_intensity = m_cborReader.toDouble();
            }
          else
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: expecting double for max_intensity"));
            }
          m_cborReader.next();

          checkError("peak max_intensity");
        }

      else if(m_expectedString == "rt")
        {
          // dumpOne(monitor, 0);
          m_cborReader.enterContainer();
          std::size_t i = 0;

          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug() << i;
              // if(m_cborReader.isDouble())
              //{
              peak.rt[i] = m_cborReader.toDouble();
              qDebug() << peak.rt[i];
              m_cborReader.next();
              i++;
              //}
            }
          m_cborReader.leaveContainer();
        }
      else if(m_expectedString == "aligned_rt")
        {
          // dumpOne(monitor, 0);
          m_cborReader.enterContainer();
          std::size_t i = 0;

          while(!m_cborReader.lastError() && m_cborReader.hasNext())
            {
              qDebug() << i;
              // if(m_cborReader.isDouble())
              //{
              peak.aligned_rt[i] = m_cborReader.toDouble();
              qDebug() << peak.rt[i];
              m_cborReader.next();
              i++;
              //}
            }
          m_cborReader.leaveContainer();
        }
      else
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unexpected element %1 in peak")
              .arg(m_expectedString));
        }
    }
  m_cborReader.leaveContainer();
  qDebug();
  return peak;
}

void
CborStreamReaderBase::reportPeakLine(
  const CborStreamReaderBase::PeakStruct &peak)
{
}

void
CborStreamReaderBase::readTrace()
{
  m_cborReader.next();
}
void
CborStreamReaderBase::readPeakShape()
{
  m_cborReader.next();
}

void
CborStreamReaderBase::readPappsoTrace(pappso::Trace &trace)
{
  if(!m_cborReader.enterContainer())
    {
      checkError("trace");
    }
  getExpectedString();
  std::vector<double> x, y;
  if(m_expectedString == "x")
    {
      readVector(x);
    }

  getExpectedString();
  if(m_expectedString == "y")
    {
      readVector(y);
    }
  trace.initialize(x, y);
  m_cborReader.leaveContainer();
}

void
CborStreamReaderBase::readVector(std::vector<double> &vector)
{

  m_cborReader.enterContainer();

  while(!m_cborReader.lastError() && m_cborReader.hasNext())
    {
      vector.push_back(m_cborReader.toDouble());
      m_cborReader.next();
      //}
    }
  m_cborReader.leaveContainer();
}

void
CborStreamReaderBase::readProjectParameters()
{
  m_cborReader.next();
}

void
CborStreamReaderBase::readActions(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.next();
}

void
CborStreamReaderBase::readIdentificationData(
  pappso::UiMonitorInterface &monitor)
{
  m_cborReader.next();
}

void
CborStreamReaderBase::readMasschroqMethods(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.next();
}

void
CborStreamReaderBase::readEnd(pappso::UiMonitorInterface &monitor)
{
  m_cborReader.next();
}

void
CborStreamReaderBase::reportMsRun()
{
}


void
CborStreamReaderBase::startFirstPass()
{
}

void
CborStreamReaderBase::startSecondPass()
{
}

void
CborStreamReaderBase::stopFirstPass()
{
}

void
CborStreamReaderBase::stopSecondPass()
{
}

void
CborStreamReaderBase::stopGroup()
{
}
