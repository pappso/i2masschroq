/**
 * \file mcql/input/cborstreamreader2json.cpp
 * \date 08/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to produce JSON readable document
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "cborstreamreader2json.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <QJsonArray>

mcql::CborStreamReader2Json::CborStreamReader2Json()
{
}

mcql::CborStreamReader2Json::~CborStreamReader2Json()
{
}

void
mcql::CborStreamReader2Json::readActions(pappso::UiMonitorInterface &monitor)
{
  writeCborObject("actions", QCborValue::fromCbor(m_cborReader));
}

void
mcql::CborStreamReader2Json::readIdentificationData(
  pappso::UiMonitorInterface &monitor)
{
  writeCborObject("identification_data", QCborValue::fromCbor(m_cborReader));
}

void
mcql::CborStreamReader2Json::readInformations(
  pappso::UiMonitorInterface &monitor)
{
  writeCborObject("informations", QCborValue::fromCbor(m_cborReader));
}

void
mcql::CborStreamReader2Json::readMasschroqMethods(
  pappso::UiMonitorInterface &monitor)
{
  writeCborObject("masschroq_methods", QCborValue::fromCbor(m_cborReader));
}

void
mcql::CborStreamReader2Json::readProjectParameters()
{
  writeCborObject("project_parameters", QCborValue::fromCbor(m_cborReader));
}

void
mcql::CborStreamReader2Json::readEnd(pappso::UiMonitorInterface &monitor)
{
  writeCborObject("end", QCborValue::fromCbor(m_cborReader));
}


void
mcql::CborStreamReader2Json::readQuantificationData(
  pappso::UiMonitorInterface &monitor)
{
  m_cborReader.enterContainer(); // array
  qWarning();
  QJsonArray qdata_array;
  while(!m_cborReader.lastError() && m_cborReader.hasNext())
    {
      m_cborReader.enterContainer(); // map
      QJsonObject qdata;

      while(getExpectedString())
        {
          qWarning() << m_expectedString;
          if(m_expectedString == "first_pass" ||
             m_expectedString == "second_pass")
            {
              m_cborReader.next();
            }
          else if(!m_expectedString.isEmpty())
            {
              QJsonValue js_value =
                QCborValue::fromCbor(m_cborReader).toJsonValue();
              qdata.insert(m_expectedString, js_value);
              qWarning() << js_value.toString();
            }
          // m_cborReader.next();
        }
      qdata_array.append(qdata);
      m_cborReader.leaveContainer();
    }

  m_cborReader.leaveContainer();
  m_jsonRoot.insert("quantification_data", qdata_array);
}


void
mcql::CborStreamReader2Json::writeCborObject(const QString &name,
                                             const QCborValue &cbor_value)
{
  QJsonValue json_value;
  m_jsonRoot.insert(name, cbor_value.toJsonValue());
}

const QJsonObject &
mcql::CborStreamReader2Json::getJsonObject() const
{
  return m_jsonRoot;
}
