/**
 * \file mcql/input/cborstreamreaderdebugquantificationdata.cpp
 * \date 17/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to extract given quantified petpides
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "cborstreamreaderdebugquantificationdata.h"

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>

mcql::CborStreamReaderDebugQuantificationData::
  CborStreamReaderDebugQuantificationData()
{
}

mcql::CborStreamReaderDebugQuantificationData::
  ~CborStreamReaderDebugQuantificationData()
{
}

void
mcql::CborStreamReaderDebugQuantificationData::setFindPeptideIdList(
  const QStringList &peptide_id)
{
  m_findPeptideIdList = peptide_id;
}

const QJsonObject &
mcql::CborStreamReaderDebugQuantificationData::getJsonObject()
{
  return m_jsonRoot;
}


void
mcql::CborStreamReaderDebugQuantificationData::readPeptideMeasurements(
  pappso::UiMonitorInterface &monitor)
{

  qDebug() << m_cborReader.length();
  monitor.setTotalSteps(m_cborReader.length());


  if(!m_cborReader.isLengthKnown())
    {
      throw pappso::PappsoException(
        QObject::tr(
          "ERROR: peptide_measurements map size not known in peptideid %3")
          .arg(m_peptideId));
    }
  std::size_t size = m_cborReader.length();
  m_cborReader.enterContainer(); // map
  std::size_t i = 0;
  while(getExpectedString())
    {
      qDebug() << m_expectedString;
      m_peptideId = m_expectedString;

      if(m_findPeptideIdList.contains(m_peptideId))
        {
          QCborValue cbor_value   = QCborValue::fromCbor(m_cborReader);
          QJsonObject json_object = cbor_value.toJsonValue().toObject();
          json_object.insert("msrun", m_msrunId);
          json_object.insert("group", m_groupId);
          m_jsonPeptideArray.append(json_object);
        }
      else
        {
          m_cborReader.next();
        }
      i++;
      // m_cborReader.next();
    }

  qDebug() << size << " " << i;
  if(i < size)
    {
      throw pappso::PappsoException(
        QObject::tr("ERROR: only %1 peptide_measurements out of %2 expected, "
                    "peptideid %3")
          .arg(i)
          .arg(size)
          .arg(m_peptideId));
    }
  m_cborReader.leaveContainer();
  qDebug() << size;
}

void
mcql::CborStreamReaderDebugQuantificationData::stopFirstPass()
{
  m_jsonRoot.insert("found_list_first_pass", m_jsonPeptideArray);
  m_jsonPeptideArray = QJsonArray();
}

void
mcql::CborStreamReaderDebugQuantificationData::stopSecondPass()
{
  m_jsonRoot.insert("found_list_second_pass", m_jsonPeptideArray);
  m_jsonPeptideArray = QJsonArray();
}
