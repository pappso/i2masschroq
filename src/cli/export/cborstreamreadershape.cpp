/**
 * \file src/cli/export/cborstreamreadershape.h
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief read cbor stream for shape output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "cborstreamreadershape.h"
#include <odsstream/odsexception.h>
#include <pappsomspp/pappsoexception.h>
#include <QDebug>

CborStreamReaderShape::CborStreamReaderShape(const QString &output_directory)
{
  qDebug();
  try
    {
      mpa_calcWriterInterface = new TsvDirectoryWriter(QDir(output_directory));
      mpa_calcWriterInterface->setFlushLines(true);
    }
  catch(OdsException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error writing TSV ouput in directory %1 :\n%2")
          .arg(output_directory)
          .arg(error.qwhat()));
    }

  qDebug();

  mpa_calcWriterInterface->writeSheet("peaks");
  qDebug();
  mpa_calcWriterInterface->writeCell("peak_index");
  mpa_calcWriterInterface->writeCell("rtx");
  mpa_calcWriterInterface->writeCell("rtxalign");
  mpa_calcWriterInterface->writeCell("y");
  mpa_calcWriterInterface->writeLine();
  qDebug();
}

CborStreamReaderShape::~CborStreamReaderShape()
{
}


void
CborStreamReaderShape::reportMsRunRetentionTime(
  const pappso::MsRunRetentionTime<QString> &rt_line)
{
  m_msrun2rt.insert({m_msrunId, rt_line});
}

void
CborStreamReaderShape::reportPeakLine(
  const CborStreamReaderBase::PeakStruct &peak)
{
  qDebug();
  m_peakIndex++;
}

void
CborStreamReaderShape::reportMsRun()
{
  auto it                 = m_msrun2rt.find(m_msrunId);
  m_msrunRetentionTimePtr = nullptr;
  if(it != m_msrun2rt.end())
    {
      m_msrunRetentionTimePtr = &it->second;
    }
}


void
CborStreamReaderShape::readPeakShape()
{
  qDebug();
  /*
   *
                  "peak_shape": {
                      "trace": {
                          "x": [851.78, 852.78, 853.78, 854.78],
                          "y": [851.78, 852.78, 853.78, 854.78]
                      }
                  },
                  */
  if(!m_cborReader.enterContainer())
    {
      checkError("peak_shape");
    }
  qDebug();
  getExpectedString();


  if(m_expectedString == "trace")
    {
      pappso::Trace trace;
      readPappsoTrace(trace);

      for(auto &datapoint : trace)
        {
          mpa_calcWriterInterface->writeCell(m_peakIndex);
          mpa_calcWriterInterface->writeCell(datapoint.x);
          if((m_msrunRetentionTimePtr == nullptr) ||
             (!m_msrunRetentionTimePtr->isAligned()))
            {
              mpa_calcWriterInterface->writeCell(datapoint.x); // aligned
            }
          else
            {
              mpa_calcWriterInterface->writeCell(
                m_msrunRetentionTimePtr->translateOriginal2AlignedRetentionTime(
                  datapoint.x)); // aligned
            }
          mpa_calcWriterInterface->writeCell(datapoint.y);
          mpa_calcWriterInterface->writeLine();
        }
    }


  m_cborReader.leaveContainer();
  qDebug();
}
