/**
 * \file mcql/input/cborstreamreadertsv.h
 * \date 04/01/2025
 * \author Olivier Langella
 * \brief read cbor stream for tsv output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>
#include <odsstream/tsvdirectorywriter.h>
#include <QStringList>
#include <QCborStreamReader>
#include <QStack>
#include <QFile>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <pappsomspp/processing/project/projectparameters.h>
#include <pappsomspp/masschroq/quantificationmethod.h>
#include <pappsomspp/masschroq/alignmentmethod.h>
#include "cborstreamreaderbase.h"


namespace mcql
{

class CborStreamReaderTsvBase : public CborStreamReaderBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderTsvBase();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderTsvBase();


  /** @brief do not report missed peaks in output files
   * mimic the MassChroQ leagacy behaviour that does not report missed peaks
   * (not quantified peaks)
   */
  virtual void setNoMissedPeak(bool no_missed_peak);
  virtual const std::vector<QString> &getQuantifiedGroupIdList() const;


  protected:
  void dumpOne(pappso::UiMonitorInterface &monitor, int nestingLevel);
  virtual void readInformations(pappso::UiMonitorInterface &monitor) override;
  virtual void
  readMasschroqMethods(pappso::UiMonitorInterface &monitor) override;
  void writeQuantificationMethod(
    pappso::masschroq::QuantificationMethod *p_quantification_method);
  void
  writeAlignmentMethod(pappso::masschroq::AlignmentMethod *p_alignment_method);
  virtual void
  readIdentificationData(pappso::UiMonitorInterface &monitor) override;
  void writePeptideProteinList();
  void skipCurrentElement();

  void writeQuantificationSheet();
  virtual void reportPeakLine(const PeakStruct &peak) override;
  virtual void readProjectParameters() override;
  virtual void startGroup() override;

  protected:
  CalcWriterInterface *mpa_calcWriterInterface = nullptr;
  QString m_currentSheetName;


  private:
  QStack<quint8> m_byteArrayEncoding;
  qint64 m_offset = 0;

  std::map<QString, QString> m_proteinMap;
  double m_niMinimumAbundance = 0;
  pappso::ProjectParameters m_projectParameters;

  bool m_isMissedPeaks = true;
  std::vector<QString> m_quantifiedGroupIdList;
};

/**
 * @todo write docs
 */
class CborStreamReaderOds : public CborStreamReaderTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderOds(const QString &ods_file);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderOds();
};

/**
 * @todo write docs
 */
class CborStreamReaderTsv : public CborStreamReaderTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderTsv(const QString &output_directory);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderTsv();

  private:
  TsvDirectoryWriter *mpa_tsv;
};
} // namespace mcql
