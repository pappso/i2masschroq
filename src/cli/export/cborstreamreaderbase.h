/**
 * \file src/cli/export/cborstreamreaderbase.h
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief read cbor stream from masschroq3
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <pappsomspp/processing/cbor/cborstreamreaderinterface.h>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>

/**
 * @todo write docs
 */
class CborStreamReaderBase : public pappso::CborStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderBase();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderBase();


  virtual void readCbor(QFile *cborp, pappso::UiMonitorInterface &monitor);

  protected:
  struct PeakStruct
  {
    double area          = 0;
    double max_intensity = 0;
    double rt[3]         = {0};
    double aligned_rt[3] = {0};
  };

  void readRoot(pappso::UiMonitorInterface &monitor);
  virtual void readEnd(pappso::UiMonitorInterface &monitor);
  virtual void readInformations(pappso::UiMonitorInterface &monitor);
  virtual void readProjectParameters();
  virtual void readActions(pappso::UiMonitorInterface &monitor);
  virtual void readIdentificationData(pappso::UiMonitorInterface &monitor);
  virtual void readMasschroqMethods(pappso::UiMonitorInterface &monitor);
  virtual void readQuantificationData(pappso::UiMonitorInterface &monitor);
  virtual void readAlignmentData(pappso::UiMonitorInterface &monitor);
  virtual void
  readQuantificationDataObject(pappso::UiMonitorInterface &monitor);
  virtual void readGroupQuantificationPass(pappso::UiMonitorInterface &monitor);
  virtual void readQrDataBlock(pappso::UiMonitorInterface &monitor);
  virtual void readMsrun(pappso::UiMonitorInterface &monitor);
  virtual void readPeptideMeasurements(pappso::UiMonitorInterface &monitor);
  virtual void readPeptideMeasurement(pappso::UiMonitorInterface &monitor);
  virtual void readXic(pappso::UiMonitorInterface &monitor);
  virtual void readTrace();
  virtual void readPeakShape();
  virtual void readAlignment();


  virtual void startGroup();
  virtual void stopGroup();
  virtual void startFirstPass();
  virtual void startSecondPass();
  virtual void stopFirstPass();
  virtual void stopSecondPass();


  PeakStruct readPeak(pappso::UiMonitorInterface &monitor);
  void readPappsoTrace(pappso::Trace &trace);
  void readVector(std::vector<double> &vector);

  void checkError(const QString &element_name);


  virtual void reportPeakLine(const PeakStruct &peak);
  virtual void
  reportMsRunRetentionTime(const pappso::MsRunRetentionTime<QString> &rt_line);
  virtual void reportMsRun();

  protected:
  QString m_alignmentId;
  QString m_quantificationId;
  QString m_groupId;
  QString m_msrunId;
  QString m_peptideId;
  QString m_msrunFileName;
  QString m_msrunSampleName;
  QString m_proForma;
  QString m_label;
  QString m_mods;
  QString m_msrunReference;
  std::size_t m_charge;
  std::size_t m_isotope;
  std::size_t m_rank;
  double m_mz;
  double m_rtTarget;
  double m_thRatio;
  QString m_quality;
};
