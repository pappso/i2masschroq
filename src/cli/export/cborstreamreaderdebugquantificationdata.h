/**
 * \file mcql/input/cborstreamreaderdebugquantificationdata.h
 * \date 17/01/2025
 * \author Olivier Langella
 * \brief read cbor stream to extract given quantified petpides
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QCborStreamReader>
#include <QJsonObject>
#include <QJsonArray>
#include <QStack>
#include <QFile>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include "cborstreamreaderbase.h"

namespace mcql
{
/**
 * @todo write docs
 */
class CborStreamReaderDebugQuantificationData : public CborStreamReaderBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderDebugQuantificationData();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderDebugQuantificationData();

  void setFindPeptideIdList(const QStringList &peptide_id);


  const QJsonObject &getJsonObject();


  protected:
  virtual void
  readPeptideMeasurements(pappso::UiMonitorInterface &monitor) override;

  virtual void stopFirstPass() override;
  virtual void stopSecondPass() override;

  private:
  QJsonObject m_jsonRoot;

  QJsonArray m_jsonPeptideArray;

  QStringList m_findPeptideIdList;
};
} // namespace mcql
