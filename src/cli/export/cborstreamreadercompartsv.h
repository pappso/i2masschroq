/**
 * \file mcql/input/cborstreamreadercompartsv.h
 * \date 17/02/2025
 * \author Olivier Langella
 * \brief read cbor stream for tsv compar output
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>
#include <odsstream/tsvdirectorywriter.h>
#include "cborstreamreaderbase.h"
#include <QJsonObject>


class CborStreamReaderComparTsvBase : public CborStreamReaderBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderComparTsvBase();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderComparTsvBase();


  protected:
  virtual void readActions(pappso::UiMonitorInterface &monitor) override;
  virtual void
  readIdentificationData(pappso::UiMonitorInterface &monitor) override;
  virtual void startGroup() override;
  virtual void stopGroup() override;
  virtual void reportPeakLine(const PeakStruct &peak) override;
  virtual void stopFirstPass() override;
  void writeHeader();

  protected:
  struct ComparLine
  {
    QString peptide_id;
    double mz;
    double rt;
    int isotope;
    int rank;
    int charge;
    double isotope_th_ratio;
    QString sequence;
    QString label;
    QString mods;
    std::vector<double> msrunAreaList;
  };

  CalcWriterInterface *mpa_calcWriterInterface = nullptr;
  QString m_currentSheetName;


  private:
  std::map<QString, ComparLine> m_lineId2msrunAreaMap;
  std::map<QString, QString> m_peptideId2protein;
  std::vector<QString> m_msrunList;
  std::map<QString, std::size_t> m_msrunColumn;
  QJsonObject m_jgroupList;
};

/**
 * @todo write docs
 */
class CborStreamReaderComparOds : public CborStreamReaderComparTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderComparOds(const QString &ods_file);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderComparOds();
};

/**
 * @todo write docs
 */
class CborStreamReaderComparTsv : public CborStreamReaderComparTsvBase
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderComparTsv(const QString &output_directory);

  /**
   * Destructor
   */
  virtual ~CborStreamReaderComparTsv();

  private:
  TsvDirectoryWriter *mpa_tsv;
};
