#https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
[Desktop Entry]
Version=1.0
Name=i2masschroq
Categories=Science;Biology;Education;Qt;
Comment=Protein inference tool
Exec=${CMAKE_INSTALL_PREFIX}/bin/i2masschroq %f
Icon=${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/scalable/apps/i2MassChroQ.svg
Terminal=false
Type=Application
StartupNotify=true
Keywords=Mass spectrometry;Protein inference;Peptide;
MimeType=application/x-i2masschroq;
