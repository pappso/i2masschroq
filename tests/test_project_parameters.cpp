/**
 * \file tests/test_project_parameters.cpp
 * \date 20/03/2024
 * \author Olivier Langella
 * \brief test project parameters
 */


/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [parameters] -s

#include <QDebug>
#include <QString>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <pappsomspp/pappsoexception.h>
#include "config.h"
#include "../src/input/mzidentml/msrunlocationreader.h"

TEST_CASE("test parameters", "[parameters]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: parameters ::..", "[parameters]")
  {


#ifdef USEPAPPSOTREE
    MsRunLocationReader msrun_location_reader;

    REQUIRE(msrun_location_reader.readFile(
              "/gorgone/pappso/moulon/users/Olivier/20240131_proteobench/"
              "tandem/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_01.xml.mzid") ==
            true);

    REQUIRE(msrun_location_reader.getSpectraDataLocation().toStdString() ==
            "/gorgone/pappso/moulon/raw/20240131_proteobench/"
            "LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_01.mzML");
    ProjectParameters parameters = msrun_location_reader.getProjectParameters();
    REQUIRE(parameters.size() == 1);

    REQUIRE(
      parameters
        .getValue(ProjectParamCategory::identification, "xtandem_preset_file")
        .toString()
        .toStdString() ==
      "/gorgone/pappso/tmp/i2masschroq.bnhinH/QExactive_analysis_FDR.xml");
    ProjectParameters parameters_main;
    parameters_main.merge(msrun_location_reader.getProjectParameters());

    REQUIRE(
      parameters_main
        .getValue(ProjectParamCategory::identification, "xtandem_preset_file")
        .toString()
        .toStdString() ==
      "/gorgone/pappso/tmp/i2masschroq.bnhinH/QExactive_analysis_FDR.xml");
#endif
  }
}
