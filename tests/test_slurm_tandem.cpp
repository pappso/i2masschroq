/**
 * \file tests/test_slurm_tandem.cpp
 * \date 19/06/2024
 * \author Olivier Langella
 * \brief test sbatch with slurm for tandem job
 */


/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [slurm] -s

#include <QDebug>
#include <QString>
#include <catch2/catch_test_macros.hpp>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include "config.h"
#include <cutelee/engine.h>


TEST_CASE("slurm input test suite", "[slurm]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: slurm parser init ::..", "[slurm]")
  {

    Cutelee::Engine *engine_p = new Cutelee::Engine();
    auto loader = std::shared_ptr<Cutelee::FileSystemTemplateLoader>(
      new Cutelee::FileSystemTemplateLoader());
    loader->setTemplateDirs(
      {QString("%1/src/resources/templates/slurm").arg(CMAKE_SOURCE_DIR)});
    engine_p->addTemplateLoader(loader);
    Cutelee::Template slurm_tandem_template =
      engine_p->loadByName("slurm_tandemng.txt");
    Cutelee::Context tandem_context;
    tandem_context.insert("job_name", "tandemng");
    tandem_context.insert("request_cpus", 4);
    tandem_context.insert("count_tandem_job", 2);
    tandem_context.insert("tandem_bin_path", "/usr/bin/tandemng");


    QVariantList tandemInputList;
    tandemInputList.append("task1");
    tandemInputList.append("task2");

    tandem_context.insert("tandem_input_list", tandemInputList);
    QString slurm_batch = slurm_tandem_template->render(&tandem_context);
    qDebug() << slurm_batch;
    REQUIRE(slurm_batch.startsWith("#!/bin/bash"));
  }
}
