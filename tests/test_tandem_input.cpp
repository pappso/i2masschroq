/**
 * \file tests/test_tandem_input.cpp
 * \date 5/12/2021
 * \author Olivier Langella
 * \brief test tandem XML parser
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [tandem] -s
// ./tests/catch2-only-tests [mzidentml] -s

#include <QDebug>
#include <QString>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include "../src/input/mzidentml/msrunlocationreader.h"
#include "../src/input/tandem/tandeminfoparser.h"
#include "../src/input/tandem/tandemparamparser.h"
#include "config.h"
#include "../src/utils/utils.h"


TEST_CASE("mzidentml input test suite", "[mzidentml]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: mzIdentML parser init ::..", "[mzidentml]")
  {
    MsRunLocationReader *parser = new MsRunLocationReader();

    REQUIRE(parser->readFile(QString("%1/tests/data/input/deepprot_test.mzid")
                               .arg(CMAKE_SOURCE_DIR)) == true);
    REQUIRE(parser->errorString().toStdString() == "");

    REQUIRE(parser->getSpectraDataList().front().location ==
            "../tests/data/"
            "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59678.mgf");
  }
}

TEST_CASE("tandem input test suite", "[tandem]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  SECTION("..:: X!Tandem preset parser ::..", "[tandem]")
  {

    TandemParamParser tandem_param_parser;

    bool read_ok = tandem_param_parser.readFile(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/input/timstof_preset.xml"));
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("output, histogram column width")
              .toStdString() == "30");

    REQUIRE(tandem_param_parser.getParamFileType() == ParamFileType::preset);
    REQUIRE(tandem_param_parser.getMethodName().toStdString() == "");
    //"refine, potential modification mass">15.99491@M
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("refine, potential modification mass")
              .toStdString() == "15.99491@M");
  }


  SECTION("..:: X!Tandem parse version ::..", "[tandem]")
  {

    QString version = Utils::checkXtandemVersion("/usr/bin/tandemng");
    REQUIRE(version.toStdString() == "X!Tandem Alanine 2017.2.1.4");

    version = Utils::checkXtandemVersion("/usr/bin/tandemng2015");
    REQUIRE(version.toStdString() == "X!Tandem Vengeance 2015.12.15.2");

    version = Utils::checkXtandemVersion("/usr/bin/tandemng2015p");
    REQUIRE(version.toStdString() == "X!Tandem Piledriver 2015.04.01.1");
  }
}
