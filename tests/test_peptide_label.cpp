/**
 * \file tests/test_peptide_label.cpp
 * \date 26/09/2023
 * \author Olivier Langella
 * \brief test peptide label
 */


/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [peptidelabel] -s

#include <QDebug>
#include <QString>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include "config.h"
#include <pappsomspp/peptide/peptidestrparser.h>
#include "../src/core/peptidextp.h"

TEST_CASE("test peptidelabel", "[peptidelabel]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: peptidelabel ::..", "[peptidelabel]")
  {
    pappso::PeptideSp peptide_sp = pappso::PeptideStrParser::parseString(
      "F(MOD:00589)N(C13N15:N)I(C13N15:I)D(C13N15:D)A(C13N15:A)D(C13N15:D)K("
      "MOD:00582)V(C13N15:V)N(C13N15:N)P(C13N15:P)R(MOD:00587)");

    REQUIRE(peptide_sp.get()->toString().toStdString() ==
            "F(MOD:00589)N(C13N15:N)I(C13N15:I)D(C13N15:D)A(C13N15:A)D(C13N15:"
            "D)K(MOD:00582)V(C13N15:V)N(C13N15:N)P(C13N15:P)R(MOD:00587)");


    PeptideXtp peptide_xtp("FNIDADKVNPR");

    pappso::AaModificationP modif =
      pappso::AaModification::getInstance("MOD:00589");
    peptide_xtp.addAaModification(modif, 0);
    modif = pappso::AaModification::getInstance("C13N15:N");
    peptide_xtp.addAaModification(modif, 1);

    REQUIRE(peptide_xtp.toString().toStdString() ==
            "F(MOD:00589)N(C13N15:N)IDADKVNPR");

    LabelingMethodSp method =
      LabelingMethod("Full_C13N15").makeLabelingMethodSp();

    peptide_xtp.applyLabelingMethod(method);

    REQUIRE(peptide_xtp.toString().toStdString() ==
            "F(MOD:00589)N(C13N15:N)IDADKVNPR");
    REQUIRE(peptide_xtp.getNativePeptideP()->toString().toStdString() ==
            "FNIDADKVNPR");
    // MOD:00589
    //    <mod ref="moda7" position="0" aa="F"/>
    //     <mod ref="modb19" position="1" aa="N"/>
    //     <mod ref="modb15" position="2" aa="I"/>
    //     <mod ref="moda4" position="3" aa="D"/>
    //     <mod ref="moda2" position="4" aa="A"/>
    //     <mod ref="moda4" position="5" aa="D"/>
    //     <mod ref="modb17" position="6" aa="K"/>
    //     <mod ref="modb25" position="7" aa="V"/>
    //     <mod ref="modb19" position="8" aa="N"/>
    //     <mod ref="modb20" position="9" aa="P"/>
    //     <mod ref="modb23" position="10" aa="R"/>
  }
  SECTION("..:: check mod accession ::..", "[peptidelabel]")
  {
    // C13N15:Q
    pappso::AaModificationP modif;
    modif = pappso::AaModification::getInstance("C13N15:Q");

    REQUIRE(modif->getMass() == Catch::Approx(7.0108439758));
    // MOD:01160
    modif = pappso::AaModification::getInstance("MOD:01160");

    REQUIRE(modif->getMass() == Catch::Approx(-17.0265491015));
  }
}
