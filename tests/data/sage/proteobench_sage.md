


./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_01 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_01.mzML


./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_02 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_02.mzML


./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_03 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_03.mzML

./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_01 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_01.mzML

./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_02 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_02.mzML

./target/release/sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /home/langella/developpement/git/i2masschroq/tests/data/sage/config_qex.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_03 /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_03.mzML

# query all samples together

config_qex.json search for acetylated Nter proteins, pyro-glutamine/pyro-glutamic acid on Nter peptides

sage -f /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/database/ProteoBenchFASTA_DDAQuantification.fasta /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/all_samples_acetyl/all_samples_acetyl.json -o /gorgone/pappso/moulon/users/Olivier/20240131_proteobench/sage/all_samples_acetyl /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_01.mzML /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_02.mzML /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_A_Sample_Alpha_03.mzML /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_01.mzML /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_02.mzML /gorgone/pappso/moulon/raw/20240131_proteobench/LFQ_Orbitrap_DDA_Condition_B_Sample_Alpha_03.mzML
