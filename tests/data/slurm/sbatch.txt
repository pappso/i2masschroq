#!/bin/bash
# Options SBATCH :

#SBATCH --job-name=tandemng # Nom du Job
#SBATCH --cpus-per-task=2       # Allocation de N CPUs par tandem
#SBATCH --ntasks=2              # Nombre de jobs tandem
#SBATCH --ntasks-per-node=2              # nombre max de taches par noeuds
# #SBATCH --mincpus=4              # nombre minimum de cpu par noeuds
#SBATCH --mail-type=ALL         # Notification par email de la
# #SBATCH --nodes=1         # nombre minimum de noeuds
#SBATCH --chdir=/tmp/i2masschroq.rbegVV         # répertoire de travail
# #SBATCH --exclusive         # partage les noeuds
# #SBATCH --mem=5GB
# Traitement

#module purge
#module load hpcw
#module load parallel

# N jobs tandem en parallèle


# n1 : number of tasks
# N1 : number of nodes
srun --mpi=none --mem 2000 --cpus-per-task=2 --ntasks=1 --nodes=1 /usr/bin/tandemng /tmp/i2masschroq.rbegVV/tandem.FUVarD 2>/tmp/i2masschroq.rbegVV/tandem.FUVarD.err 1>/tmp/i2masschroq.rbegVV/tandem.FUVarD.out &

# n1 : number of tasks
# N1 : number of nodes
srun --mpi=none --mem 2000 --cpus-per-task=2 --ntasks=1 --nodes=1 /usr/bin/tandemng /tmp/i2masschroq.rbegVV/tandem.frEUNp 2>/tmp/i2masschroq.rbegVV/tandem.frEUNp.err 1>/tmp/i2masschroq.rbegVV/tandem.frEUNp.out &


wait

