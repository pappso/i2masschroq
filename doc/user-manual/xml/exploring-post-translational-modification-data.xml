<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "pappso-user-manuals.ent">
%entities;

<!ENTITY % i2masschroq-entities SYSTEM "i2masschroq-entities.ent">
%i2masschroq-entities;


<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">

<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN" "/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>

<chapter xmlns="http://docbook.org/ns/docbook"
xmlns:xlink="http://www.w3.org/1999/xlink"
xml:id="chap_exploring-post-translation-modification-identification_data"
version="5.0">

<info>

  <title>Exploring post-translational modification data</title>

  <keywordset>

    <keyword>identifications</keyword>
    <keyword>post-translation modification</keyword>
    <keyword>PTM</keyword>
    <keyword>peptide vs mass spectrum match</keyword>
    <keyword>PSM</keyword>
    <keyword>data exploration</keyword>

  </keywordset>

</info>

<para>

  This chapter describes in detail all the steps that the user accomplishes in
  their post-translational modification (PTM) data exploration session. 
</para>


<sect1 xml:id="sect_phospho-identification-run-settings">

  <title>Setting the &xtandem; Run Presets for Phospho-proteomics</title>

  <para>

    The very first step in starting a phospho-proteomics-based protein
    identification run is to configure the run so that the database search
    engine can model phopshorylated peptides on the basis of the sequence of the
    peptides in the database. That configuration step is started as described in
    <xref linkend="fig_xtpcpp-phospho-identification-run-settings" />.

  </para>


  <figure xml:id="fig_xtpcpp-phospho-identification-run-settings">

    <title>&xtandem; settings window for a phospho-proteomics project</title>

    <mediaobject>

      <imageobject role="fo">
        <imagedata fileref="print-xtpcpp-phospho-identification-run-settings.png"/>
      </imageobject>
      <imageobject role="html">
        <imagedata fileref="web-xtpcpp-phospho-identification-run-settings.png"/>
      </imageobject>

      <caption> 

        <para> The <guilabel>Choose presets</guilabel> option in this window
        allows the user to select an <application>X!Tandem</application> presets
        file to suit the protein identification run. In this example, the chosen
        presets file contains a number of configuration bits specific for a
        phospho-proteomics project.

      </para> 

    </caption>

  </mediaobject>

</figure>


<para>

  The configuration of &xtandem; needs to be performed by using the presets
  method, described in <xref linkend="sect_xtandem-parameter-presets"/> and
  following sections.  The <guibutton>Edit</guibutton> button next to the drop
  down list allows one to edit the presets that configure the handling of the
  database by the <application>X!Tandem</application> database search engine.
  The window that opens up upon clicking onto that button has two tabs that
  require the user's attention, as shown in <xref
  linkend="fig_xtpcpp-phospho-presets-protein-tab" /> and <xref
  linkend="fig_xtpcpp-phospho-presets-residue-tab" />.

</para>

<para>

  As described in <xref
  linkend="sect_loading_existing_xtandem_preset_configurations" />, it is
  possible to load existing presets in case these were defined already and might
  be reused for a repeated data exploration session on the same data set.

</para>


<figure xml:id="fig_xtpcpp-phospho-presets-protein-tab">

  <title>Setting the project to be a phospho-proteomics project</title>

  <mediaobject>

    <imageobject role="fo">
      <imagedata fileref="print-xtpcpp-phospho-presets-protein-tab.png"/>
    </imageobject>
    <imageobject role="html">
      <imagedata fileref="web-xtpcpp-phospho-presets-protein-tab.png"/>
    </imageobject>

    <caption> 

      <para>

        The major bit that need the user's attention in the <guilabel>Preset
        editor</guilabel> window's <guilabel>Protein</guilabel> tab is the
        <guilabel>Interpretation of peptide phosphorylation model</guilabel> to
        <guilabel>yes</guilabel>. The question mark icon on the side of that
        configuration option displays explanatory text on the right hand side of
        the window.

      </para> 

    </caption>

  </mediaobject>

</figure>

<figure xml:id="fig_xtpcpp-phospho-presets-residue-tab">

  <title>Configuring the phosphorylated residues</title>

  <mediaobject>

    <imageobject role="fo">
      <imagedata fileref="print-xtpcpp-phospho-presets-residue-tab.png"/>
    </imageobject>
    <imageobject role="html">
      <imagedata fileref="web-xtpcpp-phospho-presets-residue-tab.png"/>
    </imageobject>

    <caption> 

      <para>

        The phosphorylation events are not <guilabel>Fixed
        modifications</guilabel> because it is not possible to predict that they
        will occur systematically. This is the reason why the phosphorylation
        events are configured as <guilabel>Potential modifications</guilabel>.

      </para> 

    </caption>

  </mediaobject>

</figure>


<para>

  <xref linkend="fig_xtpcpp-phospho-presets-residue-tab" /> shows how to
  configure the potential phosphorylation of selected residues (tyrosine,
  threonine and serine).  Setting the &xtandem; parameters for phosphoproteomics
  analyses involves specifying the mass difference between unmodified and
  modified residues and the nature of the residue that might bear the
  modification.  There are two different settings available:

  <itemizedlist>

    <listitem>

      <para>

        <guilabel>Potential modifications</guilabel>: the <guilabel>Y</guilabel>
        residue might be phosphorylated, with a net mass increment of
        <guilabel>79.96633</guilabel> Da.

      </para>

    </listitem>


    <listitem>

      <para>

        <guilabel>Potential modifications motif</guilabel>: the
        <guilabel>S</guilabel> and <guilabel>T</guilabel> residues might be
        phosphorylated (net mass increment of <guilabel>79.96633</guilabel> Da) or
        be subject to a neutral phosphoric acid loss (net mass loss of
        <guilabel>97.9769</guilabel> Da).

      </para>

    </listitem>

  </itemizedlist>

</para>

<para>

  The reason why the potential phosphorylation of tyrosine (Y) is not mentioned
  along with the S and T modifications in the motif setting is that
  phosphorylated tyrosine residues do not suffer from phosphoric acid neutral
  loss upon collisionally activated dissociation (CID).  Phosphorylated serine
  and threonine residues are readily dephosphorylated upon CID. Hence, the
  requirement to configure both the phosphorylation and the dephosphorylation
  events as a PROSITE motif (see the question mark help).

</para>

<tip>

  <para>

    The loss of a phosphoric acid molecule <emphasis>(not ion)</emphasis> is
    called a <quote>neutral loss</quote>. By essence, the lost molecule cannot
    be detected, because it bears no  charge. However, the search software may
    detect that there might be a negative mass delta between calculated
    fragments bearing a phoshoryl group and the measured mass of product ions.
    In this eventuality, the software may deduce that the fragment was
    phosphorylated before the fragmentation occurred.

  </para>


  <para>

    The mass spectrometer might be configured to monitor neutral phosphoric acid
    loss, or not. In some instruments, that workflow is not available; however, in
    these instruments a higher energy collisional dissociation<footnote><para> In
    Orbitrap analyzer-based instrument, HCD stands for <quote>higher-energy C-trap
    dissociation</quote>. However, a more generic term is oft-used: <quote>higher
    energy collisional dissociation</quote>.</para></footnote> process elicits two
    fragmentation events: loss of a phosphoric acid molecule and peptide backbone
    dissociation. In this case, the database searching engine (&xtandem;, for us)
    is instructed to monitor the loss of phosphoric acid (that is, a neutral loss)
    on the product ions of the y ion series. In the best cases (best sequence
    coverage by the product ions), it is thus possible to locate the phosphoryl
    group on the peptide.

  </para>

</tip>


<caution xml:id="caution_phosphoproteomics-with-label-based-quantitation">

  <title>Phospho-proteomics projects often involve label-based quantification</title>

  <para>

    The <guilabel>Residue</guilabel> tab of the <guilabel>X!Tandem preset
    editor</guilabel> window shown above lists a number of fixed modifications
    that need an explanation, because we'll find them later in other figures.
    The <guilabel>57.02146@C</guilabel> modification is the carbamidomethylation
    of cysteine residues. The various 28, 32 and 36 mass increments on lysine
    residues are the di-methylation modifications with (32, 36) or without (28)
    heavy isotopes<footnote><para> Boersema <emphasis>et al.</emphasis> 2009.
    Multiplex peptide stable isotope dimethyl labeling for quantitative
    proteomics. <emphasis>Nature Protocols</emphasis>.  </para></footnote>. The
    exact same mass increments labelled with the <quote>@[</quote> notation are
    the same modifications occurring on the N-terminus of the peptide.

  </para>

</caution>


<para>

  Once all the settings have been validated, click the <guilabel>Run</guilabel>
  button, in the same manner as described at <xref
  linkend="sect_running-properly-configured-xtandem-process" />, to actually
  start the database search process.

</para>

  </sect1>


  <sect1 xml:id="sect_phospho-loading-protein-identification-results">

    <title>Loading the Protein Identification Results</title>

    <para>

      The loading of the protein identification results for a phospho-proteomics
      project occurs in an identical fashion as for a non phospho-proteomics
      project, as described in <xref
      linkend="sect_loading-protein-identification-results" />. The main program
      window shows the same <guilabel>Identifications</guilabel> tab as
      described before (see <xref
      linkend="fig_xtpcpp-ptm-project-results-after-loading-main-window" />).

    </para>


    <figure xml:id="fig_xtpcpp-ptm-project-results-after-loading-main-window">

      <title>The main window's <guilabel>Identifications</guilabel> tab</title>

      <mediaobject>

        <imageobject role="fo">
          <imagedata fileref="print-xtpcpp-ptm-project-results-after-loading-main-window.png"/>
        </imageobject>
        <imageobject role="html">
          <imagedata fileref="web-xtpcpp-ptm-project-results-after-loading-main-window.png"/>
        </imageobject>

        <caption>

          <para>

            The <guilabel>Identifications</guilabel> tab inside of the main program
            window after loading a PTMs-based project's identification results
            files. The <guibutton>View protein list</guibutton> and <guibutton>View
            MS identification list</guibutton> buttons perform exactly as described
            earlier. In order to trigger the PTMs-based data exploration session,
            click the <guibutton>View PTM islands</guibutton> button.

          </para> 

        </caption>

      </mediaobject>

    </figure>


    <para>

      When &i2mcq; has done loading all the results, the <guilabel>Protein
      list</guilabel> window opens up as it usually does. This window, however, is
      not devoted to the exploration of phosho-proteomics data.  In order to start
      the exploration of phospho-proteomics data, it is necessary to display a
      window that lists all the post-translational modification islands
      (<quote>PTM islands</quote>). That window shows up when the user clicks onto
      the <guibutton>View PTM islands</guibutton> button shown in <xref
      linkend="fig_xtpcpp-ptm-project-results-after-loading-main-window" />.

    </para>

  </sect1>


  <sect1 xml:id="sect_phospho-exploration-ptm-islands-identification-data">

    <title>Exploring PTM Islands Identification Data</title>

    <para>

      The workflow involved in the scrutiny of phospho-proteomics data is
      comparable to that for a conventional proteomis project, as described in
      <xref linkend="sect_protein-list-window" />. There are differences,
      however, both in terminology and in the kind of data presented to the user
      that require a detailed review, performed in the following sections. The
      first step is to show the PTM islands, which is the step analogous to
      showing the identified proteins. The second step is to look into a given
      PTM island by scrutinizing the PTM peptides that make it; this step is
      analogous to showing the peptide list for a given protein.

    </para>


    <sect2 xml:id="sect2_phospho-ptm-islands-list-window">

      <title>The PTM Islands List Window</title>

      <para>

        The <guilabel>PTM island list</guilabel> window in <xref
        linkend="fig_xtpcpp-ptm-islands-list-window" /> displays, in a table view,
        all the PTM islands that were identified (see <xref
        linkend="sect_protein-identification-phospho-proteomics" />).

        <footnote>

          <para>

            The data presented in the examples below come from an experiment published
            in T. Y. Delormel <emphasis>et al</emphasis>.  2022. In vivo
            identification of putative CPK5 substrates in <emphasis>Arabidopsis
            thaliana</emphasis>. <emphasis>Plant Science</emphasis>. DOI: <link
            xlink:href="https://doi.org/10.1016/j.plantsci.2021.111121"/>.

          </para>

        </footnote>

      </para>


      <figure xml:id="fig_xtpcpp-ptm-islands-list-window">

        <title>PTM islands list window</title>

        <mediaobject>

          <imageobject role="fo">

            <imagedata fileref="print-xtpcpp-ptm-islands-list-window.png" 
            format="PNG" scale="100"/>

          </imageobject>

          <imageobject role="html">

            <imagedata fileref="web-xtpcpp-ptm-islands-list-window.png" 
            format="PNG" scale="100"/>

          </imageobject>

          <caption>

            <para>

              The <guilabel>PTM island list</guilabel> table view has many columns
              that characterize each PTM island, as described in detail in the text
              below.

            </para>

          </caption>

        </mediaobject>

      </figure>

      <para>

        In the table view of all the PTM islands, each row corresponds to an island.
        It must be noted that multiple rows may appear as identical islands. This is
        not exactly true because, while the PTM islands appear to be exactly the
        same, these have been identified in proteins that are listed in the proteins
        database under different accession numbers (see the
        <guilabel>accession</guilabel> column of the table view).

      </para>

      <para>

        The columns in the table view hold post-translational modification-specific data described below.

        <itemizedlist>

          <listitem>

            <para>

              <guilabel>Checked</guilabel>: if checked, the corresponding PTM island will be taken into account;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>PTM island ID</guilabel>: unambiguous identifier for the PTM
              island. The nomenclature is important and follows a precise syntax according to this scheme: 

            </para>

            <para>

              ptm&lt;letter1&gt;&lt;number&gt;&lt;letter2&gt;&lt;number&gt;&lt;letter3&gt;&lt;number&gt;,
              with the following meaning:

              <itemizedlist>

                <listitem>

                  <para>

                    &lt;letter1&gt;&lt;number&gt;: identifies the group of proteins that share PTM islands;

                  </para>

                </listitem>

                <listitem>

                  <para>

                    &lt;letter2&gt;&lt;number&gt;: identifies the PTM island in the above group;

                  </para>

                </listitem>

                <listitem>

                  <para>

                    &lt;letter3&gt;&lt;number&gt;: identifies the accession number of the protein in the group above.

                  </para>

                </listitem>

              </itemizedlist>

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>accession</guilabel>: the accession number of the protein in
              the protein database file;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>description</guilabel>: the description of the protein in
              the protein database file;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>ptm positions</guilabel>: the number of positions in the
              PTM island that bear a post-translational modification;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>spectrum</guilabel>: number of distinct spectra that allowed
              defining this PTM island;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>PTM spectrum</guilabel>: number of distinct spectra that
              allowed the identification of more than one searched
              post-translational modifications;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>sequence</guilabel>: number of unique peptide sequences
              found in this PTM island;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>multi PTM</guilabel>: number of spectra that allowed the
              identification of more than one post-translational modification site;

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>start</guilabel>: the position of the PTM island on the
              protein sequence (the first residue of the PTM island);

            </para>

          </listitem>

          <listitem>

            <para>

              <guilabel>length</guilabel>: the number of residues that encompass the PTM island.

            </para>

          </listitem>

        </itemizedlist>

      </para>

      <para>

        In the same way as for the <guilabel>Protein list</guilabel> window's table
        view of the identified proteins, the cells of the table are
        <quote>active</quote>: clicking onto any cell triggers the opening of a
        window that provides details about the corresponding PTM island.

      </para>

    </sect2>


    <sect2 xml:id="sect_delving-inside-ptm-island-identification-data">

      <title>Delving Inside the PTM Island Identification Data</title>

      <para>

        The protein identifications list table view, as pictured in <xref
        linkend="fig_xtpcpp-ptm-islands-list-window"/> is actually an active matrix
        where the user can easily trigger the exposition of the data that yielded
        any PTM island identification element of the table. This is simply done by
        clicking onto any cell of the table at the row matching the PTM island for
        which scrutiny of the data is requested.

      </para>

      <para>

        Depending on the column at which the mouse click happens, there might be two
        different windows showing up:

        <itemizedlist>

          <listitem>

            <para>

              Clicking onto a cell in either the <guilabel>accession</guilabel> or
              <guilabel>description</guilabel> column opens the <guilabel>Protein
              details</guilabel> window shown in <xref
              linkend="fig_xtpcpp-phospho-island-protein-details" />.

            </para>


            <figure xml:id="fig_xtpcpp-phospho-island-protein-details">

              <title>Protein details window</title>

              <mediaobject>

                <imageobject role="fo">
                  <imagedata fileref="print-xtpcpp-phospho-island-protein-details.png"/>
                </imageobject>
                <imageobject role="html">
                  <imagedata fileref="web-xtpcpp-phospho-island-protein-details.png"/>
                </imageobject>

                <caption> 

                  <para>This window provides details about the protein identified as
                  bearing one or more post-translation modification(s). The peptides that
                  were matched as PSMs are highlighted in yellow. The
                  <guibutton>AT3G56150</guibutton> provides a link to an external
                  resource<footnote><para>Here, the page <link
                  xlink:href="https://www.arabidopsis.org/servlets/TairObject?type=locus&amp;name=AT3G56150"/>
                  opens in the browser.</para></footnote>. The other informational data
                  bits are self-explanatory.</para>

              </caption>

            </mediaobject>

          </figure>

        </listitem>

        <listitem>

          <para>

            Clicking onto a cell in the <guilabel>PTM island ID</guilabel> column
            opens the <guilabel>PTM peptide list</guilabel> window for that specific
            island.  That window is displayed in both figures <xref
            linkend="fig_xtpcpp-phospho-island-peptide-table-view-window-1" /> and
            <xref linkend="fig_xtpcpp-phospho-island-peptide-table-view-window-2" />
            in the following section (<xref
            linkend="sect_phospho-ptm-peptides-list-window" />).

          </para>

        </listitem>

      </itemizedlist>

    </para>

  </sect2>

</sect1>


<sect1 xml:id="sect_phospho-ptm-peptides-list-window">

  <title>The PTM Peptides List Window</title>

  <para>

    Each PTM island is essentially defined by a set of related peptides that
    sport one or more post-translational modifications. The exploration of the
    PTM peptides thus involves looking into the different peptides of a
    PTM island. A number of characteristics of these peptides are described in
    the following text.

  </para>


  <figure xml:id="fig_xtpcpp-phospho-island-peptide-table-view-window-1">

    <title>PTM peptides list window (first columns)</title>

    <mediaobject>

      <imageobject role="fo">
        <imagedata fileref="print-xtpcpp-phospho-island-peptide-table-view-window-1.png"/>
      </imageobject>
      <imageobject role="html">
        <imagedata fileref="web-xtpcpp-phospho-island-peptide-table-view-window-1.png"/>
      </imageobject>

      <caption> 

        <para>Every PTM island has, associated to it, a list of
        post-translationally modified peptides. This figure illustrates the
        first columns of the table view that lists all the peptides making a PTM
        island. The contextual menu visible near peptide ID
        <guilabel>pepb87b23</guilabel> will be detailed later (<xref
        linkend="sect_multi-site-modifications-ambiguities" />).</para>

  </caption>

</mediaobject>

</figure>


<figure xml:id="fig_xtpcpp-phospho-island-peptide-table-view-window-2">

  <title>PTM peptides list window (last columns)</title>

  <mediaobject>

    <imageobject role="fo">
      <imagedata fileref="print-xtpcpp-phospho-island-peptide-table-view-window-2.png"/>
    </imageobject>
    <imageobject role="html">
      <imagedata fileref="web-xtpcpp-phospho-island-peptide-table-view-window-2.png"/>
    </imageobject>

    <caption> 

      <para>Every PTM island has, associated to it, a list of
      post-translationally modified peptides. This figure illustrates the last
      columns of the table view that lists all the peptides making a PTM
      island.</para> 

  </caption>

</mediaobject>

</figure>

<para>

  The <guilabel>PTM peptide list</guilabel> window contains a large set of
  peptide characteristics organized in a number of columns, as described below:

  <itemizedlist>

    <listitem>

      <para>

        <guilabel>peptide ID</guilabel>: the unambiguous identity of the peptide;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>sample</guilabel>: the file name of the sample in which the
        peptide was found and sequenced;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>scan</guilabel>: the scan number of the MS/MS spectrum in
        which the precursor peptidic ion was fragmented;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>RT</guilabel>: the retention time at which the peptide eluted;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>charge</guilabel>: the charge of the peptidic ion;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>sequence</guilabel>: the sequence of the peptide. Note that
        the residues that do (or might) bear a post-translational modification
        are printed in red color;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>modifs</guilabel>: a semi-column-separated list of modified
        positions. The modification is identified by the net mass change that
        occurs upon the chemical modification. The <guilabel>1Y:32.06 4S:79.97
        15K:32.06</guilabel> example indicates that the tyrosine at position 1
        is dimethylated, the serine at position 4 is phosphorylated and that
        the lysine at position 15 is methylated (the dimethyl modification was
        commented above).

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>start</guilabel>: the position of the peptide's first
        residue in the protein sequence;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>length</guilabel>: the number of residues in the peptide;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>top Evalue</guilabel>: best Evalue from those calculated for
        different peptide spectrum matches that occurred in a single
        fragmentation scan (see <xref
        linkend="sect_multi-site-modifications-ambiguities" /> below for a
        thorough description of this situation);

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>theoretical MH+</guilabel>: the calculated mass of the &mh;
        peptidic ion;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>delta MH+</guilabel>: the difference between the measured
        and the calculated &mh; masses;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>top hyperscore</guilabel>: best Hyperscore from those
        calculated for different peptide spectrum matches that occurred in a
        single fragmentation scan (see <xref
        linkend="sect_multi-site-modifications-ambiguities" /> below for a
        thorough description of this situation);

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>top PTM positions</guilabel>: positions of modified residues
        for the peptide having the best Evalue (see above) for the current scan;

      </para>

    </listitem>

    <listitem>

      <para>

        <guilabel>observed PTM positions</guilabel>: space-separated list of all
        the modified positions found for the current scan.

      </para>

    </listitem>

  </itemizedlist>

</para>


<!--Dans la feneêtre de liste des PTM peptide list, choisir ptma6a1a1 pour avoir le-->
<!--cas de figure où on est certain d'une modif (phospho) mais pas de sa position (peptide pepb87b23 dans la liste).-->

<!--Pour ce peptide on a trois lignes: il a donc été indentifié dans trois scans MS2-->
<!--différents, on voit dans une ligne S rouge et S orange: il n'y a pas de-->
<!--localisation sûre pour 1 événement d ephosphorylaiton. Une autre ligne montre un-->
<!--seul S rouge, là la position est certaine au regard des données MS2.-->

<!--Quand on clique sur une cellule de la lignes rouge/orange : sous-menu proposant-->
<!--d'ouvrir les fenêtre peptide details pour l'un et l'autre scan (Ctrl pour ouviri-->
<!--les deux séparément). Dans cet exemple on voit qu'il s'agit absolument du mêe-->
<!--scan, donc on ne peut vriament pas choisir. (scan 4473).-->

<!--Si l'on prend le scan 4187, alors on voit qu'en cliquant pas d'aleternative, pas-->
<!--d'orange sur la ligne -> position établie.-->

<sect2 xml:id="sect_delving-inside-phospho-peptide-identification-data">

  <title>Delving Inside the PTM Peptide Identification Data</title>

  <para>

    The <guilabel>PTM peptide list</guilabel> table view (<xref
    linkend="fig_xtpcpp-phospho-island-peptide-table-view-window-1" />) is
    actually an active matrix where the user can easily trigger the exposition o
    fthe data that yielded any PTM peptide identification element of the table.
    This is simply done by clicking onto any cell of the table at the row
    matching the peptide for which scrutiny of the data is requested.

  </para>

  <sect3 xml:id="sect_phospho-peptide-details-window">

    <title>The Peptide Details Window</title>

    <para>

      Clicking a cell in the <guilabel>peptide ID</guilabel> column opens the
      <guilabel>Peptide details</guilabel> window shown in <xref
      linkend="fig_xtpcpp-phospho-peptide-ms-ms-details-one-positive-phospho-site"
      />. Notice how this window is similar to the one described for
      conventional non PTM-based projects at <xref
      linkend="sect_peptide-details-window" />.

    </para>


    <figure xml:id="fig_xtpcpp-phospho-peptide-ms-ms-details-one-positive-phospho-site">

      <title>Peptide details window</title>

      <mediaobject>

        <imageobject role="fo">
          <imagedata fileref="print-xtpcpp-phospho-peptide-ms-ms-details-one-positive-phospho-site.png"/>
        </imageobject>
        <imageobject role="html">
          <imagedata fileref="web-xtpcpp-phospho-peptide-ms-ms-details-one-positive-phospho-site.png"/>
        </imageobject>

        <caption>

          <para>

            This window displays a large amount of informational data bits that
            characterize the MS/MS spectrum &vs; peptide match (PSM) for the
            peptide ID that was clicked in the <guilabel>PTM peptide
            list</guilabel> table view window. Almost all the information data
            bits shown in this figure are self-explanatory.

          </para>

        </caption>

      </mediaobject>

    </figure>

    <tip>

      <para>

        The nomenclature of the product ions in the MS/MS spectrum shown in the
        figure above is simple: when the ion under a given MS/MS spectrum peak is
        the result of a neutral loss of phosphoric acid, the ion is labelled
        <quote>y<emphasis>x</emphasis>(-P<emphasis>z</emphasis>)</quote>, with
        <emphasis>x</emphasis> being the index of the y ion, and
        (-P<emphasis>z</emphasis>) indicating the loss of <emphasis>z</emphasis>
        phosphoric acid neutral molecule(s).

      </para>

      <para>

        When ions actually bear any number of post-translational modifications,
        these are not listed along with the ion series (<emphasis>b</emphasis> or
        <emphasis>y</emphasis>) and index text because that would quickly become
        unwieldy, from a graphical point of view.

      </para>

    </tip>

  </sect3>


  <sect3 xml:id="sect_multi-site-modifications-ambiguities">

    <title>Ambiguities on the post-translational modification sites</title>

    <para>

      The <guilabel>PTM peptide list</guilabel> table view in <xref
      linkend="fig_xtpcpp-phospho-island-peptide-table-view-window-1" /> lists
      the <guilabel>pepb87b23</guilabel> peptide ID thrice because that peptide
      was identified in three different MS/MS scans (see the
      <guilabel>scan</guilabel> column values for the three rows). What is
      interesting with these three rows, is that the peptide sequence showing
      the modification position(s) varies from a row to the other. In one row,
      the sequence only shows a single red-colored serine residue. That serine
      was positively identified as a phosphorylated residue. In the other two
      rows, one serine is red-colored and the other is orange-colored. For these
      two scans, the position of the phosphorylated residue is not ascertained:
      the MS/MS data only tell that it is possible that one or the other serine
      residue be phosphorylated (but not both).

    </para>

    <note>

      <para>

        The coloring of these two serine residues is arbitrary in this case:
        since the PSMs that yielded this same sequence are absolutely identical
        from a score point of view (when opening the peptide details window, one
        can check that the Evalues are identical for the two PSMs), the software
        labels in red the first modification position and in orange all the
        remaining ones.

      </para>

    </note>

    <para>

      When clicking any cell of any one of the two rows where there is an
      ambiguity over the location of the phosphorylation event, &i2mcq; shows a
      contextual menu that displays the possible phosphorylation positions.
      The user can thus select one or the other of the menu items to display the
      details of the corresponding peptide.

    </para>

    <para>

      The ambiguity about the phosphorylation site above, on <guilabel>Peptide
      ID</guilabel> <guilabel>pepb87b23</guilabel>, is intereresting. Indeed, when
      the user selects the first item of the contextual menu and then (by keeping
      the <keycap>Ctrl</keycap> key pressed) selects the second item of that
      menu, the windows that open up show exactly the same informational data bits
      about the MS/MS scan: the two PSMs were calculated by the search engine on
      the basis of the very same MS/MS scan (<xref
      linkend="fig_xtpcpp-phospho-peptide-ms-ms-details-two-psm-in-one-scan" />).
      This is one reason why both positions could not be ascertained: the engine
      says that one position is plausible (with a pretty low Evalue) and that the
      other position is equally plausible (with the exact same low Evalue).


    </para>


    <figure xml:id="fig_xtpcpp-phospho-peptide-ms-ms-details-two-psm-in-one-scan">

      <title>Two mass spectrum &vs; peptide matches (PSM) in a single MS/MS scan</title>

      <mediaobject>

        <imageobject role="fo">
          <imagedata fileref="print-xtpcpp-phospho-peptide-ms-ms-details-two-psm-in-one-scan.png" scale="93"/>
        </imageobject>
        <imageobject role="html">
          <imagedata fileref="web-xtpcpp-phospho-peptide-ms-ms-details-two-psm-in-one-scan.png"/>
        </imageobject>

        <caption>

          <para>This figure shows the two <guilabel>Peptide details</guilabel>
            windows that are opened when the user clicks onto the first and the
            the second menu items of the contextual menu shown in the
            <guilabel>PTM peptide list</guilabel> window shown in <xref
            linkend="fig_xtpcpp-phospho-island-peptide-table-view-window-1" />. In
            both windows, the scan number is the same (<guilabel>4238</guilabel>),
            demonstrating that both PSMs were computed from the same MS/MS
            spectrum acquisition.  Further, the same pretty low E-value should hint
            at a low reliability of the phosphorylation site
            identification.</para>

        </caption>

      </mediaobject>

    </figure>



  </sect3>


  <sect3 xml:id="sect_xic-viewer-window-for-phospho-peptide-details">

    <title>The XIC Viewer Window for the PTM Peptide Details</title>

    <para>

      One interesting feature of the <guilabel>Peptide details</guilabel>
      window, is the <guilabel>XIC</guilabel> button (top right) that triggers
      the calculation of an extracted ion current chromatogram, as pictured in
      <xref linkend="fig_xtpcpp-xic-viewer-for-phospho-peptide-details"/>.
      Although very similar to the window described at <xref
      linkend="sect_xic-viewer-window-for-peptide-details" />, this
      phospho-proteomics-specific version of the <guilabel>XIC viewer</guilabel>
      has specific informational data bits described below.


    </para>


    <figure xml:id="fig_xtpcpp-xic-viewer-for-phospho-peptide-details">

      <title>The extracted ion current (XIC) chromatogram viewer window</title>

      <mediaobject>

        <imageobject role="fo">

          <imagedata fileref="print-xtpcpp-phospho-xic-viewer-for-peptide-details.png" 
          format="PNG" scale="100"/>

        </imageobject>

        <imageobject role="html">

          <imagedata fileref="web-xtpcpp-phospho-xic-viewer-for-peptide-details.png" 
          format="PNG" scale="80"/>

        </imageobject>

        <caption>

          <para>

            The extracted ion current (XIC) chromatogram viewer shows the
            peptide sequence interspersed with post-translational modifications
            data. The modifications are listed as PSIMOD OBO accession number
            <guilabel>(MOD:xxxxx)</guilabel> text elements<footnote><para>
                Montecchi-Palazzi <emphasis>et al.</emphasis> 2008. PSI-MOD
                community standard for representation of protein modification
                data. <emphasis>Nature biotechnol.</emphasis></para></footnote>.

          </para>

        </caption>

      </mediaobject>

    </figure>

    <para>

      The phospho-proteomics-specific informational data bits provided in the
      <guilabel>XIC viewer</guilabel> window (see <xref
      linkend="fig_xtpcpp-xic-viewer-for-phospho-peptide-details" />) are
      located right below the top border of the plot frame.  

    </para>

  </sect3>

</sect2>


</sect1>



  </chapter>

