<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter [

<!ENTITY % pappso-entities SYSTEM "pappso-user-manuals.ent">
%pappso-entities;

<!ENTITY % i2masschroq-entities SYSTEM "i2masschroq-entities.ent">
%i2masschroq-entities;

<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">
<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN"
"/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>

<chapter
  xml:id="chap_generalities" 
  xmlns="http://docbook.org/ns/docbook" version="5.0"
  xmlns:xlink="http://www.w3.org/1999/xlink">

<info>

  <title>Generalities</title>

  <keywordset>
    <keyword>general concepts</keyword>
    <keyword>peptide spectrum match</keyword>
    <keyword>identification</keyword>
    <keyword>protein inference</keyword>
    <keyword>installation</keyword>
  </keywordset>

</info>

<para>In this chapter, I wish to introduce some general concepts around the
  &i2mcq; program, the reference to be used to cite the software in
  publications, the building and installation procedures.</para>

<sect1 xml:id="sect_history-project">

  <title> History of the project</title>

  <para>

      &i2mcq; is the successor of the &xtpjava; project that has seen the following changes along the years:

    <itemizedlist>

      <listitem>

        <para>

          Full rewrite of the &xtpjava; program from Java to C++17. The
          Java-based software program had been published in

          Olivier Langella, Benoît Valot, Thierry Balliau, Mélisande
          Blein-Nicolas, Ludovic Bonhomme, and Michel Zivy (2016)
          <emphasis>X!TandemPipeline: A Tool to Manage Sequence Redundancy for
          Protein Inference and Phosphosite Identification.</emphasis> in
          <emphasis>J. Proteome Res.</emphasis> 2017, 16, 2, 494–503.
          https://doi.org/10.1021/acs.jproteome.6b00632.

          <tip>

            <para>

              Before the integrations described below, the product of the
              rewrite has been called transitorily &xtpcpp;++ (or
              <application>xtpcpp</application>). That name might appear in some
              places while the code/documentation is being revised to change
                its name to &i2mcq;.

            </para>

          </tip>

        </para>

      </listitem>

      <listitem>

        <para>

          Integration into the new software of the &mcq; software project that
          was developed as a standalone C++ software piece. &mcq; is a software
          project that was developed to perform quantitative proteomics in a
          variety of modes (label-free or with labelling).

        </para>

      </listitem>

      <listitem>

        <para>

          Unfinalized integration of the &mcqr; project that was developed as a
          standalone project. &mcqr; is a GNU&nbsp;R project aimed at performing
            bio-statistical analyses on the quantification analysis performed by &mcq;.

        </para>

      </listitem>

    </itemizedlist>

  </para>

  <para>

    The &i2mcq; project encompasses three main quantitative proteomics 
    fields of endeavour:

    <itemizedlist>

      <listitem>

        <para>

          Database search, peptide identification and protein inference. The
          database search is actually performed by &xtandem; and is started
          seamlessly by &i2mcq;. Protein grouping is performed by original code
            in &i2mcq;.

        </para>

      </listitem>

      <listitem>

        <para>

          Quantitative proteomics, mainly based on area-under-the-curve
          processes (requires the full mass data set to extract ion current
          chromatograms, XIC).  This part was historically performed by the
            &mcq; software program.

        </para>

      </listitem>

      <listitem>

        <para>

          Bio-statistical analysis of the quantification data. This part was
          historically performed by the &mcqr; GNU&nbsp;R-based package
          (unpublished software as of yet).

        </para>

      </listitem>

    </itemizedlist>

  </para>

</sect1>


<sect1 xml:id="sect_meaning-of-the-software-name">

  <title>What does &i2mcq; Stand for?</title> 

  <para>

    The &i2mcq; software project aims at providing users with an integrated
    software solution for quantitative proteomics. As described in detail in
    another chapter of this book, quantitative proteomics involve a number of
    steps that can be enumerated in sequence below:

    <itemizedlist>

      <listitem>

        <para>

          Search databases to connect MS/MS spectra to peptide sequences. This
        step is called <emphasis>identification</emphasis>;

      </para>

    </listitem>

    <listitem>

      <para>

        Apply logic to reliably identify proteins based on the peptides identified
      at the previous step. This step is called <emphasis>inference</emphasis>;

    </para>

  </listitem>

  <listitem>

    <para>

      Optionally perform quantification of the
      <emphasis>i</emphasis>dentified peptides and
      <emphasis>i</emphasis>nferred proteins.  &i2mcq; has
      area-under-the-curve quantitative proteomics capabilites that are
      based on precursor peptide ion current extraction from the mass
      spectrometric data. The extracted ion currents are then plot like
      chromatograms: intensity as a function of retention time.  This
      analytical process thus somehow involves <quote><emphasis>Mass
      Chro</emphasis>matograms</quote> for the
      <emphasis>Q</emphasis>uantification.

    </para>

  </listitem>

</itemizedlist>

  </para>

  <para>

    From the sequence above, the &i2mcq; name becomes self-explanatory!

  </para>

  <tip>

    <para>

      It is however possible (and encouraged) to mentally read &i2mcq; as
      <quote><emphasis>I too MassChroQ&hairsp;!&hairsp;</emphasis></quote>

    </para>

  </tip>

</sect1>


<sect1 xml:id="sect_transitioning-from-xtpcpp-to-i2masschroq">

  <title>Transitioning from &xtpcpp; to &i2mcq;</title> 

  <para>

    The previous &xtpcpp; version of this software did store configuration data
    in the local configuration directory and in the
    <filename>PAPPSO/xtpcpp.conf</filename> file. In order to preserve these
    configuration data after having transitioned from &xtpcpp; to &i2mcq;,
    please, rename that configuration file to
    <filename>PAPPSO/i2masschroq.conf</filename>.

  </para>

</sect1>

<sect1 xml:id="sect_general-concepts-terminologies">

  <title>General concepts and terminologies</title> 

  <para>

    This section describes the general concepts at the basis of the analysis of
    proteomics data that one needs to grok in order to properly assimilate the
    workings of the &i2mcq; software.

  </para>

  <sect2 xml:id="sect_bottom-up-top-down-proteomics">

    <title>Bottom-up Proteomics or Top-down Proteomics?</title>

    <para>

      Proteomics is a mass spectrometry-based field of endeavour that is aimed at
      characterizing the <quote>protein complement</quote> of a given genome. The
      protein complement of a genome is the set of proteins that are expressed at a
      given instant in the life of a cell, a tissue or an organ, for example.
      Characterizing that protein complement actually means identifiying the
      proteins expressed by a given living cell or tissue or organ. Optionally, if
      feasible, the characterization of post-translational modifications might be
      desirable.

    </para>

    <para>

    There are two main variants of protemics: <quote>bottom-up</quote>
    proteomics and <quote>top-down</quote> proteomics:

    <itemizedlist>

      <listitem>

        <para>

          The first variant—bottom-up proteomics—identifies proteins on the
          basis of the identification of all the peptides obtained by first
          digesting all the proteins of the sample using an enzyme of known
          specificity. In this variant, the sample that is injected in the
          mass spectrometer is the resulting peptide mixture (first resolved
          by high performance liquid chromatography).  The identification of
          the proteins contained in the initial sample is performed in a
          number of steps that are actually the focus of &i2mcq;. Indeed the
            &i2mcq; software is a bottom-up-oriented software program.

        </para>

      </listitem>

      <listitem>

        <para>

          The second variant—top-down proteomics—identifies proteins on the
          The second variant identifies proteins on the basis of intact proteins
          directly injected in the mass spectrometer. Of course, it might be
          necessary to fragment the proteins in the mass spectrometer and to use
          the fragments to actually identify the protein. However, the fact that
          the protein is first detected and analyzed as one entity (and not as
          set of peptides), allows for some very useful discoveries, like the
          identity and number of post-translational modifications, for example.

        </para>

      </listitem>

    </itemizedlist>


    <note>

      <para>

        At the moment, &i2mcq; does not handle top-down proteomics data: it
        is a bottom-up proteomics software project.

      </para>

    </note>

  </para>

</sect2>

<sect2 xml:id="sect_typical-cyle-of-mass-data-acquisition">

  <title>Typical cycle of a mass spectrometer data acquisition</title>

  <para>

    Once the initial sample, containing all the proteins to identify, has
    been digested using a protease of known cleavage specificity (trypsin,
    typically), the peptidic mixture (that might be highly complex) needs to
    be resolved as much as possible using chromatography. In the vast
    majority of the proteomics experimental settings, the chromatography
    setup is connected to the mass spectrometer so that when the gradient is
    developed, all the peptides are immediately injected <quote>on
    line</quote> to the mass spectrum ion source.

  </para>

  <para>

    The mass spectrometer runs an analysis cycle that can be summarized like
    the following:

    <itemizedlist>

      <listitem>

        <para>

          Acquire a full scan mass spectrum of the whole set of ions at a given
          chromatography retention time. This kind of mass spectrum is
          called a MS spectrum;

        </para>

      </listitem>

      <listitem>

        <para>

          Enter a loop during which ions having the most intense signal are
          subjected in turn to collision-induced dissociation (CID), that
          is, are fragmented by accelerating them against gas molecules in a
          fragmentation cell. The mass spectra that are collected at each
          one of these fragmentation acquisitions are called MS/MS spectra
          because they are obtained after two mass analysis events: the
          first event is the measurement of the intact peptide ion's m/z
          value (full scan mass spectrum) and the second event is the
          measurement of all the obtained fragments' m/z values (MS/MS
          scan).

        </para>

      </listitem>

    </itemizedlist>

  </para>

  <para>

    Each instrument records all the MS and MS/MS spectra in a raw data
    format file that is specific of the vendor. Free Software developers
    cannot know the internal structure of the files. To use the mass
    spectrometric data, they need to rely on a specific software that
    performs the conversion from the raw data format to an open data format
  (mzML). That program is called <application>msconvert</application>,
  from  the <productname>ProteoWizard</productname> project. 

</para>

<note>

  <para>

    Mass spectrometrists used to call ions that were analyzed in full scan
    mass spectra <quote>parent ions</quote>. They also used to call fragment
    ions arising upon fragmentation of a parent ion <quote>daughter
    ions</quote>. This terminology has been deprecated and has been replaced
    with <quote>precursor ion</quote> and <quote>product ion</quote>,
    respectively. In our document, we thus use the new terminology.

  </para>

</note>

</sect2>

<sect2 xml:id="sect_outline-xtpcpp-working-session">

  <title>Outline of an &i2mcq; working session</title>

  <para>

      &i2mcq; loads mzXML- and mzML-formatted files and needs for its
    operations to have accesss to all the MS and MS/MS spectra. Once data
    files have been loaded, &i2mcq; allows the user to perform the
    following tasks, that will be detailed in later chapters:

    <itemizedlist>

      <listitem>

        <para>

          Configure the &xtandem; database searching software (that is, the
          software, external to &i2mcq; that actually performs the
          peptide-mass spectrum matches);

        </para>

      </listitem>

      <listitem>

        <para>

          Run the &xtandem; software and load its results;

        </para>

      </listitem>

      <listitem>

        <para>

          Display the results to the user in a way that they can be
          scrutinized and checked. The peptide identification results
          serve as the basis for another processing step that is
          integrally performed by &i2mcq;: the <quote>protein
          inference</quote>. That step aims at using the peptide
          identifications to actually craft a list of proteins identities.
          The user is provided with various means to control that step in
          various ways.

        </para>

      </listitem>

      <listitem>

        <para>

          Optionally start the &mcq; module to perform the quantitative
          proteomics on the identification data checked at the previous step.

        </para>

      </listitem>

      <listitem>

        <para>

          Optionally start the &mcqr; module to perform the bio-statistical
          analysis of the quantitative proteomics data obtained at the
          previous step.

        </para>

      </listitem>

    </itemizedlist>

  </para>

</sect2>

</sect1>


<sect1 xml:id="sect_citing-software">

  <title>

    Citing the &i2mcq; software.

  </title>

  <para>

    The &i2mcq; software is not yet published. A great part of the features
    found in &i2mcq; have initially been found in the &xtpjava; software that
    can be cited using the following reference:

    Olivier Langella, Benoît Valot, Thierry Balliau, Mélisande Blein-Nicolas,
  Ludovic Bonhomme, and Michel Zivy (2017) <emphasis>X!TandemPipeline: A Tool to
  Manage Sequence Redundancy for Protein Inference and Phosphosite
  Identification.</emphasis> in <emphasis>J. Proteome Res.</emphasis>, 16, 2,
  494–503. DOI: <link xlink:href="https://doi.org/10.1021/acs.jproteome.6b00632"/>.

  </para>

</sect1>


<sect1 xml:id="sect_installation-build-software">

  <title>Installation of the software</title>

  <para>

      The installation material is available at <link
        xlink:href="http://pappso.inrae.fr/en/bioinfo/xtandempipeline/download/"/>.

    </para>

    <sect2 xml:id="sect_installation-windows-macos-systems">

      <title>Installation on MS&nbsp;Windows and macOS systems</title>

      <para>

        The installation of the software is extremely easy on the MS-Windows and
        macOS platforms. In both cases, the installation programs are standard and
        require no explanation.

      </para>

    </sect2>


    <sect2 xml:id="sect_installation-debian-ubuntu-systems">

      <title>Installation on Debian- and Ubuntu-based systems</title>

      <para>

        The installation on Debian- and Ubuntu-based GNU/Linux platforms is
        also extremely easy (even more than in the above situations).
        ; is indeed packaged and released in the official
        distribution repositories of these distributions and the only command
        to run to install it is:

      </para>

      <para>

        <prompt>$</prompt>

        <footnote><para>The prompt character might be
        <keysym>%</keysym> in some shells, like
        <application>zsh</application>.</para></footnote> 

    <command>sudo apt install &lt;package_name&gt;</command><keycap>RETURN</keycap>

  </para>

  <para>

    In the command above, the typical <emphasis>package_name</emphasis> is
    in the form <filename>i2masschroq</filename> for the program package and
    <filename>i2masschroq-doc</filename> for the user manual package.

  </para>

  <para>

    Once the package has been installed the program shows up in the
    <emphasis>Science</emphasis> menu. It can also be launched from the shell
    using the following command: 

  </para>

  <para>

    <prompt>$</prompt> <command>i2masschroq</command><keycap>RETURN</keycap>

  </para>

</sect2>

</sect1>

</chapter>

