#!/usr/bin/env python3

import argparse
import os
import shutil
import macBundleCreation as mbc

parser = argparse.ArgumentParser()

parser.add_argument("libs", nargs = 1, help = '''List of libraries (paths) to be acted on.''');

parser.add_argument("--backup", action = 'store_true', help = '''Specify that the libraries must be backed up.''');

parser.add_argument("--rename", action = 'store_true', help = '''Specify that the librarires 'must be renamed.''');

parser.add_argument("--reinstate", action = 'store_true', help = '''Specify that the libraries must be reinstated.''');

args = parser.parse_args()

# We get this command line argument as a list containing one string
# that is made of any number >= 1 of library absolute paths separated by
# spaces.
backupLibs = args.libs[0].split(" ");

print("The libs to be backed up:" + str(backupLibs))

if backupLibs is not None:
  mbc.backupLibraries(backupLibs);

renameLibs = args.rename;

if renameLibs is True:
  mbc.renameLibraries(backupLibs);

reinstateLibs = args.reinstate;

if reinstateLibs is True:
  mbc.reinstateLibraries(backupLibs);




exit(0)
