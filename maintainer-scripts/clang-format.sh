#!/bin/sh

for file in $(find . -name '*' | egrep ".*\.(h|cpp)$")
do 
	echo "Formatting ${file}"
	clang-format -i ${file}
done
