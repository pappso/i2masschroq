#!/bin/sh

if [ "x$1" = "x" ]
then
  echo "Please provide the version number."
exit 1
fi

VERSION=$1

git archive --format=tar.gz --prefix=xtpcpp-${VERSION}/ -o ../tarballs/xtpcpp-${VERSION}.tar.gz HEAD
