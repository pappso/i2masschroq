# New Feature request

## Feature description
(Describe shortly the feature)

## Feature position
(Where the new feature is located, *for a new widget describe its position according the other widget in the window*)

## New Widget description
(*Optional, what the new widget should look like*)

## Expected Result
(What is the expected result: description or example)

 
