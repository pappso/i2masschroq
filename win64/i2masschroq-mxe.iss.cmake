[Setup]
AppName=i2MassChroQ

#define public winerootdir "z:"
; Set version number below
#define public version "${i2MassChroQ_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "z:/home/langella/developpement/git/i2masschroq/"
#define cmake_build_dir "z:/home/langella/developpement/git/i2masschroq/wbuild"

; Set version number below
AppVerName=i2MassChroQ version {#version}
DefaultDirName={pf}\i2MassChroQ
DefaultGroupName=i2MassChroQ
OutputDir="{#cmake_build_dir}"

; Set version number below
OutputBaseFilename=i2MassChroQ-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=i2MassChroQ-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2016- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="i2MassChroQ, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "z:/win64/mxeqt6_dll/*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/qt6/plugins/*"; DestDir: {app}/plugins; Flags: ignoreversion recursesubdirs
Source: "z:/home/langella/developpement/git/cutelee/wbuild/templates/lib/libCuteleeQt6Templates.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/cutelee/wbuild/cutelee-qt6/6.1/*"; DestDir: {app}/cutelee-qt6/6.1; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "z:/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: i2MassChroQComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: i2MassChroQComp
Source: "{#sourceDir}\win64\i2MassChroQ_icon.ico"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\i2MassChroQ_user_manual.pdf"; DestDir: {app};

Source: "{#cmake_build_dir}\src\i2MassChroQ.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\src\tandemwrapper.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\src\mzxmlconverter.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\masschroq\wbuild\src\libmasschroq.dll"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\masschroq\wbuild\src\masschroq.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\masschroq\wbuild\src\mcql\mcql.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\masschroq\wbuild\src\mcql\mcqlcbor.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\tandemng\wbuild\src\tandemng.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\tandemng\wbuild\src\2015\tandemng2015.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\..\tandemng\wbuild\src\2015p\tandemng2015p.exe"; DestDir: {app}; Components: i2MassChroQComp
Source: "{#cmake_build_dir}\..\win64\tandem.exe"; DestDir: {app}; Components: i2MassChroQComp

[Icons]
Name: "{group}\i2MassChroQ"; Filename: "{app}\i2MassChroQ.exe"; WorkingDir: "{app}";IconFilename: "{app}\i2MassChroQ_icon.ico"
Name: "{group}\Uninstall i2MassChroQ"; Filename: "{uninstallexe}"

[Types]
Name: "i2MassChroQType"; Description: "Full installation"

[Components]
Name: "i2MassChroQComp"; Description: "i2MassChroQ files"; Types: i2MassChroQType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\i2MassChroQ.exe"; Description: "Launch i2MassChroQ"; Flags: postinstall nowait unchecked
