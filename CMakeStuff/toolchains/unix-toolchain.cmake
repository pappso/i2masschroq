message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")
message("If building of the user manual is required, add -DBUILD_USER_MANUAL=1")
message("If using the locally built pappsomspp libs, add -DLOCAL_DEV=1")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.
# Here we want it to be lowercase, UNIX-culture,
# and it is needed for the user manual build.
SET(TARGET i2masschroq)
SET(CAP_TARGET i2MassChroQ)

## Install directories
if(NOT CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX /usr)
endif()
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/bin)
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/share/doc/${TARGET})


find_package( Qt6 COMPONENTS Core Gui Svg Xml PrintSupport Sql Network Concurrent REQUIRED )


find_package(QCustomPlotQt6 REQUIRED)


find_package(OdsStream REQUIRED)


find_package(PappsoMSpp COMPONENTS Core Widget REQUIRED)


find_package(QuaZip-Qt6 REQUIRED)


find_package(ZLIB REQUIRED)


#sudo apt install cutelee6-qt6-dev
find_package(Cutelee6Qt6 REQUIRED)

