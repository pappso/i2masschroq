# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella, Filippo Rusconi

find_path(PappsoMSpp_INCLUDE_DIRS pappsomspp/types.h
	PATHS /usr/local/include /usr/include
	PATH_SUFFIXES pappsomspp libpappsomspp ENV PATH)


find_library(PappsoMSpp_LIBRARY NAMES pappsomspp)

if(PappsoMSpp_INCLUDE_DIRS AND PappsoMSpp_LIBRARY)

	mark_as_advanced(PappsoMSpp_INCLUDE_DIRS)
	mark_as_advanced(PappsoMSpp_LIBRARY)

	message(STATUS "~~~~~~~~~~~~~ ${PappsoMSpp_LIBRARY} ~~~~~~~~~~~~~~~")

	set(PappsoMSpp_FOUND TRUE)

endif()

if(PappsoMSpp_FOUND)

	if(NOT PappsoMSpp_FIND_QUIETLY)
		message(STATUS "Found PappsoMSpp_LIBRARY: ${PappsoMSpp_LIBRARY}")
	endif()

	if(NOT TARGET PappsoMSpp::Core)

		add_library(PappsoMSpp::Core UNKNOWN IMPORTED)

		set_target_properties(PappsoMSpp::Core PROPERTIES
			IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
			INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")

	endif()

	find_library(PappsoMSppWidget_LIBRARY NAMES pappsomspp-widget)

	if(PappsoMSpp_INCLUDE_DIRS AND PappsoMSppWidget_LIBRARY)

		mark_as_advanced(PappsoMSppWidget_LIBRARY)  

		message(STATUS "~~~~~~~~~~~~~ ${PappsoMSppWidget_LIBRARY} ~~~~~~~~~~~~~~~")

		set(PappsoMSppWidget_FOUND TRUE)

		if(NOT TARGET PappsoMSpp::Widget)

			add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)

			set_target_properties(PappsoMSpp::Widget PROPERTIES
				IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
				INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")

		endif()

	endif()

	if(NOT PappsoMSpp_FIND_QUIETLY)
		message(STATUS "Found PappsoMSppWidget_LIBRARY: ${PappsoMSppWidget_LIBRARY}")
	endif()

else()

	if(PappsoMSpp_FIND_REQUIRED)

		message(FATAL_ERROR "Could not find libpappsomspp. Please do specify the
		PappsoMSpp_INCLUDE_DIRS and PappsoMSpp_LIBRARY variables using cmake!")

	endif()

endif()
